package osfs

import (
	"os"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

type (
	Cleaner interface {
		IfOk(err error, isAsErrors ...any)
		IfError(err error, isAsErrors ...any)
		Up(relDirs ...string)
	}
	cleanerT struct {
		baseDir path.AbsDir
		errs    []error
	}
)

func MkdirTemp(dirname string, pattern string) (path.AbsDir, Cleaner) {
	must.Ok(os.MkdirAll(dirname, 0777), os.ErrExist)
	baseDir := must.Ok1(os.MkdirTemp(dirname, pattern))
	tmpDir := path.New(baseDir).AsDir()
	return tmpDir, &cleanerT{baseDir: tmpDir}
}

func (cln *cleanerT) IfOk(err error, exceptIsAsErrors ...any) {
	cln.wipeIf(err, func(err, restErr error) bool {
		return restErr == nil
	}, exceptIsAsErrors)
}

func (cln *cleanerT) IfError(err error, exceptIsAsErrors ...any) {
	cln.wipeIf(err, func(err, restErr error) bool {
		return err != nil && restErr == nil
	}, exceptIsAsErrors)
}

func (cln *cleanerT) wipeIf(err error, cond func(err, restErr error) bool, exceptIsAsErrors []any) {
	restErr := errs.IgnoreErrors(err, exceptIsAsErrors...)
	cln.errs = append(cln.errs, restErr)
	if cond(err, restErr) {
		WipeDir(cln.baseDir)
	}
}

func (cln *cleanerT) Up(relDirs ...string) {
	err := errs.Join(cln.errs...)
	if err == nil {
		wipeDirs(cln.baseDir)(relDirs...)
	}
}

func wipeDirs(baseDir path.AbsDir) func(dirs ...string) {
	return func(dirs ...string) {
		for _, dirS := range dirs {
			WipeDir(baseDir.Join(dirS).AsDir())
		}
		WipeDir(baseDir)
	}
}

func WipeDir(dir path.AbsDir) {
	must.Ok(os.RemoveAll(dir.String()), os.ErrNotExist)
}
