package findup

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

var ErrDirWithEntryNotFound = errors.New("parent dir with entry not found")

// GetDirOfEntry returns an absolute path of a folder containing `fsEntry` found inside `startDir` or its parents
func GetDirOfEntry(startDir path.AbsDir, fsEntry string, noSymLinks bool) (dir path.AbsDir, errOut error) {
	defer errs.DeferWrapf(&errOut, "findup: get dir of %q in %q", fsEntry, startDir)
	if startDir.IsEmpty() {
		startDir = getCurrentDir(noSymLinks)
	}

	var err error
	var childDir path.AbsDir
	dir = startDir.ExpandHome().AsDir()
	for dir != childDir {
		fsEntryPath := dir.Join(fsEntry).Abs()
		//slog.Info("findup check", "entry path", fsEntryPath)
		_, err = fsEntryPath.Stat()
		if err == nil {
			return dir, nil
		}
		childDir = dir
		dir = dir.AbsDir()
	}
	return path.AbsDir{}, fmt.Errorf("get dir of %q in %q: %w", fsEntry, startDir, ErrDirWithEntryNotFound)
}

// GetPathOfEntry returns an absolute path of a file or folder found inside `startDir` or its parents
func GetPathOfEntry(startDir path.AbsDir, fsEntry string, noSymLinks bool) (p path.AbsPath, errOut error) {
	defer errs.DeferWrapf(&errOut, "findup: get path of %q in %q", fsEntry, startDir)
	dir, err := GetDirOfEntry(startDir, fsEntry, noSymLinks)
	if err != nil {
		return path.AbsPath{}, err
	}
	return dir.Join(fsEntry).Abs(), nil
}

func getCurrentDir(noSymLinks bool) path.AbsDir {
	cwd := path.New(must.Ok1(os.Getwd())).AsDir()
	if !noSymLinks || os.Getenv("FIND_SUB_IN_PATH_FOLLOW_SYMLINKS") == "true" {
		cwd = must.Ok1(cwd.EvalSymlinks()).AsDir()
	}
	return cwd
}

// Deprecated: use GetDirOfEntry or GetPathOfEntry instead
func GetParentDirWithEntry(startDirS, fsEntry string, join, noSymLinks bool) (string, error) {
	if startDirS == "" {
		startDirS = getCurDir(noSymLinks)
	}
	startDirS = MustS(filepath.Abs(startDirS))

	var err error
	childDirS := ""
	dirS := startDirS
	for dirS != childDirS {
		myArgs := append([]string{dirS}, fsEntry)
		fsEntryPath := filepath.Join(myArgs...)
		_, err = os.Stat(fsEntryPath)
		if err == nil {
			if join {
				return fsEntryPath, nil
			}
			return dirS, nil
		}
		childDirS = dirS
		dirS = filepath.Dir(childDirS)
	}
	return "", fmt.Errorf("FAILED to find %q in path %q: %w", fsEntry, startDirS, err)
}

func getCurDir(noSymLinks bool) string {
	dirS := MustS(os.Getwd())
	if !noSymLinks || os.Getenv("FIND_SUB_IN_PATH_FOLLOW_SYMLINKS") == "true" {
		dirS = MustS(filepath.EvalSymlinks(dirS))
	}
	return dirS
}

// Deprecated: use `must.Ok1` instead
func MustS(str string, err error) string {
	if err != nil {
		panic(err)
	}
	return str
}
