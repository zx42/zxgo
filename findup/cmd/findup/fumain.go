package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/alecthomas/kong"

	"gitlab.com/zx42/zxgo/findup"
)

var (
	CLI struct {
		FsEntry          string `arg name:"look-for" help:"Filesystem entry to look for in any parent folder"`
		Dir              string `help:"Folder to start search in. Default is the current directory" type:"path"`
		Join             bool   `help:"print the found parent folder and the searched entry together as one path"`
		Slash            bool   `help:"print the result path with slashes"`
		NoFollowSymLinks bool   `help:"do NOT follow symbolic links when walking up the path"`
	}
)

func main() {
	kong.Parse(&CLI,
		kong.Name("find_sub_in_path"),
		kong.Description("A tool for finding a parent folder which contains a certain entry."),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
			Summary: true,
		}))
	// fmt.Printf("CLI: FsEntry=%q NoFollowSymLinks=%v ctx = %#v\n", CLI.FsEntry, CLI.NoFollowSymLinks, ctx)
	parentDirS, err := findup.GetParentDirWithEntry(CLI.Dir, CLI.FsEntry, CLI.Join, CLI.NoFollowSymLinks)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(2)
	}
	if CLI.Slash {
		parentDirS = filepath.ToSlash(parentDirS)
	}
	fmt.Println(parentDirS)
}
