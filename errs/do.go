package errs

func Do(errp *error, call func() error) {
	if *errp == nil {
		*errp = call()
	}
}

func Do1[T1 any](err *error, call func() (T1, error)) (t T1) {
	if *err == nil {
		t, *err = call()
	}
	return t
}

func Do2[T1, T2 any](err *error, call func() (T1, T2, error)) (t1 T1, t2 T2) {
	if *err == nil {
		t1, t2, *err = call()
	}
	return t1, t2
}

func Do3[T1, T2, T3 any](err *error, call func() (T1, T2, T3, error)) (t1 T1, t2 T2, t3 T3) {
	if *err == nil {
		t1, t2, t3, *err = call()
	}
	return t1, t2, t3
}
