package errs

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/zx42/zxgo/slice"
)

type (
	errStr string
)

// Wrapf returns nil if `err` == nil or `err` wrapped with `msgFmtS` and `args`.
// if `msgFmtS` does not contain '%w' it will be added at the end and
// `err` will be appended to `args`
func Wrapf(err error, msgFmtS string, args ...any) error {
	if err == nil {
		return nil
	}
	if !strings.Contains(msgFmtS, "%w") {
		msgFmtS += ": %w"
		args = append(args, err)
	}
	return fmt.Errorf(msgFmtS, args...)
}

// DeferWrapf wraps an error using `Wrapf` and should be called in a defer at the beginning of a function
func DeferWrapf(errOut *error, msgFmtS string, args ...any) {
	*errOut = Wrapf(*errOut, msgFmtS, args...)
}

// Deprecated: use `fmt.Errorf` instead
func ErrStr(s string) error {
	if s == "" {
		return nil
	}
	return errStr(s)
}

func (es errStr) Error() string {
	return string(es)
}

func IgnoreErrors(err error, exceptIsAsErrs ...any) error {
	exceptIsErrs, exceptAsErrs := slice.FilterByType[error](exceptIsAsErrs)
	err = FilterError(err, exceptIsErrs...)(exceptAsErrs...)
	return err
}

// second argument list should be pointers to types that implement error
func FilterError(err error, exceptIsErrs ...error) func(exceptAsErrs ...any) error {
	if err == nil {
		return noError
	}

	for _, e := range exceptIsErrs {
		if errors.Is(err, e) {
			return noError
		}
	}

	return func(exceptAsErrs ...any) error {
		for _, e := range exceptAsErrs {
			if errors.As(err, e) {
				return nil
			}
		}
		return err
	}
}

func noError(exceptAsErrs ...any) error {
	return nil
}

//type comparableError interface {
//	error
//	comparable
//}
//
//func Join[ErrT comparableError](errs ...ErrT) error {
//	nonNilErrors := make([]error, 0, len(errs))
//	for _, err := range errs {
//		if err != nil {
//			nonNilErrors = append(nonNilErrors, err)
//		}
//	}
//	if len(nonNilErrors) == 0 {
//		return nil
//	}
//	return errors.Join(nonNilErrors...)
//}

func Join(errs ...error) error {
	nonNilErrors := make([]error, 0, len(errs))
	for _, err := range errs {
		//if fmt.Sprintf("%v",err)!="<nil>"{
		//	nonNilErrors = append(nonNilErrors, err)
		//}
		if err != nil {
			x := fmt.Sprintf("%v", err)
			if x != "<nil>" && x != "" {
				nonNilErrors = append(nonNilErrors, err)
			}
		}
	}
	if len(nonNilErrors) == 0 {
		return nil
	}
	if len(nonNilErrors) == 1 {
		return nonNilErrors[0]
	}
	return errors.Join(nonNilErrors...)
}

func Unwrap(err error) []error {
	if err != nil {
		type unwrapper interface {
			Unwrap() []error
		}
		euw, ok := err.(unwrapper)
		if ok {
			return euw.Unwrap()
		}
	}
	return nil
}
