package must

import (
	"path/filepath"
)

//var (
//	FollowSymlinks = true
//	ExpandTilde    = true
//	ToSlash        = true
//)

//func Exist(fname path.Path, name string) {
//	if !path.Exists(fname) {
//		panic(fmt.Sprintf("[%s] file or dir %q does not exist", name, fname))
//	}
//}

//func Abs(fpathElems ...string) string {
//	absPath := absClean(filepath.Join(fpathElems...))
//	if FollowSymlinks {
//		absPath = absFollowSymlinks(absPath)
//	}
//	if ExpandTilde {
//		absPath = internal.ExpandHome(absPath)
//	}
//	if ToSlash {
//		absPath = filepath.ToSlash(absPath)
//	}
//	return absPath
//}

//func absClean(fpath string) string {
//	abs := Str(filepath.Abs(fpath))
//	clean := filepath.Clean(abs)
//	if runtime.GOOS == "windows" {
//		clean = strings.ToUpper(clean[:1]) + clean[1:]
//		//} else {
//		//	panic(runtime.GOOS)
//	}
//	return clean
//}

////func absFollowSymlinks(fpathElems ...string) string {
////	joined := filepath.Join(fpathElems...)
//func absFollowSymlinks(fpath path.Path) path.Path {
//	//abs1 := abs(joined)
//	if !path.Exists(fpath) {
//		return fpath
//	}
//	if runtime.GOOS == "windows" && len(fpath) > 250 {
//		return fpath
//	}
//
//	symlinks, err := filepath.EvalSymlinks(string(fpath))
//	woSymLinks := Str(symlinks, err, "filepath.EvalSymlinks(%q)", fpath)
//	return path.Path(woSymLinks)
//	//abs2 := abs(woSymLinks)
//	//return abs2
//}

////func absExpand(fpathElems ...string) string {
////	fpath := filepath.Join(fpathElems...)
//	func absExpand(fpath string) string {
//	expanded := expandHome(fpath)
//	absDir := must.Abs(expanded)
//	withSlash := filepath.ToSlash(absDir)
//	log.New(false).
//		Log("Abs(%q) fpath=%q expanded=%q absDir=%q withSlash=%q",
//			fpathElems, fpath, expanded, absDir, withSlash)
//	return withSlash
//}

func Rel(basePath, targetPath string) string {
	rel := Str(filepath.Rel(basePath, targetPath))
	withSlash := filepath.ToSlash(rel)
	return withSlash
}
