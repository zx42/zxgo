package must

import (
	"errors"
)

type (
//	pointer[T any] interface {
//		*T
//	}
//
//	nilable[T any] interface {
//		*T | []T
//	}
//
//	nilable[T any, K comparable] interface {
//		*T | []T | map[K]T
//	}
)

var (
	ErrNil = errors.New("is nil")
)

func NonNil[T any](t *T, args ...any) *T {
	if t == nil {
		Ok(ErrNil, args...)
	}
	return t
}

//func NonNil[T nilable[T, K], K comparable](t T, args ...any) T {
//	if t == nil {
//		Ok(ErrNil, args...)
//	}
//	return t
//}
