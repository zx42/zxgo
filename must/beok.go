package must

import (
	"fmt"
	"strings"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/slice"
)

type (
	beOkT struct {
		val interface{}
		err error
	}
)

// 1. Ok() does nothing if err == nil
// 2. Otherwise if no args are given Ok() will panic(err)
// 3. Otherwise the first args can be error's. if err Is one of them Ok() does nothing
// 4. Otherwise the next arg can be a format string for fmt.Sprintf(). If so it will be passed with the remaining args to Sprintf() and Ok() will panic with that message
// 5. Otherwise Ok() will panic with fmt.Sprint(args...)
func Ok(err error, args ...interface{}) {
	exceptIsErrs, args := slice.SplitByType[error](args)
	exceptAsErrs, args := slice.SplitByFunc(args, func(a any) bool { _, ok := a.(*error); return ok })
	err = errs.FilterError(err, exceptIsErrs...)(exceptAsErrs...)
	OrPanic(err, args)
}

func Val(v interface{}, err error) beOkT {
	return beOkT{
		val: v,
		err: err,
	}
}

func (bo beOkT) Except(exceptErrs ...error) beOkT {
	return beOkT{
		val: bo.val,
		err: errs.FilterError(bo.err, exceptErrs...)(),
	}
}

func (bo beOkT) OrPanic(args ...interface{}) interface{} {
	OrPanic(bo.err, args)
	return bo.val
}

// 1. OrPanic() does nothing if err == nil
// 2. Otherwise if no args are given OrPanic() will panic(err)
// 3. Otherwise the first arg can be a format string for fmt.Sprintf(). If so it will be passed with the remaining args to Sprintf() and OrPanic() will panic with that message
// 4. Otherwise OrPanic() will panic with fmt.Sprint(args...)
func OrPanic(err error, args []interface{}) {
	OrDo(err, args, func(x any) { panic(x) })
}

func OrDo(err error, args []interface{}, handleMessage func(any)) {
	if err == nil {
		return
	}

	if len(args) == 0 {
		handleMessage(err)
		return
	}

	var msg any = "ERROR"
	fmtS, ok := args[0].(string)
	if ok {
		msg = fmt.Errorf(fmtS, args...)
		if !strings.Contains(fmtS, "%w") {
			argsL := append(args[1:], err)
			msg = fmt.Errorf(fmtS+": %w", argsL...)
		}
	} else {
		argsL := append(args, err)
		msg = fmt.Sprint(argsL...)
	}
	handleMessage(msg)
}
