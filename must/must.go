package must

func Ok1[R any](retval R, err error, args ...interface{}) R {
	Ok(err, args...)
	return retval
}

func Ok2[R1 any, R2 any](retval1 R1, retval2 R2, err error, args ...interface{}) (R1, R2) {
	Ok(err, args...)
	return retval1, retval2
}

func Ok3[R1, R2, R3 any](retval1 R1, retval2 R2, retval3 R3, err error, args ...interface{}) (R1, R2, R3) {
	Ok(err, args...)
	return retval1, retval2, retval3
}

func Ok4[R1, R2, R3, R4 any](retval1 R1, retval2 R2, retval3 R3, retval4 R4, err error, args ...interface{}) (R1, R2, R3, R4) {
	Ok(err, args...)
	return retval1, retval2, retval3, retval4
}

func Ok5[R1, R2, R3, R4, R5 any](retval1 R1, retval2 R2, retval3 R3, retval4 R4, retval5 R5, err error, args ...interface{}) (R1, R2, R3, R4, R5) {
	Ok(err, args...)
	return retval1, retval2, retval3, retval4, retval5
}

func Str(s string, err error, args ...interface{}) string {
	Ok(err, args...)
	return s
}

func Int(n int, err error, args ...interface{}) int {
	Ok(err, args...)
	return n
}

func Func(errFunc func() error, args ...interface{}) {
	Ok(errFunc(), args...)
}
