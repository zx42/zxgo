package must

import (
	"fmt"
)

func BeTrue(ok bool, args ...any) {
	if ok {
		return
	}
	Ok(fmt.Errorf("false"), args...)
}
