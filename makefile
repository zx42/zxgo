# all: findup fswatcher resty
all: findup # resty
	go test ./...

findup: updatemod
	cd findup && $(MAKE)

# fswatcher: updatemod
# 	cd fswatcher && $(MAKE)

# resty: updatemod
# 	cd resty && $(MAKE)

updatemod:
	go mod tidy
