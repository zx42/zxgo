package hash

import (
	"crypto/sha512"
	"encoding/hex"

	"gitlab.com/zx42/zxgo/must"
)

func HashSha512AsHex(data []byte, maxSize int) string {
	h := sha512.New()
	n := must.Ok1(h.Write(data))
	if n != len(data) {
		panic("oops")
	}
	hashS := hex.EncodeToString(h.Sum(nil))
	if maxSize < len(hashS) {
		return hashS[:maxSize]
	}
	return hashS
}
