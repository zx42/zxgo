package cmd

import (
	"fmt"
	"os/exec"
)

func ExternalSubCmd(fileExtensions ...string) func(prefix string, clArgs []string) *exec.Cmd {
	return func(prefix string, clArgs []string) *exec.Cmd {
		subCmd := clArgs[0]
		for _, ext := range fileExtensions { // []string{".exe", ".sh"} {
			exeName := fmt.Sprintf("%s%s%s", prefix, subCmd, ext)
			exePath, err := exec.LookPath(exeName)
			if err != nil {
				continue
			}
			cmd := exec.Command(exePath, clArgs[1:]...)
			// cmd.Stdout = os.Stdout
			// cmd.Stderr = os.Stderr
			// err = cmd.Run()
			// if err != nil {
			// 	err = fmt.Errorf("run %q: %w", clArgs, err)
			// 	log.Println("ERROR: ", err.Error())
			// }
			return cmd
		}
		return nil
	}
}
