package osfs_future_refactor

import (
	"bytes"
	"testing"
)

func Test_deepCompare(t *testing.T) {
	type args struct {
		file1     string
		file2     string
		chunkSize int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
		{name: "equal files", args: args{
			file1:     "hello",
			file2:     "hello",
			chunkSize: 3,
		}, want: true},
		{name: "non equal files", args: args{
			file1:     "hello",
			file2:     "hell no",
			chunkSize: 3,
		}, want: false},
		{name: "one empty file", args: args{
			file1:     "hello",
			file2:     "",
			chunkSize: 3,
		}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f1 := bytes.NewBufferString(tt.args.file1)
			f2 := bytes.NewBufferString(tt.args.file2)
			if got := deepCompare(f1, f2, tt.args.chunkSize); got != tt.want {
				t.Errorf("deepCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}
