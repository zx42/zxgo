package osfs_future_refactor

import (
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"

	"gitlab.com/zx42/zxgo/path"
)

func CopyAll(srcDir, tgtDir path.AbsDir) (errOut error) {
	report := func(err error) {
		if err != nil {
			slog.Error("copy file failed", "err", err)
			if errOut == nil {
				errOut = err
			}
		}
	}
	fsys := os.DirFS(srcDir.String())
	err := fs.WalkDir(fsys, ".", func(relPathS string, d fs.DirEntry, err error) error {
		if err != nil {
			report(err)
			return nil
		}
		if d.IsDir() {
			return nil
		}
		// srcFPath := srcDir.Join(path).Abs()
		// tgtFPath := tgtDir.Join(path).Abs()
		srcFPath := srcDir.Join(relPathS).(path.AbsPath)
		tgtFPath := tgtDir.Join(relPathS).(path.AbsPath)
		err = Copy(srcFPath, tgtFPath)
		if err != nil {
			report(err)
			return nil
		}
		return nil
	})
	return err
}

func Copy(srcFPath, tgtFPath path.AbsPath) (errOut error) {
	_, _, err := RenameToBackup(tgtFPath)
	if err != nil {
		err = fmt.Errorf("copy file: rename target: %w", err)
		return err
	}
	srcF, err := srcFPath.Open()
	if err != nil {
		err = fmt.Errorf("copy file: open source: %w", err)
		return err
	}
	close := func(f io.Closer) {
		err := f.Close()
		if errOut == nil {
			errOut = err
		}
	}
	defer close(srcF)
	tgtF, err := tgtFPath.Create()
	if err != nil {
		err = fmt.Errorf("copy file: create target: %w", err)
		return err
	}
	defer close(tgtF)

	_, err = io.Copy(tgtF, srcF)
	if err != nil {
		err = fmt.Errorf("copy file %q -> %q: %w", srcFPath.String(), tgtFPath.String(), err)
		return err
	}

	return nil
}
