package osfs_future_refactor

import (
	"fmt"
	"io/fs"
	"os"

	"gitlab.com/zx42/zxgo/path"
	"gitlab.com/zx42/zxgo/path/files"
)

type (
	//Path interface {
	//	path.Path
	//
	//	// EvalSymlinks() (Path, error)
	//	Stat() (fs.FileInfo, error)
	//	Open() (*os.File, error)
	//	Create() (*os.File, error)
	//	Exists() bool
	//	// RenameTo(newPath Path) error
	//	ReadFile() ([]byte, error)
	//	WriteFile(data []byte, perm fs.FileMode) error
	//}

	natPath struct {
		path.Path
	}
	AbsPath struct {
		path.AbsPath
	}
	AbsDir struct {
		path.AbsDir
	}
)

// func (np natPath) EvalSymlinks() (Path, error) {
// 	// TODO implement me
// 	panic("implement me")
// }

// func (np natPath) EvalSymlinks() (Path, error) {
// 	woSymLinks, err := filepath.EvalSymlinks(np.String())
// 	if err != nil {
// 		return np, fmt.Errorf("EvalSymlinks(): %w", err)
// 	}
// 	return natPath{Path: path.New(woSymLinks)}, nil
// }
//
// func (ad AbsDir) IoFsFS() fs.FS {
// 	return os.DirFS(ad.String())
// }

//func (np natPath) Stat() (fs.FileInfo, error) {
//	fi, err := os.Stat(np.String())
//	return fi, err
//}
//
//func (np natPath) Open() (*os.File, error) {
//	of, err := os.Open(np.String())
//	return of, err
//}
//
//func (np natPath) Create() (*os.File, error) {
//	err := AbsDir{np.Dir()}.MkdirAll(0777)
//	if err != nil {
//		return nil, fmt.Errorf("create %q: %w", np, err)
//	}
//	cf, err := os.Create(np.String())
//	return cf, err
//}
//
//func (np natPath) Exists() bool {
//	return Exists(np)
//}
//
//func (np natPath) RenameTo(newPath Path) error {
//	err := AbsDir{newPath.Dir()}.MkdirAll(0777)
//	if err != nil {
//		return fmt.Errorf("rename %q >> %q: mkdir parent: %w", np, newPath, err)
//	}
//	err = os.Rename(np.String(), newPath.String())
//	return err
//}

func (np natPath) RenameToBackup(fpath path.Path) (
	// nextFreeFPath Path, i int,
	err error,
) {
	_, _, err = files.RenameToBackup(fpath)
	return err
}

func (np natPath) ReadFile() ([]byte, error) {
	contB, err := os.ReadFile(np.String())
	return contB, err
}

func (np natPath) WriteFile(data []byte, perm fs.FileMode) error {
	if err := path.EnsureParentFolderExists(np); err != nil {
		return fmt.Errorf("write file %q: %w", np, err)
	}
	err := os.WriteFile(np.String(), data, perm)
	return err
}

func (ad AbsDir) MkdirAll(perm os.FileMode) error {
	return os.MkdirAll(ad.String(), perm)
}

// func (ap AbsPath) RenameTo(newPath AbsPath) error {
// 	err := newPath.Dir().MkdirAll(0777)
// 	if err != nil {
// 		return fmt.Errorf("rename %q >> %q: mkdir parent: %w", ap.string, newPath.string, err)
// 	}
// 	err = os.Rename(ap.string, newPath.string)
// 	return err
// }

func (ap AbsPath) Remove() error {
	return Remove(ap)
}

func (ap AbsPath) SymlinkTo(tgtPath path.AbsPath) error {
	if err := path.EnsureParentFolderExists(ap); err != nil {
		return fmt.Errorf("symlink %q >> %q: mkdir parent: %w", ap, tgtPath, err)
	}
	err := os.Symlink(tgtPath.String(), ap.String())
	if err != nil {
		return fmt.Errorf("symlink %q >> %q: %w", ap, tgtPath, err)
	}
	return nil
}

func Exists(dir path.Path, fnames ...string) bool {
	_, err := Stat(dir, fnames...)
	return err == nil
}

func Stat(dir path.Path, fnames ...string) (fpath path.AbsPath, err error) {
	fpath = dir.Join(fnames...).(path.AbsPath)
	_, err = os.Stat(fpath.String())
	return fpath, err
}

func Remove(f path.Path) error {
	err := os.Remove(f.String())
	return err
}
