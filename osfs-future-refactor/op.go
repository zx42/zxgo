package osfs_future_refactor

// func (p path.path) Stat() (fs.FileInfo, error) {
// 	fi, err := os.Stat(p.string)
// 	return fi, err
// }
//
// func (p path.path) Open() (*os.File, error) {
// 	of, err := os.Open(p.string)
// 	return of, err
// }
//
// func (p path.path) Create() (*os.File, error) {
// 	err := p.Dir().MkdirAll(0777)
// 	if err != nil {
// 		return nil, fmt.Errorf("create %q: %w", p.string, err)
// 	}
// 	cf, err := os.Create(p.string)
// 	return cf, err
// }
//
// func (p path.path) Exists() bool {
// 	return Exists(p)
// }
//
// func (p path.path) RenameTo(newPath path.Path) error {
// 	err := newPath.Dir().MkdirAll(0777)
// 	if err != nil {
// 		return fmt.Errorf("rename %q >> %q: mkdir parent: %w", p.string, newPath, err)
// 	}
// 	err = os.Rename(p.string, newPath.String())
// 	return err
// }
//
// func (p path.path) RenameToBackup(fpath path.Path) (
// 	// nextFreeFPath Path, i int,
// 	err error,
// ) {
// 	_, _, err = file.RenameToBackup(fpath)
// 	return err
// }
//
// func (p path.path) ReadFile() ([]byte, error) {
// 	contB, err := os.ReadFile(p.string)
// 	return contB, err
// }
//
// func (p path.path) WriteFile(data []byte, perm fs.FileMode) error {
// 	err := p.Dir().MkdirAll(0777)
// 	if err != nil {
// 		return fmt.Errorf("write file %q: %w", p.string, err)
// 	}
// 	err = os.WriteFile(p.string, data, perm)
// 	return err
// }
//
// func (ad path.AbsDir) MkdirAll(perm os.FileMode) error {
// 	return os.MkdirAll(ad.string, perm)
// }
//
// // func (ap AbsPath) RenameTo(newPath AbsPath) error {
// // 	err := newPath.Dir().MkdirAll(0777)
// // 	if err != nil {
// // 		return fmt.Errorf("rename %q >> %q: mkdir parent: %w", ap.string, newPath.string, err)
// // 	}
// // 	err = os.Rename(ap.string, newPath.string)
// // 	return err
// // }
//
// func (ap path.AbsPath) Remove() error {
// 	return Remove(ap.path)
// }
//
// func (ap path.AbsPath) SymlinkTo(tgtPath path.AbsPath) error {
// 	err := ap.Dir().MkdirAll(0777)
// 	if err != nil {
// 		return fmt.Errorf("symlink %q >> %q: mkdir parent: %w", ap.string, tgtPath.string, err)
// 	}
// 	err = os.Symlink(tgtPath.string, ap.string)
// 	if err != nil {
// 		return fmt.Errorf("symlink %q >> %q: %w", ap.string, tgtPath.string, err)
// 	}
// 	return nil
// }
//
// func Exists(dir path.Path, fnames ...string) bool {
// 	_, err := Stat(dir, fnames...)
// 	return err == nil
// }
//
// func Stat(dir path.Path, fnames ...string) (fpath path.AbsPath, err error) {
// 	fpath = dir.Join(fnames...).(path.AbsPath)
// 	_, err = os.Stat(fpath.string)
// 	return fpath, err
// }
//
// func Remove(f path.Path) error {
// 	err := os.Remove(f.String())
// 	return err
// }
