package resty

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/go-resty/resty/v2"
	"gopkg.in/yaml.v3"
)

type (
	cacheT struct {
		writeDir string
		readDir  string
		Protocol string
	}

	cacheEntryT struct {
		Request  *myRequestT
		Response *myResponseT
	}

	myRequestT struct {
		Method string
		Url    string
		Header http.Header `yaml:"header,flow"`
		Query  url.Values  `yaml:"query,omitempty,flow"`
	}

	myResponseT struct {
		Status     string // e.g. "200 OK"
		StatusCode int    // e.g. 200
		Proto      string `yaml:"proto,omitempty" json:"proto,omitempty"` // e.g. "HTTP/1.0"
		//ProtoMajor int    // e.g. 1
		//ProtoMinor int    // e.g. 0

		// Header maps header keys to values. If the response had multiple
		// headers with the same key, they may be concatenated, with comma
		// delimiters.  (RFC 7230, section 3.2.2 requires that multiple headers
		// be semantically equivalent to a comma-delimited sequence.) When
		// Header values are duplicated by other fields in this struct (e.g.,
		// ContentLength, TransferEncoding, Trailer), the field values are
		// authoritative.
		//
		// Keys in the map are canonicalized (see CanonicalHeaderKey).
		Header http.Header `yaml:"header,flow"`

		// Body represents the response body.
		//
		// The response body is streamed on demand as the Body field
		// is read. If the network connection fails or the server
		// terminates the response, Body.Read calls return an error.
		//
		// The http Client and Transport guarantee that Body is always
		// non-nil, even on responses without a body or responses with
		// a zero-length body. It is the caller's responsibility to
		// close Body. The default HTTP client's Transport may not
		// reuse HTTP/1.x "keep-alive" TCP connections if the Body is
		// not read to completion and closed.
		//
		// The Body is automatically dechunked if the server replied
		// with a "chunked" Transfer-Encoding.
		//
		// As of Go 1.12, the Body will also implement io.Writer
		// on a successful "101 Switching Protocols" response,
		// as used by WebSockets and HTTP/2's "h2c" mode.
		Body    interface{}
		BodyRaw string

		// ContentLength records the length of the associated content. The
		// value -1 indicates that the length is unknown. Unless Request.Method
		// is "HEAD", values >= 0 indicate that the given number of bytes may
		// be read from Body.
		ContentLength int64

		// Contains transfer encodings from outer-most to inner-most. Value is
		// nil, means that "identity" encoding is used.
		TransferEncoding []string `yaml:"transfer-encoding,omitempty" json:"transfer-encoding,omitempty"`

		// Close records whether the header directed that the connection be
		// closed after reading Body. The value is advice for clients: neither
		// ReadResponse nor Response.Write ever closes a connection.
		Close bool `yaml:"close,omitempty" json:"close,omitempty"`

		// Uncompressed reports whether the response was sent compressed but
		// was decompressed by the http package. When true, reading from
		// Body yields the uncompressed content instead of the compressed
		// content actually set from the server, ContentLength is set to -1,
		// and the "Content-Length" and "Content-Encoding" fields are deleted
		// from the responseHeader. To get the original response from
		// the server, set Transport.DisableCompression to true.
		Uncompressed bool `yaml:"uncompressed,omitempty" json:"uncompressed,omitempty"`

		// Trailer maps trailer keys to values in the same
		// format as Header.
		//
		// The Trailer initially contains only nil values, one for
		// each key specified in the server's "Trailer" header
		// value. Those values are not added to Header.
		//
		// Trailer must not be accessed concurrently with Read calls
		// on the Body.
		//
		// After Body.Read has returned io.EOF, Trailer will contain
		// any trailer values sent by the server.
		Trailer http.Header `yaml:"trailer,omitempty,flow" json:"trailer,omitempty"`

		// Request is the request that was sent to obtain this Response.
		// Request's Body is nil (having already been consumed).
		// This is only populated for Client requests.
		//Request *Request

		// TLS contains information about the TLS connection on which the
		// response was received. It is nil for unencrypted responses.
		// The pointer is shared between responses and should not be
		// modified.
		TLS *tls.ConnectionState `yaml:"tls,omitempty" json:"tls,omitempty"`
	}
)

func (c *cacheT) LogRequest(nrq *NetRequest, err error, rsp Response) error {
	if c.writeDir == "" || err != nil {
		return err
	}

	url := nrq.Request.RawRequest.URL
	fpath, err := c.getLogFName(c.writeDir, nrq.Method, url, ".yaml")
	if err != nil {
		return fmt.Errorf("get log filename: %w", err)
	}

	contLenI64, _ := strconv.ParseInt(rsp.Header().Get("Content-Length"), 10, 64)
	data := cacheEntryT{
		Request: &myRequestT{
			Method: nrq.Method,
			Url:    nrq.URL,
			Header: nrq.Header,
			Query:  url.Query(),
		},
		Response: &myResponseT{
			Status:     rsp.Status(),
			StatusCode: rsp.StatusCode(),
			Header:     rsp.Header(),
			//Body:             nil,
			BodyRaw:       rsp.BodyString(),
			ContentLength: contLenI64,
			//TransferEncoding: nil,
			//Close:            false,
			//Uncompressed:     false,
			//Trailer:          nil,
			//Request:          nil,
			//TLS:              nil,
		},
	}
	var bodyJson interface{}
	err = rsp.DeJSON(&bodyJson)
	if err == nil {
		data.Response.Body = bodyJson
	}
	//jsonB, err := json.MarshalIndent(data, "", "  ")
	jsonB, err := yaml.Marshal(data)
	if err != nil {
		return fmt.Errorf("marshal request+response as json: %w", err)
	}
	//fpath += ".yaml"
	err = os.WriteFile(fpath, jsonB, 0700)
	if err != nil {
		return fmt.Errorf("writing json file %q: %w", fpath, err)
	}
	return nil
}

func (c *cacheT) LoadResponse(method string, url *url.URL) (Response, error) {
	if c.readDir == "" {
		return nil, fmt.Errorf("missing read cache dir")
	}
	// logging
	fpath, err := c.getLogFName(c.readDir, method, url, ".yaml")
	if err != nil {
		return nil, fmt.Errorf("get cache filename: %w", err)
	}
	//fpath += ".yaml"
	cacheB, err := os.ReadFile(fpath)
	if err != nil {
		return nil, fmt.Errorf("read cache file %q: %w", fpath, err)
	}
	var cacheEntry cacheEntryT
	err = yaml.Unmarshal(cacheB, &cacheEntry)
	if err != nil {
		return nil, fmt.Errorf("unmarshal cache file %q: %w", fpath, err)
	}
	rsp := cacheEntry.Response
	bodyBuf := io.NopCloser(bytes.NewBufferString(rsp.BodyRaw))
	nrsp := &NetResponse{
		Response: &resty.Response{
			//Request: nrq.Request,
			RawResponse: &http.Response{
				Status:     rsp.Status,
				StatusCode: rsp.StatusCode,
				//Proto:            "",
				//ProtoMajor:       0,
				//ProtoMinor:       0,
				Header:        rsp.Header,
				Body:          bodyBuf,
				ContentLength: rsp.ContentLength,
				//TransferEncoding: nil,
				//Close:            false,
				//Uncompressed:     false,
				//Trailer:          nil,
				//Request:          nil,
				//TLS:              nil,
			},
		},
	}
	return nrsp, nil
}

func (c *cacheT) getLogFName(baseDir string, method string, url *url.URL, fext string) (string, error) {
	return GetLogFName(c.Protocol, baseDir, method, url, fext)
	//host := strings.ReplaceAll(url.Host, ":", "__")
	//dirName := fmt.Sprintf("%s__%s", host, url.Scheme)
	//prot := c.Protocol
	//if prot == "" {
	//	prot = "unknown"
	//}
	//dir := filepath.Join(baseDir, prot, dirName)
	//err := os.MkdirAll(dir, 0700)
	//if err != nil {
	//	return "", fmt.Errorf("mkdir %q: %w", dir, err)
	//}
	//cpath := strings.ReplaceAll(url.Path, "/", "__")
	////fname := fmt.Sprintf("%s_%s.json",
	//fname := fmt.Sprintf("%s_%s%s",
	//	method, strings.TrimLeft(cpath, "_"), fext)
	//fpath := filepath.Join(dir, fname)
	//return fpath, nil
}

func GetLogFName(protocol, baseDir string, method string, url *url.URL, fext string) (string, error) {
	//host := strings.ReplaceAll(url.Host, ":", "__")
	//dirName := fmt.Sprintf("%s__%s", host, url.Scheme)
	////prot := c.Protocol
	//if protocol == "" {
	//	protocol = "unknown"
	//}
	//dir := filepath.Join(baseDir, protocol, dirName)
	//err := os.MkdirAll(dir, 0700)
	dir, err := GetLogDirName(protocol, baseDir, url)
	if err != nil {
		return "", fmt.Errorf("mkdir %q: %w", dir, err)
	}
	cpath := strings.ReplaceAll(url.Path, "/", "__")
	//fname := fmt.Sprintf("%s_%s.json",
	fname := fmt.Sprintf("%s_%s%s",
		method, strings.TrimLeft(cpath, "_"), fext)
	fpath := filepath.Join(dir, fname)
	return fpath, nil
}

func GetLogDirName(protocol, baseDir string, url *url.URL) (string, error) {
	host := strings.ReplaceAll(url.Host, ":", "__")
	dirName := fmt.Sprintf("%s__%s", host, url.Scheme)
	//prot := c.Protocol
	if protocol == "" {
		protocol = "unknown"
	}
	dir := filepath.Join(baseDir, protocol, dirName)
	err := os.MkdirAll(dir, 0700)
	if err != nil {
		return "", fmt.Errorf("mkdir %q: %w", dir, err)
	}
	return dir, nil
}
