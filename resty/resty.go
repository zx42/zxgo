package resty

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"

	resty "github.com/go-resty/resty/v2"
)

type (
	Client interface {
		InsecureSkipHttpsVerify() Client
		SetTLSClientConfig(tlsCfg *tls.Config) Client
		Request() Request
		GetClient() *http.Client
	}
	Request interface {
		Get(url string) (Response, error)
		Post(url string) (Response, error)
		Do(method, url string) (Response, error)
		SetProtocol(prot string) Request
		SetHeader(key string, value string) Request
		AddHeaders(headers http.Header) Request
		SetHeaders(headersM map[string]string) Request
		SetCookie(name, value string) Request
		SetBody(body interface{}) Request
		SetBodyJson(body interface{}) Request
	}
	Response interface {
		StatusCode() int
		Status() string
		Body() []byte
		BodyString() string
		Header() http.Header
		DeJSON(tgtData interface{}) error
	}

	NetClient struct {
		*resty.Client
		cache *cacheT
	}
	NetRequest struct {
		*resty.Request
		err          error
		httpMethodsM map[string]httpMethodF
		cache        *cacheT
	}
	NetResponse struct {
		*resty.Response
	}

	httpMethodF func(url string) (*resty.Response, error)
)

func NewNetClient() Client {
	return newNetClient("", "")
}

func NewLoggingNetClient(writeLogDir string) Client {
	return newNetClient(writeLogDir, "")
}

func NewMockNetClient(readLogDir string) Client {
	return newNetClient("", readLogDir)
}

func newNetClient(writeLogDir, readLogDir string) Client {
	cache := &cacheT{
		writeDir: writeLogDir,
		readDir:  readLogDir,
	}
	return &NetClient{
		Client: resty.New(),
		cache:  cache,
	}
}

//func NewNetClient(writeLogDir, readLogDir string) Client {
//	cache := &cacheT{
//		writeDir: writeLogDir,
//		readDir:  readLogDir,
//	}
//	return &NetClient{
//		Client: resty.New(),
//		cache:  cache,
//	}
//}

func (nc *NetClient) SetTLSClientConfig(tlsCfg *tls.Config) Client {
	nc.Client.SetTLSClientConfig(tlsCfg)
	return nc
}

func (nc *NetClient) InsecureSkipHttpsVerify() Client {
	nc.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	return nc
}

func (nc *NetClient) Request() Request {
	r := nc.Client.R()
	return &NetRequest{
		Request: r,
		httpMethodsM: map[string]httpMethodF{
			"GET":  r.Get,
			"POST": r.Post,
		},
		cache: nc.cache,
	}
}

func (nc *NetClient) GetClient() *http.Client {
	return nc.Client.GetClient()
}

func (nrq *NetRequest) Get(url string) (Response, error) {
	return nrq.Do("GET", url)
}

func (nrq *NetRequest) Post(url string) (Response, error) {
	return nrq.Do("POST", url)
}

func (nrq *NetRequest) Do(method, urlS string) (Response, error) {
	url, err := url.Parse(urlS)
	if err != nil {
		return nil, fmt.Errorf("parsing url %q: %w", urlS, err)
	}

	resp, err := nrq.cache.LoadResponse(method, url)
	if err == nil {
		return resp, nil
	}

	httpCall, ok := nrq.httpMethodsM[method]
	if !ok {
		panic(fmt.Sprintf("unknown http method %q used", method))
	}
	resp, err = nrq.doRequest(urlS, method, httpCall)
	// logging
	//err = nrq.cache.LogRequest(nrq, method, urlS, err, resp)
	err = nrq.cache.LogRequest(nrq, err, resp)
	return resp, err
}

func (nrq *NetRequest) doRequest(url string, method string, httpRequestF httpMethodF) (Response, error) {
	// check if an error already occurred, e.g. in SetBodyJson()
	if nrq.err != nil {
		return nil, fmt.Errorf("skip [%s] %q because of earlier error: %w", method, url, nrq.err)
	}

	// actually DO the network request
	resp, err := httpRequestF(url)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode()/100 != 2 {
		return nil, fmt.Errorf("%s request to %q unsuccessful: %d %s",
			method, url, resp.StatusCode(), resp.Status())
	}

	nresp := &NetResponse{
		Response: resp,
	}
	return nresp, nil
}

func (nrq *NetRequest) SetProtocol(prot string) Request {
	nrq.cache.Protocol = prot
	return nrq
}

func (nrq *NetRequest) SetHeader(key string, value string) Request {
	if key != "" && value != "" {
		nrq.Request.SetHeader(key, value)
	}
	return nrq
}

func (nrq *NetRequest) AddHeaders(headers http.Header) Request {
	for tag, values := range headers {
		for _, value := range values {
			nrq.Request.Header.Add(tag, value)
		}
	}
	return nrq
}

func (nrq *NetRequest) SetHeaders(headersM map[string]string) Request {
	for tag, value := range headersM {
		if tag == "Cookie" {
			cookieHdrValueL := strings.SplitN(value, "=", 2)
			if len(cookieHdrValueL) == 2 {
				nrq.SetCookie(cookieHdrValueL[0], cookieHdrValueL[1])
				continue
			} else {
				panic(fmt.Sprintf("invalid cookie header value %q", value))
			}
		}
		nrq.Request.Header.Set(tag, value)
	}
	return nrq
}

func (nrq *NetRequest) SetCookie(name, value string) Request {
	nrq.Request.SetCookie(&http.Cookie{
		Name:  name,
		Value: value,
		//Path:     "/",
		//Domain:   "sample.com",
		//MaxAge:   36000,
		HttpOnly: true,
		Secure:   false,
	})
	return nrq
}

func (nrq *NetRequest) SetBody(body interface{}) Request {
	nrq.Request.SetBody(body)
	return nrq
}

func (nrq *NetRequest) SetBodyJson(body interface{}) Request {
	jsonB, err := json.Marshal(body)
	if nrq.err == nil {
		nrq.err = err
	}
	if err == nil {
		nrq.SetHeader("Content-Type", "application/json")
	}
	return nrq.SetBody(jsonB)
}

func (nrs *NetResponse) StatusCode() int {
	return nrs.Response.StatusCode()
}

func (nrs *NetResponse) Status() string {
	return nrs.Response.Status()
}

func (nrs *NetResponse) Body() []byte {
	//if len(nrs.Response.Body()) > 0 {
	if nrs.Response.Body() != nil {
		return nrs.Response.Body()
	}
	var buf bytes.Buffer
	_, err := io.Copy(&buf, nrs.RawBody())
	if err != nil {
		panic(err)
	}
	return buf.Bytes()
}

func (nrs *NetResponse) BodyString() string {
	return string(nrs.Body())
}

func (nrs *NetResponse) Header() http.Header {
	return nrs.Response.Header()
}

func (nrs *NetResponse) DeJSON(tgtData interface{}) error {
	if tgtData == nil {
		return nil
	}
	body := nrs.Body()
	err := json.Unmarshal(body, tgtData)
	//log.Printf("DeJSON(%q): %#v", nrs.BodyString(), tgtData)
	if err != nil {
		dumpFName, err2 := dumpJsonToTempFile(body)
		if err2 == nil {
			return fmt.Errorf("DeJSONize response of %q in %q: %w", nrs.Request.URL, dumpFName, err)
		}
		return fmt.Errorf("DeJSONize response of %q in %q: %w // %v", nrs.Request.URL, dumpFName, err, err2)
	}
	return nil
}

// TODO: dump file into HPE-Asset specific log file
func dumpJsonToTempFile(body []byte) (dumpFName string, dumpJsonToTempFileErr error) {
	jsonF, err := os.CreateTemp("", "resty-adapt-dejson-error-*.json")
	if err != nil {
		return "", fmt.Errorf("creating temp file: %w", err)
	}
	dumpFName = jsonF.Name()
	defer func() {
		err2 := jsonF.Close()
		if err2 != nil {
			dumpJsonToTempFileErr = fmt.Errorf("closing temp file %q: %w", dumpFName, err2)
		}
	}()

	bodyBuf := bytes.NewBuffer(body)
	n, err := io.Copy(jsonF, bodyBuf)
	if err != nil {
		return "", fmt.Errorf("writing temp file %q: %w", dumpFName, err)
	}
	if n != int64(len(body)) {
		return "", fmt.Errorf("when writing JSON body of %d bytes to temp JSON file %q: wrote %d bytes",
			len(body), dumpFName, n)
	}
	return dumpFName, nil
}
