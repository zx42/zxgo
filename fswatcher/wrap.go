package fswatcher

import (
	"github.com/fsnotify/fsnotify"
)

type (
	Event struct {
		fsnotify.Event
	}
	Op struct {
		fsnotify.Op
	}
)

var (
	Nop = Op{}
)

func (ev Event) Has(op fsnotify.Op) bool {
	return ev.Op&op != 0
}

func (op Op) String() string {
	s := op.Op.String()
	if s == "" {
		return "None"
	}
	return s
}
