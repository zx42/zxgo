package fswatcher

import (
	"fmt"
	"log"
	"time"

	"github.com/fsnotify/fsnotify"

	"gitlab.com/zx42/zxgo/path"
)

type (
	Watcher struct {
		*fsnotify.Watcher

		WaitBeforeFileClose time.Duration
		WaitAfterHandler    time.Duration
		Verbose             bool

		FileCreated Handler
		//FileWritten Handler
		//FileWritten func(fname string)
		FileClosed  Handler
		FileRemoved Handler

		//FileCreated  func(fname string)
		//FileWritten *FileEventHandler
		////FileWritten func(fname string)
		//FileClosed  func(fname string)
		//FileRemoved func(fname string)

		filesM    map[string]*openFileT
		fileDoneC chan *openFileT
	}
)

//func NewWatcher(waitBeforeFileCloseSec int,
//	//queueSize int,
//	verbose bool) *Watcher {
//	//w := New(queueSize)
//	w := New()
//	w.WaitBeforeFileClose = time.Duration(waitBeforeFileCloseSec) * time.Second
//	w.Verbose = verbose
//	return w
//}

// New starts and returns a new watcher
func New() *Watcher {
	//func New(queueSize int) *Watcher {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	w := &Watcher{
		Watcher:          watcher,
		WaitAfterHandler: 2 * time.Second,
		filesM:           make(map[string]*openFileT),
		fileDoneC:        make(chan *openFileT),
		//fileDoneC:        make(chan *openFileT, queueSize),
	}
	go w.loop()
	return w
}

// Add one or more paths to watch
func (w *Watcher) Add(filesOrFolders ...path.AbsPath) error {
	for _, dir := range filesOrFolders {
		err := w.Watcher.Add(dir.String())
		if err != nil {
			return fmt.Errorf("adding folder %q: %w", dir, err)
		}
	}
	return nil
}

func (w *Watcher) Close() error {
	if w.Verbose {
		log.Println("Close Watcher")
	}
	return w.Watcher.Close()
}

func (w *Watcher) loop() {
	for {
		select {
		case event0, ok := <-w.Events:
			if !ok {
				return
			}
			event := Event{Event: event0}
			if event.Has(fsnotify.Create) {
				of, ok := w.filesM[event.Name]
				if !ok {
					of = newOpenFile(&event, w.WaitBeforeFileClose, w.WaitAfterHandler, w.Verbose,
						w.FileCreated, w.FileClosed, w.FileRemoved, w.fileDoneC)
					//} else {
					//	panic("CREATE open file " + event.Name)
					//of.resetTimerC <- false
				}
				w.filesM[event.Name] = of
				continue

				//handleCallback(event.Name, w.FileCreated)
				//continue
			}
			if event.Has(fsnotify.Write) {
				of, ok := w.filesM[event.Name]
				if !ok {
					//of = newOpenFile(event.Name, w.WaitBeforeFileClose, w.Verbose, w.FileWritten, w.fileDoneC)
					of = newOpenFile(&event, w.WaitBeforeFileClose, w.WaitAfterHandler, w.Verbose,
						w.FileCreated, w.FileClosed, w.FileRemoved, w.fileDoneC)
				} else {
					//of.resetTimerC <- false
					of.eventC <- &event
				}
				//of.lastModified = time.Now()
				w.filesM[event.Name] = of
				continue
			}
			if event.Has(fsnotify.Remove) || event.Has(fsnotify.Rename) {
				of, ok := w.filesM[event.Name]
				if !ok {
					continue
				}
				//of.resetTimerC <- true
				of.eventC <- &event
				continue
			}
			//if w.Verbose {
			log.Println("event:", event)
			//}

		case cf := <-w.fileDoneC:
			//event := Event{Event: fsnotify.Event{
			//	Name: cf.name,
			//	Op:   fsnotify.Remove,
			//}}
			//cf.eventC <- &event
			//log.Println("closed file: ", cf.name)
			delete(w.filesM, cf.name)
			//if cf.removed {
			//	cf.startEventHandler(fsnotify.Remove, w.FileRemoved)
			////} else {
			////	cf.startEventHandler(w.FileClosed)
			//}

		case err, ok := <-w.Errors:
			if !ok {
				return
			}
			log.Println("error:", err)
		}
	}
}
