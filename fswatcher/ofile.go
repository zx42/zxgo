package fswatcher

import (
	"fmt"
	"log/slog"
	"time"

	"github.com/fsnotify/fsnotify"
)

type (
	openFileT struct {
		name                string
		waitBeforeFileClose time.Duration
		waitAfterHandler    time.Duration
		verbose             bool

		// normally write events for a file while it is handling a write will be ignored.
		// with this flag set they will be queued and processed sequentially
		//queueEvents bool

		create Handler
		write  Handler
		remove Handler

		//lastModified     time.Time
		removed          bool
		eventC           chan *Event
		handlerFinishedC chan Op
		activeHandler    Op
	}
	Handler func(fname string)
)

func newOpenFile(
	ev *Event,
	waitBeforeFileClose time.Duration,
	waitAfterHandler time.Duration,
	verbose bool,

	//queueEvents bool,

	create Handler,
	write Handler,
	remove Handler,
	fileDoneC chan<- *openFileT,
) *openFileT {
	of := &openFileT{
		name:                ev.Name,
		waitBeforeFileClose: waitBeforeFileClose,
		waitAfterHandler:    waitAfterHandler,
		//queueEvents:         queueEvents,
		create: create,
		write:  write,
		//rename:              rename,
		remove:           remove,
		verbose:          verbose,
		eventC:           make(chan *Event),
		handlerFinishedC: make(chan Op),
	}
	go of.loop(fileDoneC)
	of.eventC <- ev
	return of
}

func (of *openFileT) loop(fileDoneC chan<- *openFileT) {
	fileCloseTimeOutC := time.After(of.waitBeforeFileClose)
	for {
		select {
		case ev, ok := <-of.eventC:
			if !ok {
				panic("Ooops: reset channel closed: " + of.name)
			}
			switch ev.Op {
			case fsnotify.Create:
				print("c ")
				if of.activeHandler.Op != 0 {
					msg := fmt.Sprintf("[%q]: unexpected create event while handling event %d", of.name, of.activeHandler)
					panic(msg)
				}
				of.activeHandler = of.startEventHandler(fsnotify.Create, of.create)
				if of.verbose {
					//log.Printf("create file %q", of.name)
					slog.Info("create", "file", of.name)
				}
				continue

			case fsnotify.Write:
				print("w")
				switch of.activeHandler.Op {
				case fsnotify.Create:
					// ignore writes while a create is handled
					print("c ")
					//continue
				case fsnotify.Write:
					// ignore writes while a write is handled
					print("w ")
					//continue
				case 0:
					// ignore writes while we are waiting for the writes to quiet down
					print("_ ")
					//continue
				default:
					msg := fmt.Sprintf("[%q]: unexpected write event while handling event %s",
						of.name, of.activeHandler)
					panic(msg)
				}
				continue

			case fsnotify.Remove | fsnotify.Rename:
				print("r ")
				of.removed = true
				of.activeHandler = of.startEventHandler(fsnotify.Remove, of.remove)
				if of.verbose {
					//action := "rename"
					//if ev.Op == fsnotify.Remove {
					//	action = "remove"
					//}
					//log.Printf("%s file %q", action, of.name)
					slog.Info("rename | remove", "file", of.name, "action", ev.Op.String())
				}
				continue

			default:
				print(". ")
				fmt.Printf(".%s ", ev.Op.String())
				if of.verbose {
					slog.Info("other event", "file", of.name, "event", ev.Op.String())
				}
				continue
			}

		//case <-time.After(of.waitBeforeFileClose):
		case <-fileCloseTimeOutC:
			fileCloseTimeOutC = nil
			// handle write
			of.activeHandler = of.startEventHandler(fsnotify.Write, of.write)
			if of.verbose {
				//log.Printf("write file %q", of.name)
				slog.Info("file closed", "file", of.name)
			}
			continue

		case op := <-of.handlerFinishedC:
			if of.activeHandler != op {
				msg := fmt.Sprintf("[%q]: expected active handler to be %s but was %s",
					of.name, op, of.activeHandler.String())
				panic(msg)
			}
			of.activeHandler = Nop
			if of.verbose {
				slog.Info("signalling file done", "file", of.name)
			}
			fileDoneC <- of
			if of.verbose {
				slog.Info("file done signalled", "file", of.name)
			}
		}
	}
}

func (of *openFileT) startEventHandler(op fsnotify.Op, handlerCallback func(fname string)) Op {
	if handlerCallback == nil {
		return Nop
	}

	wop := Op{op}
	go func() {
		if of.verbose {
			slog.Info("calling file close handler", "file", of.name)
		}
		handlerCallback(of.name)
		if of.verbose {
			slog.Info("file close handler done", "file", of.name)
		}
		time.Sleep(of.waitAfterHandler)
		if of.verbose {
			slog.Info("file close handler signalling done", "file", of.name)
		}
		of.handlerFinishedC <- wop
		if of.verbose {
			slog.Info("file close handler done signalling", "file", of.name)
		}
	}()
	return wop
}
