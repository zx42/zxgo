package fswatcher_test

import (
	"os"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/zx42/zxgo/fswatcher"
	"gitlab.com/zx42/zxgo/path"
	"gitlab.com/zx42/zxgo/path/files"
)

func TestWatcher(t *testing.T) {
	Convey("Given a watcher", t, func() {
		err := os.MkdirAll("tmp", 0777)
		So(err, ShouldBeNil)
		defer os.RemoveAll("tmp")
		//w := watch.NewWatcher(10, false)
		w := fswatcher.New()
		defer w.Close()

		fclosed := 0
		w.FileClosed = func(fname string) {
			//log.Printf("file %q closed", fname)
			fclosed++
		}
		fremoved := 0
		w.FileRemoved = func(fname string) {
			//log.Printf("file %q removed", fname)
			fremoved++
		}
		err = w.Add(path.New("tmp").Abs())
		So(err, ShouldBeNil)
		w.WaitBeforeFileClose = 200 * time.Millisecond

		Convey("create a big file", func() {
			err = files.CreateTestDataFile("tmp/big.file", 256*files.Kibi, 100*time.Millisecond)
			So(err, ShouldBeNil)

			Convey("and waitForFileWriteToEnd", func() {
				time.Sleep(250 * time.Millisecond)
				Convey("it should be processed", func() {
					So(fclosed, ShouldEqual, 1)
					So(fremoved, ShouldEqual, 0)
				})
			})

			Convey("and remove it", func() {
				err = os.Remove("tmp/big.file")
				So(err, ShouldBeNil)

				// FIXME: fix this test/code
				SkipConvey("it should not be processed", func() {
					So(fclosed, ShouldEqual, 0)
					So(fremoved, ShouldEqual, 1)
				})
			})
		})
	})
}
