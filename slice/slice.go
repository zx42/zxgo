package slice

func Contains[T comparable](slice []T, x T) bool {
	for _, v := range slice {
		if v == x {
			return true
		}
	}

	return false
}

func ContainsF[T comparable](slice ...T) func(x T) bool {
	m := map[T]bool{}
	for _, v := range slice {
		m[v] = true
	}
	return func(x T) bool {
		return m[x]
	}
}

func ContainsFunc[T comparable](slice []T, x T, isSimilar func(x, y T) bool) bool {
	for _, v := range slice {
		if isSimilar(v, x) {
			return true
		}
	}
	return false
}

func ToMap[T any](list []T, getKey func(v T) string, merge func(T, T) T) map[string]T {
	m := make(map[string]T)
	for _, item := range list {
		key := getKey(item)
		old, ok := m[key]
		if ok {
			item = merge(item, old)
		}
		m[key] = item
	}
	return m
}

func Map[T, R any](slice []T, trafo func(T) (R, bool)) []R {
	rL := make([]R, 0, len(slice))
	for _, value := range slice {
		r, add := trafo(value)
		if add {
			rL = append(rL, r)
		}
	}
	return rL
}

func Merge[T any](slice1, slice2 []T, getKey func(T) string, merge func(T, T) T) []T {
	m1 := ToMap(slice1, getKey, merge)
	m2 := ToMap(slice2, getKey, merge)
	outL := make([]T, 0, len(m1)+len(m2))
	for name, item := range m1 {
		item2, ok := m2[name]
		if ok {
			item = merge(item, item2)
			delete(m2, name)
		}
		outL = append(outL, item)
	}
	for _, item2 := range m2 {
		outL = append(outL, item2)
	}
	return outL
}

func Reverse[T any](slice []T) {
	lenSlice := len(slice)
	for i := range slice[:lenSlice/2] {
		j := lenSlice - i - 1
		slice[i], slice[j] = slice[j], slice[i]
	}
}

func SplitByType[T any](args []any) ([]T, []any) {
	headL := make([]T, 0, len(args))
	for i := 0; i < len(args); i++ {
		t, ok := args[i].(T)
		if ok {
			headL = append(headL, t)
			continue
		}
		args = args[i:]
		break
	}
	return headL, args
}

func SplitByFunc(args []any, cond func(any) bool) ([]any, []any) {
	headL := make([]any, 0, len(args))
	for i, arg := range args {
		if cond(arg) {
			headL = append(headL, arg)
			continue
		}
		args = args[i:]
		break
	}
	return headL, args
}

func FilterByType[T any](args []any) ([]T, []any) {
	tL := make([]T, 0, len(args))
	aL := make([]any, 0, len(args))
	for _, a := range args {
		t, ok := a.(T)
		if ok {
			tL = append(tL, t)
		} else {
			aL = append(aL, a)
		}
	}
	return tL, aL
}

func FilterByTypeUntil[T any, Stop any](args []any) ([]T, []any) {
	tL := make([]T, 0, len(args))
	aL := make([]any, 0, len(args))
	for i, a := range args {
		_, ok := a.(Stop)
		if ok {
			aL = append(aL, args[i:])
			break
		}

		t, ok := a.(T)
		if ok {
			tL = append(tL, t)
		} else {
			aL = append(aL, a)
		}
	}
	return tL, aL
}

func KeysOfMap[Key comparable, Val any](m map[Key]Val) []Key {
	keysL := make([]Key, 0, len(m))
	for key := range m {
		keysL = append(keysL, key)
	}
	return keysL
}
