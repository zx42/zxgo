//go:build ignore

package step

import (
	"fmt"

	"gitlab.com/zx42/zxgo/trafo/paths"
	"gitlab.com/zx42/zxgo/trafo/plugin"
)

type (
	Step interface {
		fmt.Stringer
		GetNum() (int, bool)
	}
	Container interface {
		GetData() any
	}
	StringStep string
	PluginStep struct {
		*plugin.PlugT
		//PluginId string
		//Plug     plugin.PlugT
	}
)

func (ss StringStep) String() string {
	return string(ss)
}

func (ss StringStep) GetNum() (int, bool) {
	return paths.GetNum(string(ss))
}

func (ps PluginStep) String() string {
	return fmt.Sprintf("%T: %q: %+v", ps, ps.Id, ps.PlugT)
}

func (ps PluginStep) GetNum() (int, bool) {
	return -2, false
}

func (ps PluginStep) GetData() any {
	return ps.InData
}
