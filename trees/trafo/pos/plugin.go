//go:build ignore

package pos

import (
	"log/slog"
)

type (
	Plugin interface {
		//Apply(plug *PlugT) error
		Forward(srcPos Pos) (tgtPos Pos, err error)
	}
	//PlugT struct {
	//	Id      string
	//	InData  any         `json:"in-data,omitempty" yaml:"in-data,omitempty"`
	//	OutData any         `json:"out-data,omitempty" yaml:"out-data,omitempty"`
	//	OutPath paths.PathT `json:"out-path,omitempty" yaml:"out-path,omitempty"`
	//}
	LogPluginT struct {
	}
)

//func (lpi *LogPluginT) Apply(plug *PlugT) error {
//	slog.Info("plugin apply", "plug", plug)
//	return nil
//}

func (lpi *LogPluginT) Forward(srcPos Pos) (tgtPos Pos, err error) {
	slog.Info("plugin apply", "pos", srcPos)
	return Pos{}, nil
}
