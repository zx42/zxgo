//go:build ignore

package pos

import (
	"fmt"

	"gitlab.com/zx42/zxgo/trafo/paths"
	"gitlab.com/zx42/zxgo/trafo/plugin"
)

type (
	Step interface {
		fmt.Stringer
		GetNum() (int, bool)
	}
	Container interface {
		GetData() any
	}
	StringStep string
	PluginStep struct {
		*plugin.PlugT
		//PluginId string
		//Plug     plugin.PlugT
	}
)

func (ss StringStep) String() string {
	return string(ss)
}

func (ss StringStep) GetNum() (int, bool) {
	return paths.GetNum(string(ss))
}

func (ss StringStep) Forward(srcPos Pos) (tgtPos Pos, err error) {
	data, key := srcPos.getData()
	sp.Key = key
	keyS := sp.Key.CurStr()
	dataM, isMap := data.(map[string]any)
	var dat any
	if isMap {
		dat = dataM[keyS]
	} else {
		idx, isNum := paths.GetNum(keyS)
		if isNum {
			dataL := data.([]any)
			dat = dataL[idx]
		}
	}
	if dat == nil {
		if sp.MissingData != nil {
			d, k := sp.MissingData(sp)
			sp.Key = k
			dat = d
		}
	}
	dp := SrcPos{
		CurData:     dat,
		Key:         sp.Key.Down(),
		MissingData: sp.MissingData,
	}
	return dp.setString()
}

func (ps PluginStep) String() string {
	return fmt.Sprintf("%T: %q: %+v", ps, ps.Id, ps.PlugT)
}

func (ps PluginStep) GetNum() (int, bool) {
	return -2, false
}

func (ps PluginStep) GetData() any {
	return ps.InData
}
