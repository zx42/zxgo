//go:build ignore

package pos

import (
	"fmt"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/trafo/paths"
)

type Pos struct {
	CurData any
	Key     paths.PathT
	s       string
	//MissingData func(SrcPos) (any, paths.PathT)
}

func NewPos(data any, keyChain []any) Pos {
	p := Pos{
		CurData: data,
		Key:     paths.NewKey(keyChain...),
	}
	return p.setString()
}

func (p *Pos) setString() Pos {
	//data, key := p.getData()
	//p.Ipp = key
	p.s = fmt.Sprintf("%s", p.Key)
	//p.s = fmt.Sprintf("%+v -- %s", data, p.Ipp)
	return *p
}

func (p Pos) getData(plugins map[string]Plugin) any {
	if p.CurData != nil {
		return p.CurData
	}
	plug := p.Key.Plug()
	if plug != nil {
		pi := plugins[plug.Id]
		if pi != nil {
			err := pi.Apply(plug.PlugT)
			must.Ok(err)
			return plug.OutData
		}
	}
	panic(fmt.Sprintf("missing data at pos %q", p.setString()))
}

//func (p Pos) Dive() Pos {
//	data, key := p.getData()
//	sp.Ipp = key
//	keyS := sp.Ipp.CurStr()
//	dataM, isMap := data.(map[string]any)
//	var dat any
//	if isMap {
//		dat = dataM[keyS]
//	} else {
//		idx, isNum := paths.GetNum(keyS)
//		if isNum {
//			dataL := data.([]any)
//			dat = dataL[idx]
//		}
//	}
//	if dat == nil {
//		if sp.MissingData != nil {
//			d, k := sp.MissingData(sp)
//			sp.Ipp = k
//			dat = d
//		}
//	}
//	dp := SrcPos{
//		CurData:     dat,
//		Ipp:         sp.Ipp.Down(),
//		MissingData: sp.MissingData,
//	}
//	return dp.setString()
//}
