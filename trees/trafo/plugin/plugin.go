//go:build ignore

package plugin

import (
	"gitlab.com/zx42/zxgo/trafo/paths"

	"log/slog"
)

type (
	Plugin interface {
		//Apply(plug *PlugT) error
		Forward(data any) error
	}
	PlugT struct {
		Id      string
		InData  any         `json:"in-data,omitempty" yaml:"in-data,omitempty"`
		OutData any         `json:"out-data,omitempty" yaml:"out-data,omitempty"`
		OutPath paths.PathT `json:"out-path,omitempty" yaml:"out-path,omitempty"`
	}
	LogPluginT struct {
	}
)

func (lpi *LogPluginT) Apply(plug *PlugT) error {
	slog.Info("plugin apply", "plug", plug)
	return nil
}
