package paths

import (
	"strconv"
)

func toStringMap(data any) any {
	switch dta := data.(type) {
	case map[string]any:
		for key, val := range dta {
			dta[key] = toStringMap(val)
		}
		return dta
	case []any:
		m := map[string]any{}
		for i, item := range dta {
			m[strconv.Itoa(i)] = toStringMap(item)
		}
		return m
	default:
		return data
	}
}
