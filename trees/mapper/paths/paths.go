package paths

import (
	"fmt"
	"strings"

	"golang.org/x/exp/slices"

	"gitlab.com/zx42/zxgo/num"
)

type (
	IntraPagePathT struct {
		chain []string
		ix    int
		ipps  IntraPagePathStringT
	}
	IntraPagePathStringT struct {
		s   string
		len int
		ix  int
	}
)

var (
	IntraPagePathVerboseString = true
	Root                       = NewIntraPagePath()
)

// NewIntraPagePath returns a new IntraPagePathT
// It clones the `keyChain` parameter
func NewIntraPagePath(keyChain ...any) IntraPagePathT {
	newKey := IntraPagePathT{
		chain: make([]string, len(keyChain)),
		ix:    0,
	}
	for i, k := range keyChain {
		kS := fmt.Sprintf("%v", k)
		if kS == "0" {
			kS = "[]"
		}
		newKey.chain[i] = kS
	}
	return newKey.setString()
}

func (ipp IntraPagePathT) String() string {
	return ipp.ipps.s
}

func (ipps *IntraPagePathStringT) ToStr(k *IntraPagePathT) string {
	if ipps.len == len(k.chain) && ipps.ix == k.ix {
		return ipps.s
	}

	ipps.len = len(k.chain)
	ipps.ix = k.ix

	upS := strings.Join(k.chain[:k.ix], " | ")
	cur := "~"
	if !k.IsLeaf() {
		cur = k.chain[k.ix]
	}
	var downS string
	if k.ix+1 < len(k.chain) {
		downS = strings.Join(k.chain[k.ix+1:], " | ")
	}
	s := strings.TrimSpace(
		fmt.Sprintf("%s ( %s ) %s", upS, cur, downS),
	)
	ipps.s = s
	return s
}

func (ipp IntraPagePathT) setString() IntraPagePathT {
	if IntraPagePathVerboseString {
		ipp.ipps.ToStr(&ipp)
	}
	return ipp
}

func (ipp IntraPagePathT) AppendStr(keyS string) IntraPagePathT {
	ipp.chain = append(ipp.chain, keyS)
	return ipp.setString()
}

func (ipp IntraPagePathT) AppendInt(i int) IntraPagePathT {
	return ipp.AppendStr(fmt.Sprintf("%d", i))
}

func (ipp IntraPagePathT) IsLeaf() bool {
	return len(ipp.chain) == ipp.ix
}

func (ipp IntraPagePathT) Height() int {
	return len(ipp.chain)
}

func (ipp IntraPagePathT) First() string {
	if len(ipp.chain) == 0 {
		return ""
	}
	return ipp.chain[0]
}

func (ipp IntraPagePathT) Last() string {
	if len(ipp.chain) == 0 {
		return ""
	}
	return ipp.chain[len(ipp.chain)-1]
}

func (ipp IntraPagePathT) CurStr() string {
	if ipp.ix>=len(ipp.chain){
		return ""
	}
	return ipp.chain[ipp.ix]
}

func (ipp IntraPagePathT) HasElem(elemS string) bool {
	return slices.Contains(ipp.chain, elemS)
}

func (ipp IntraPagePathT) StartList() IntraPagePathT {
	if !ipp.IsList() {
		panic("no list")
	}
	return ipp.clone(0)
}

func (ipp IntraPagePathT) Next() IntraPagePathT {
	idx, ok := num.GetNum(ipp.chain[ipp.ix])
	if ok {
		return ipp.clone(idx + 1)
	}
	panic(fmt.Sprintf("key %s is no numbered idx: %q", ipp, ipp.chain[ipp.ix]))
}

func (ipp IntraPagePathT) clone(idxNew int) IntraPagePathT {
	knext := IntraPagePathT{
		chain: make([]string, len(ipp.chain)),
		ix:    ipp.ix,
	}
	copy(knext.chain, ipp.chain)
	knext.chain[ipp.ix] = fmt.Sprintf("%d", idxNew)
	return knext.setString()
}

func (ipp IntraPagePathT) IsList() bool {
	idxS := ipp.CurStr()
	return idxS == "[]" // || IsNum(idxS)
}

func (ipp IntraPagePathT) IsNum() bool {
	idxS := ipp.CurStr()
	return num.IsNum(idxS)
}

func (ipp IntraPagePathT) Down() IntraPagePathT {
	if ipp.IsLeaf() {
		panic("out of paths")
	}

	return IntraPagePathT{
		chain: ipp.chain,
		ix:    ipp.ix + 1,
	}.setString()
}

func (ipp IntraPagePathT) Up() IntraPagePathT {
	if ipp.ix <= 0 {
		panic("out of paths")
	}

	return IntraPagePathT{
		chain: ipp.chain,
		ix:    ipp.ix - 1,
	}.setString()
}
