package mapper

import (
	"strconv"

	"gitlab.com/zx42/zxgo/trees/mapper/paths"
	"gitlab.com/zx42/zxgo/trees/mapper/pos"
)

type (
	MapperT struct {
		parentData map[string]any
	}
)

func MapSrcTgt(
	mpr *MapperT,
	inData any, srcPath []any,
	missingDataFn func(srcpos pos.SrcPos) (any, paths.IntraPagePathT),
	outData any, tgtPath []any,
) map[string]any {
	srcPos := pos.NewSrcPos(inData, srcPath)
	srcPos.MissingData = missingDataFn
	tgtPos := pos.NewTgtPos(outData, tgtPath)
	mpr.Map(srcPos, tgtPos)
	return tgtPos.ParentM["root"].(map[string]any)
}

func NewMapper() *MapperT {
	return &MapperT{}
}

func (mpr *MapperT) Map(
	srcPos pos.SrcPos,
	tgtPos pos.TgtPos,
) {
	walkSrc(srcPos, tgtPos)
	mpr.parentData = tgtPos.ParentM
}

func (mpr MapperT) FinalTgtData() any {
	mpr.parentData["root"] = mapsToSlices("root", mpr.parentData["root"], mpr.parentData)
	return mpr.parentData["root"]
}

func walkSrc(srcPos pos.SrcPos, tgtPos pos.TgtPos) {
	if srcPos.Ipp.IsLeaf() {
		walkTgt(srcPos, tgtPos)
		return
	}

	if srcPos.Ipp.IsNum() {
		walkTgt(srcPos.Dive(), tgtPos)
		return
	}

	if srcPos.Ipp.IsList() {
		walkTgt(srcPos, tgtPos)
		return
	}

	walkSrc(srcPos.Dive(), tgtPos)
}

func walkTgt(srcPos pos.SrcPos, tgtPos pos.TgtPos) {
	if tgtPos.Ipp.IsLeaf() {
		tgtPos.Set(srcPos)
		return
	}

	if tgtPos.Ipp.IsNum() {
		walkSrc(srcPos, tgtPos.Dive())
		return
	}

	if tgtPos.Ipp.IsList() {
		iterate(srcPos, tgtPos)
		return
	}

	walkTgt(srcPos, tgtPos.Dive())
}

func iterate(srcPos pos.SrcPos, tgtPos pos.TgtPos) {
	if !srcPos.Ipp.IsList() || !tgtPos.Ipp.IsList() {
		panic("oops")
	}
	srcIpp := srcPos.Ipp.StartList()
	tgtIpp := tgtPos.Ipp.StartList()
	itPos := srcPos.StartList()
	srcLen := srcPos.SliceLen()
	for idx := 0; idx < srcLen; idx++ {
		itPos.Ipp = srcIpp
		tgtPos.Ipp = tgtIpp
		walkSrc(itPos, tgtPos)
		srcIpp = srcIpp.Next()
		tgtIpp = tgtIpp.Next()
	}
	tgtPos.CountSlice()
}

func mapsToSlices(key string, value any, parent map[string]any) any {
	switch valueM := value.(type) {
	case map[string]any:
		if parent != nil {
			count, ok := parent[key+"Count"].(int)
			if ok {
				//if len(valueM) != count {
				//	panic("oops")
				//}

				valueL := make([]any, count)
				for i := range valueL {
					iS := strconv.Itoa(i)
					v := valueM[iS]
					valueL[i] = mapsToSlices(iS, v, valueM)
				}
				if parent != nil {
					parent[key] = valueL
				}
				return valueL
			}
		}

		for k, v := range valueM {
			valueM[k] = mapsToSlices(k, v, valueM)
		}
		return valueM
	}
	return value
}
