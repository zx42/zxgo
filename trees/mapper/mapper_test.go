package mapper_test

import (
	"reflect"
	"testing"

	"gitlab.com/zx42/zxgo/trees/mapper"
	"gitlab.com/zx42/zxgo/trees/mapper/pos"
)

type (
	M = map[string]any
	L = []any
)

func Test_mapData(t *testing.T) {
	type args struct {
		srcData  any
		srcKeysL []any
		tgtKeysL []any
		//varM     meta.varMapT
		call L
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		// src tree: *--a--b | keys: [a]
		// tgt tree: *--result--c--b | keys: [c]
		{name: "map1", args: args{
			srcData: M{"a": "b"}, srcKeysL: L{"a"},
			tgtKeysL: L{"c"},
		}, want: M{"c": "b"}},

		// src tree: *--a--b | keys: [a]
		// tgt tree: *--result--c--b | keys: [c]
		{name: "map1-error1", args: args{
			srcData: M{"a": "b"}, srcKeysL: L{"b"},
			tgtKeysL: L{"c"},
		}, want: M{"c": nil}},

		// src tree: *--a--b--c--d--e | keys: [a b c d]
		//              +--b2 +--d2
		// tgt tree: *--c--e | keys: [c]
		{name: "map2", args: args{
			srcData: M{"a": M{"b": M{"c": M{"d": "e"}}}}, srcKeysL: L{"a", "b", "c", "d"},
			tgtKeysL: L{"x"},
		}, want: M{"x": "e"}},

		{name: "map2-error1", args: args{
			srcData: M{"a": M{"b": M{"c": M{"d": "e"}}}}, srcKeysL: L{"a", "b1", "c", "d"},
			tgtKeysL: L{"x"},
		}, want: M{"x": nil}},

		{name: "map3", args: args{
			srcData: M{"a": M{"b": M{"c": M{"d": "e"}}}}, srcKeysL: L{"a", "b", "c", "d"},
			//tgtKeysL: L{"x"},
		}, want: "e"},

		// src tree: *--a--b--c--d--e | keys: [a b c d]
		// tgt tree: *--z--y--e | keys: [z y]
		{name: "map4", args: args{
			srcData: M{"a": M{"b": M{"c": M{"d": "e"}}}}, srcKeysL: L{"a", "b", "c", "d"},
			tgtKeysL: L{"z", "y"},
		}, want: M{"z": M{"y": "e"}}},

		{name: "map4-error1", args: args{
			srcData: M{"a": M{"b": M{"c": M{"d": "e"}}}}, srcKeysL: L{"x", "b", "c", "d"},
			tgtKeysL: L{"z", "y"},
		}, want: M{"z": M{"y": nil}}},

		// src tree: *--a--b | keys: [a]
		// tgt tree: *--z--y--b | keys: [z y]
		{name: "map5", args: args{
			srcData: M{"a": "b"}, srcKeysL: L{"a"},
			tgtKeysL: L{"z", "y"},
		}, want: M{"z": M{"y": "b"}}},

		// src tree: *--a--b | keys: [a]
		// tgt tree: *--z--y--b | keys: [z y]
		{name: "map6", args: args{
			srcData: M{"a": "b"}, srcKeysL: L{"a"},
			tgtKeysL: L{"z", "y"},
		}, want: M{"z": M{"y": "b"}}},

		// src tree: *--[b--c b--d] | keys: [0 b]
		// tgt tree: *--[c d] | keys: [0]
		{name: "list1", args: args{
			srcData: L{"a", "b"}, srcKeysL: L{0},
			tgtKeysL: L{0},
		}, want: L{"a", "b"}},

		// src tree: *--[b--c b--d] | keys: [0 b]
		// tgt tree: *--[c d] | keys: [0]
		{name: "list1[]", args: args{
			srcData: L{"a", "b"}, srcKeysL: L{L{}},
			tgtKeysL: L{L{}},
		}, want: L{"a", "b"}},

		// src tree: *--[b--c b--d] | keys: [0 b]
		// tgt tree: *--[c d] | keys: [0]
		{name: "list2", args: args{
			srcData: L{M{"a": "b"}, M{"a": "c"}}, srcKeysL: L{0},
			tgtKeysL: L{0},
		}, want: L{M{"a": "b"}, M{"a": "c"}}},

		{name: "list3", args: args{
			srcData: L{M{"a": "b"}, M{"a": "c"}}, srcKeysL: L{0, "a"},
			tgtKeysL: L{0},
		}, want: L{"b", "c"}},

		// src tree: *--a--[b--c b--d] | keys: [a 0 b]
		// tgt tree: *--z--[c d] | keys: [z 0]
		{name: "list4", args: args{
			srcData: M{"a": L{M{"b": "c"}, M{"b": "d"}}}, srcKeysL: L{"a", 0, "b"},
			tgtKeysL: L{"z", 0},
		}, want: M{"z": L{"c", "d"}, "zCount": 2}},

		// src tree: *--a--[b--c b--d] | keys: [a 0 b]
		// tgt tree: *--z--[y--c y--d] | keys: [z 0 y]
		{name: "list5", args: args{
			srcData: M{"a": L{M{"b": "c"}, M{"b": "d"}}}, srcKeysL: L{"a", 0, "b"},
			tgtKeysL: L{"z", 0, "y"},
		}, want: M{"z": L{M{"y": "c"}, M{"y": "d"}}, "zCount": 2}},

		// src tree: *--a--[b--c b--d] | keys: [a 0 b]
		// tgt tree: *--z--y--[x--w--c x--w--d] | keys: [z y 0 x w]
		{name: "list6", args: args{
			srcData: M{"a": L{M{"b": "c"}, M{"b": "d"}}}, srcKeysL: L{"a", 0, "b"},
			tgtKeysL: L{"z", "y", 0, "x", "w"},
		}, want: M{"z": M{"y": L{M{"x": M{"w": "c"}}, M{"x": M{"w": "d"}}}, "yCount": 2}}},

		// src tree: *--[a--[b c] a--[d e]] | keys: [0 a 0]
		// tgt tree: *--[[b c] [d e]] | keys: [0 0]
		{name: "nested-list1", args: args{
			srcData: L{L{"a", "b"}, L{"c"}, L{"d", "e"}}, srcKeysL: L{0, 0},
			//tgtData: nil, tgtKeysL: L{0, 0},
			//tgtParent: M{}, tgtParentKey: "result",
			tgtKeysL: L{0, 0},
		}, want: L{L{"a", "b"}, L{"c"}, L{"d", "e"}}},

		// src tree: *--[a--[b c] a--[d e]] | keys: [0 a 0]
		// tgt tree: *--[[b c] [d e]] | keys: [0 0]
		{name: "nested-list2", args: args{
			srcData: L{M{"a": L{"b", "c"}}, M{"a": L{"d", "e"}}}, srcKeysL: L{0, "a", 0},
			tgtKeysL: L{0, 0},
		}, want: L{L{"b", "c"}, L{"d", "e"}}},

		// src tree: *--a--[b--[c--d c--e] b--[c--f c--g]] | keys: [a 0 b 0 c]
		// tgt tree: *--z--y--[x--w--[v--u--d v--u--e] x--w--[v--u--f v--u--g]] | keys: [z y 0 x w 0 v u]
		{name: "nested-list3", args: args{
			srcData: M{"a": L{
				M{"b": L{M{"c": "d"}, M{"c": "e"}}},
				M{"b": L{M{"c": "f"}, M{"c": "g"}}},
			}},
			srcKeysL: L{"a", 0, "b", 0, "c"},
			tgtKeysL: L{"z", "y", 0, "x", "w", 0, "v", "u"},
		}, want: M{"z": M{"y": L{
			M{"x": M{"w": L{M{"v": M{"u": "d"}}, M{"v": M{"u": "e"}}}, "wCount": 2}},
			M{"x": M{"w": L{M{"v": M{"u": "f"}}, M{"v": M{"u": "g"}}}, "wCount": 2}},
		}, "yCount": 2}}},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mapper := mapper.NewMapper()
			src := pos.NewSrcPos(tt.args.srcData, tt.args.srcKeysL)
			//tgt := newWrPos(nil, tt.args.tgtKeysL)
			tgt := pos.NewTgtPos(nil, tt.args.tgtKeysL)
			mapper.Map(src, tgt)
			mapper.FinalTgtData()
			if !reflect.DeepEqual(mapper.FinalTgtData(), tt.want) {
				t.Errorf("MapData() = %+v, want %+v", tgt.ParentM, tt.want)
			}
			wantL, ok := tt.want.(L)
			if ok {
				if tgt.ParentM["rootCount"] != len(wantL) {
					t.Errorf("MapData() = %+v, want %+v", tgt.ParentM, tt.want)
				}
			}
		})
	}
}
