package pos

import (
	"fmt"

	"gitlab.com/zx42/zxgo/num"
	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	TgtPos struct {
		Ipp     paths.IntraPagePathT
		ParentM map[string]any
		s       string
	}
)

func NewTgtPos(data any, key []any) TgtPos {
	fullKey := append([]any{"root"}, key...)
	ttp := TgtPos{
		ParentM: map[string]any{"root": data},
		Ipp:     paths.NewIntraPagePath(fullKey...).Down(),
	}
	return ttp.setString()
}

func (tp TgtPos) CountSlice() TgtPos {
	upKey := tp.Ipp.Up()
	upKeyS := upKey.CurStr()

	size := 0
	data := tp.ParentM[upKeyS]
	dataM, ok := data.(map[string]any)
	if ok {
		for idxS := range dataM {
			_, isNum := num.GetNum(idxS)
			if isNum {
				size++
			}
		}
	}
	tp.ParentM[upKeyS+"Count"] = size
	return tp.setString()
}

func (tp TgtPos) String() string {
	return tp.s
}

func (tp *TgtPos) setString() TgtPos {
	tp.s = fmt.Sprintf("%+v -- %s", tp.ParentM, tp.Ipp)
	return *tp
}

func (tp TgtPos) Dive() TgtPos {
	upKeyS := tp.Ipp.Up().CurStr()
	data, ok := tp.ParentM[upKeyS]
	if !ok {
		panic("parent missing")
	}
	dataM, ok := data.(map[string]any)
	if !ok || len(dataM) == 0 {
		dataM = map[string]any{}
		data = dataM
	}
	keyS := tp.Ipp.CurStr()
	_, ok = dataM[keyS]
	if !ok {
		dataM[keyS] = nil
	}
	dtp := TgtPos{
		Ipp:     tp.Ipp.Down(),
		ParentM: dataM,
	}
	tp.ParentM[upKeyS] = dataM
	return dtp.setString()
}

func (tp *TgtPos) Set(srcPos SrcPos) {
	if !srcPos.Ipp.IsLeaf() || !tp.Ipp.IsLeaf() {
		panic("no leaf")
	}
	upKey := tp.Ipp.Up()
	upKeyS := upKey.CurStr()
	data, _ := srcPos.getData()
	tp.ParentM[upKeyS] = data
}
