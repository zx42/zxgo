package pos

import (
	"fmt"
	"strconv"

	"gitlab.com/zx42/zxgo/num"
	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type SrcPos struct {
	CurData     any
	Ipp         paths.IntraPagePathT
	s           string
	MissingData func(SrcPos) (any, paths.IntraPagePathT)
}

func NewSrcPos(data any, keyChain []any) SrcPos {
	tp := SrcPos{
		CurData: data,
		Ipp:     paths.NewIntraPagePath(keyChain...),
	}
	return tp.setString()
}

func (sp SrcPos) getData() (any, paths.IntraPagePathT) {
	if sp.CurData != nil {
		return sp.CurData, sp.Ipp
	}
	if sp.MissingData != nil {
		return sp.MissingData(sp)
	}
	//panic(fmt.Sprintf("missing src data at %q", sp.Ipp))
	return nil, sp.Ipp
}

func (sp SrcPos) SliceLen() int {
	data, ipp := sp.getData()
	sp.Ipp = ipp
	dataL, ok := data.([]any)
	if ok {
		return len(dataL)
	}

	dataM := data.(map[string]any)
	for i := 0; i < 1000000; i++ {
		iS := strconv.Itoa(i + 1)
		_, ok := dataM[iS]
		if !ok {
			return i
		}
	}
	panic(fmt.Sprintf("found list with > 1000000 entries"))
}

func (sp SrcPos) StartList() SrcPos {
	data, key := sp.getData()
	sp.Ipp = key
	dataL := data.([]any)
	m := map[string]any{}
	for i, val := range dataL {
		iS := strconv.Itoa(i)
		m[iS] = val
	}
	sp.CurData = m
	return sp.setString()
}

func (sp SrcPos) String() string {
	return sp.s
}

func (sp *SrcPos) setString() SrcPos {
	data, key := sp.getData()
	sp.Ipp = key
	sp.s = fmt.Sprintf("%+v -- %s", data, sp.Ipp)
	return *sp
}

func (sp SrcPos) Dive() SrcPos {
	data, key := sp.getData()
	sp.Ipp = key
	keyS := sp.Ipp.CurStr()
	dataM, isMap := data.(map[string]any)
	var dat any
	datExists := false
	if isMap {
		dat, datExists = dataM[keyS]
	} else {
		idx, isNum := num.GetNum(keyS)
		if isNum {
			dataL := data.([]any)
			dat = dataL[idx]
			datExists = true
		}
	}
	if !datExists && dat == nil {
		if sp.MissingData != nil {
			d, k := sp.MissingData(sp)
			sp.Ipp = k
			dat = d
		}
	}
	dp := SrcPos{
		CurData:     dat,
		Ipp:         sp.Ipp.Down(),
		MissingData: sp.MissingData,
	}
	spDown := dp.setString()
	return spDown
}
