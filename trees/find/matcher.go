package find

import (
	"regexp"
)

type (
	Matcher struct {
		rxs []*regexp.Regexp
	}
)

func NewMatcher(keyValRxs ...string) *Matcher {
	kvRxs := make([]*regexp.Regexp, len(keyValRxs))
	for i, kvRxS := range keyValRxs {
		kvRxs[i] = regexp.MustCompile("(?i)" + kvRxS)
	}
	return &Matcher{
		rxs: kvRxs,
	}
}

func (m Matcher) Matches(s string) (bool, int) {
	for i, rx := range m.rxs {
		if rx.MatchString(s) {
			return true, i
		}
	}
	return false, 0
}
