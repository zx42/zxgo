package find

import (
	"fmt"

	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	valueCacheT struct {
		byValueM map[string]map[string]*keyValueT
	}
)

func newValueCache() *valueCacheT {
	return &valueCacheT{
		byValueM: map[string]map[string]*keyValueT{},
	}
}

func (vc *valueCacheT) Add(ipp paths.IntraPagePathT, value any) bool {
	valS, ok := value.(string)
	if value == nil || (ok && valS == "") {
		return false
	}
	valueS := fmt.Sprintf("%v", value)
	if valueS == "" || valueS == "<nil>" {
		return false
	}

	byValueM, ok := vc.byValueM[valueS]
	if !ok {
		byValueM = map[string]*keyValueT{}
		vc.byValueM[valueS] = byValueM
	}
	keyS := ipp.String()
	byValueM[keyS] = &keyValueT{
		ipp:   ipp,
		value: value,
	}
	return true
}
