package find_test

import (
	"encoding/json"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/trees/find"
)

type (
	M         map[string]any
	pageTreeT struct {
		pages []find.Page
	}
	page struct {
		entryPoint string
		body       string
	}
)

const EntryPoint = "/"

func (pt pageTreeT) Pages() []find.Page {
	return pt.pages
}

func (pg page) EntryPoint() string {
	return pg.entryPoint
}

func (pg page) Body() string {
	return pg.body
}

func TestFind(t *testing.T) {
	type testCase struct {
		name       string
		pages      []M
		lookupExpr string
		wantPaths  []string
	}
	Convey("Given a redfish collector", t, func() {
		tests := []testCase{
			// TODO: Add test cases.
			{name: "find root.a",
				lookupExpr: "a",
				pages:      []M{{"/": EntryPoint, "a": "#a"}},
				wantPaths:  []string{"$base | 'a'"},
			},
			{name: "find nested root/p2.a2",
				lookupExpr: "a2",
				pages: []M{
					{"/": EntryPoint, "p2": EntryPoint + "/p2"},
					{"/": EntryPoint + "/p2", "a2": "#a2"},
				},
				wantPaths: []string{"$base | 'p2' | '://' | 'a2'"},
			},
			{name: "find nested root/p2/p3.a3",
				lookupExpr: "a3",
				pages: []M{
					{"/": EntryPoint, "p2": EntryPoint + "/p2"},
					{"/": EntryPoint + "/p2", "p3": EntryPoint + "/p3"},
					{"/": EntryPoint + "/p3", "a3": "#a3"},
				},
				wantPaths: []string{"$base | 'p2' | '://' | 'p3' | '://' | 'a3'"},
			},
			{name: "find double root/p2/p3.a3",
				lookupExpr: "a3",
				pages: []M{
					{"/": EntryPoint, "p2": EntryPoint + "/p2", "p3": EntryPoint + "/p3"},
					{"/": EntryPoint + "/p2", "p3": EntryPoint + "/p3"},
					{"/": EntryPoint + "/p3", "a3": "#a3"},
				},
				//wantPaths: []string{"$base | 'p2' | '://' | 'p3' | '://' | 'a3'"},
				wantPaths: []string{
					"$base | 'p3' | '://' | 'a3'",
					"$base | 'p2' | '://' | 'p3' | '://' | 'a3'",
				},
			},
			{name: "find cycle root/p2.a2",
				lookupExpr: "a2",
				pages: []M{
					{"/": EntryPoint, "p2": EntryPoint + "/p2", "e": EntryPoint},
					{"/": EntryPoint + "/p2", "a2": "#a2", "p3": EntryPoint + "/p3"},
					{"/": EntryPoint + "/p3", "p2": EntryPoint + "/p2"},
				},
				wantPaths: []string{"$base | 'p2' | '://' | 'a2'"},
			},
		}
		for _, tc := range tests {
			Convey(tc.name, func() {
				pageTree := &pageTreeT{}
				for _, p := range tc.pages {
					ep := &page{
						entryPoint: p["/"].(string),
						body:       string(must.Ok1(json.MarshalIndent(p, "", "  "))),
					}
					pageTree.pages = append(pageTree.pages, ep)
				}
				fdr := find.NewFinder(EntryPoint, pageTree, nil, "")
				fdr.FindInTree(tc.lookupExpr)
				devChains := fdr.CollectInterPagePaths(find.SortLen)
				So(len(devChains), ShouldEqual, len(tc.wantPaths))
				for i, wp := range tc.wantPaths {
					So(devChains[i].String(), ShouldEqual, wp)
				}
			})
		}
	})
}
