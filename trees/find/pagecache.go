package find

import (
	"encoding/json"
	"strings"

	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	addFoundFN func(matchIdx int, ipp paths.IntraPagePathT, val any) bool
	pageCacheT struct {
		intraPageTrees         map[string]map[string]any
		commonEntryPointPrefix string
	}
	Page interface {
		EntryPoint() string
		Body() string
	}
)

func newPageCache(commonEntryPointPrefix string) *pageCacheT {
	return &pageCacheT{
		intraPageTrees:         map[string]map[string]any{},
		commonEntryPointPrefix: commonEntryPointPrefix,
	}
}

func (pc *pageCacheT) findInPage(
	page Page,
	matcher *Matcher,
	addFoundFn addFoundFN,
	verbose bool,
) []paths.IntraPagePathT {
	tree := pc.getPageTree(page, verbose)
	urlPath := strings.TrimPrefix(page.EntryPoint(), pc.commonEntryPointPrefix)
	ipp := paths.Root.AppendStr(urlPath)
	findInData(matcher, ipp, tree, addFoundFn, verbose)
	return nil
}

func (pc *pageCacheT) getPageTree(
	page Page,
	verbose bool,
) map[string]any {
	tree, ok := pc.intraPageTrees[page.EntryPoint()]
	if !ok || len(tree) == 0 {
		err := json.Unmarshal([]byte(page.Body()), &tree)
		if err != nil {
			return nil
		}
		pc.intraPageTrees[page.EntryPoint()] = tree
	}
	return tree
}
