package find

import (
	"fmt"
	"log/slog"
	"strings"

	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	finderT struct {
		entryPoint string
		pageTree   PageTree
		verbose    bool
		pc         *pageCacheT
		ppt        *parentPageTreeT
		vc         *valueCacheT
	}
	PageTree interface {
		Pages() []Page
	}
	keyValueT struct {
		ipp   paths.IntraPagePathT
		value any
	}
)

func NewFinder(
	entryPoint string,
	pageTree PageTree,
	sortIntraPathsFn SortIntraPathsFN,
	commonEntryPointPrefix string,
) *finderT {
	fdr := &finderT{
		entryPoint: entryPoint,
		pageTree:   pageTree,
		pc:         newPageCache(commonEntryPointPrefix),
		ppt:        newParentPageTree(entryPoint, sortIntraPathsFn),
		vc:         newValueCache(),
	}
	return fdr
}

func (fdr *finderT) FindInTree(keyValRxs ...string) *Matcher {
	pageLinkRxS := `^` + fdr.entryPoint
	keyValRxs = append([]string{pageLinkRxS}, keyValRxs...)
	matcher := NewMatcher(keyValRxs...)
	for _, page := range fdr.pageTree.Pages() {
		fdr.pc.findInPage(page, matcher, fdr.addFound, fdr.verbose)
	}
	return matcher
}

func findInData(matcher *Matcher, parentIpp paths.IntraPagePathT, data any, addFoundFn addFoundFN, verbose bool) {
	switch dta := data.(type) {
	case string:
		match, idx := matcher.Matches(dta)
		if match {
			if addFoundFn(idx, parentIpp, dta) && verbose {
				slog.Info("found string", "value", dta)
			}
		}
	case map[string]any:
		for keyS, val := range dta {
			ipp := parentIpp.AppendStr(keyS)
			match, idx := matcher.Matches(keyS)
			if match {
				if addFoundFn(idx, ipp, val) && verbose {
					slog.Info("found ipp", "ipp", ipp, "value", val)
				}
				continue
			}

			valS, ok := val.(string)
			if ok {
				match, idx := matcher.Matches(valS)
				if match {
					if addFoundFn(idx, ipp, val) && verbose {
						slog.Info("found value", "ipp", ipp, "value", val)
					}
					continue
				}
			}
			findInData(matcher, ipp, val, addFoundFn, verbose)
		}
	case []any:
		for i, item := range dta {
			ipp := parentIpp.AppendInt(i)
			findInData(matcher, ipp, item, addFoundFn, verbose)
		}
	case float64, bool, nil:
	default:
		slog.Info("unusual data type", "type", fmt.Sprintf("%T", data))
		dataS := fmt.Sprintf("%v", data)
		match, idx := matcher.Matches(dataS)
		if match {
			if addFoundFn(idx, parentIpp, data) && verbose {
				slog.Info("found", "value", dta)
			}
		}
	}
}

func (fdr *finderT) addFound(idx int, ipp paths.IntraPagePathT, val any) bool {
	if idx == 0 {
		valS := val.(string)
		if !strings.HasPrefix(valS, fdr.entryPoint) {
			panic("oops")
		}
		return fdr.ppt.Add(ipp, valS)
	}

	return fdr.vc.Add(ipp, val)
}
