package find

import (
	"fmt"
	"sort"

	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	SorterT struct {
		sortBy SortBy
	}
	SortBy int
)

const (
	SortAlpha SortBy = iota
	SortLen
)

func (str SorterT) SortStringChains(chains []StringPathT) {
	sort.Slice(chains, func(i, j int) bool {
		ci := chains[i]
		cj := chains[j]
		ciS := ci.String()
		cjS := cj.String()
		switch str.sortBy {
		case SortAlpha:
			return ciS < cjS
		case SortLen:
			if len(ci.ss) == len(cj.ss) {
				if len(ciS) == len(cjS) {
					return ciS < cjS
				}
				return len(ciS) < len(cjS)
			}
			return len(ci.ss) < len(cj.ss)
		default:
			panic(fmt.Sprintf("unknown sort algorithm: %d", str.sortBy))
		}
	})
}

func sortIntraPagePaths(ippsL []paths.IntraPagePathT) {
	sort.Slice(ippsL, func(i, j int) bool {
		h1 := ippsL[i].Height()
		h2 := ippsL[j].Height()
		if h1 == h2 {
			return len(ippsL[i].First()) > len(ippsL[j].First())
		}
		return h1 < h2
	})
}
