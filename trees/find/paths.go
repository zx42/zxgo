package find

import (
	"fmt"
	"strings"

	"gitlab.com/zx42/zxgo/num"
	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	StringPathT struct {
		ss         []string
		Annotation string
	}
	pathListT struct {
		nestedList []interPagePathT
	}
	interPagePathT struct {
		chainL []paths.IntraPagePathT
	}
)

func (sp StringPathT) String() string {
	return strings.Join(sp.ss, " | ")
}

func (sp StringPathT) ListString() string {
	return strings.Join(sp.ss, ", ")
}

func (sp StringPathT) ListLen() int {
	return len(sp.ss)
}

func (ipp interPagePathT) Prepend(key paths.IntraPagePathT) (bool, interPagePathT) {
	if ipp.isKnown(key) {
		return false, ipp
	}
	nipp := interPagePathT{chainL: make([]paths.IntraPagePathT, 1, len(ipp.chainL)+1)}
	nipp.chainL[0] = key
	nipp.chainL = append(nipp.chainL, ipp.chainL...)
	return true, nipp
}

func (ipp interPagePathT) isKnown(key paths.IntraPagePathT) bool {
	keyFirstS := key.First()
	for _, pKey := range ipp.chainL {
		if pKey.First() == keyFirstS {
			return true
		}
	}
	return false
}

func (ipp interPagePathT) StringPath() StringPathT {
	srcKeyChain := []string{"$base"}
	for i, kp := range ipp.chainL {
		if kp.Height() == 0 {
			continue
		}
		if i > 0 {
			srcKeyChain = append(srcKeyChain, "'://'")
		}
		k := kp.Down()
		for !k.IsLeaf() {
			curS := k.CurStr()
			elemS := fmt.Sprintf("'%s'", curS)
			_, isNum := num.GetNum(curS)
			if elemS == "'[]'" {
				elemS = "[]"
			} else if isNum {
				elemS = curS
			}
			srcKeyChain = append(srcKeyChain, elemS)
			k = k.Down()
		}
	}
	stringPath := StringPathT{
		ss:         srcKeyChain,
		Annotation: "",
	}
	return stringPath
}

func (fdr *finderT) CollectInterPagePaths(sortBy SortBy) (chains []StringPathT) {
	chainsM := map[string]string{}
	for valueS, byValueM := range fdr.vc.byValueM {
		for keyS, kv := range byValueM {
			moreChains := fdr.ppt.JoinIntraPagePaths(kv.ipp)
			for _, chain := range moreChains {
				chainS := chain.String()
				_, known := chainsM[chainS]
				if known {
					panic(fmt.Sprintf("stringPath %q is already known for key %q and value %q", chainS, keyS, valueS))
				}
				chain.Annotation = fmt.Sprintf("%+v", kv.value)
				chains = append(chains, chain)
			}
		}
	}
	sorter := SorterT{sortBy: sortBy}
	sorter.SortStringChains(chains)
	return chains
}
