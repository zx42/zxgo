package find

import (
	"gitlab.com/zx42/zxgo/trees/mapper/paths"
)

type (
	parentPageTreeT struct {
		entryPoint       string
		parentPagesM     map[string][]paths.IntraPagePathT
		parentPageKeysM  map[string]bool
		sortIntraPathsFn SortIntraPathsFN
	}
	SortIntraPathsFN func(ippsL []paths.IntraPagePathT)
)

func newParentPageTree(entryPoint string, sortIntraPathsFn SortIntraPathsFN) *parentPageTreeT {
	if sortIntraPathsFn == nil {
		sortIntraPathsFn = sortIntraPagePaths
	}
	return &parentPageTreeT{
		entryPoint:       entryPoint,
		parentPagesM:     map[string][]paths.IntraPagePathT{},
		parentPageKeysM:  map[string]bool{},
		sortIntraPathsFn: sortIntraPathsFn,
	}
}

func (ppt *parentPageTreeT) Add(ipp paths.IntraPagePathT, linkS string) bool {
	if ipp.First() == linkS {
		return false
	}
	if ppt.parentPageKeysM[ipp.String()] {
		return false
	}
	ppt.parentPageKeysM[ipp.String()] = true
	ppt.parentPagesM[linkS] = append(ppt.parentPagesM[linkS], ipp)
	return true
}

func (ppt *parentPageTreeT) JoinIntraPagePaths(ipp paths.IntraPagePathT) (stringPathsL []StringPathT) {
	pathList := ppt.resolveIntraPagePath(ipp, interPagePathT{})
	for _, chain := range pathList.nestedList {
		ss := chain.StringPath()
		stringPathsL = append(stringPathsL, ss)
	}
	return stringPathsL
}

func (ppt *parentPageTreeT) resolveIntraPagePath(
	key paths.IntraPagePathT,
	tail interPagePathT,
) (resolvedChains *pathListT) {
	keyFirstS := key.First()
	added, ipp := tail.Prepend(key)
	if !added {
		return nil
	}

	if keyFirstS == ppt.entryPoint {
		return &pathListT{nestedList: []interPagePathT{ipp}}
	}

	parentPageKeysL := ppt.parentPagesM[keyFirstS]
	ppt.sortIntraPathsFn(parentPageKeysL)
	nestedList := []interPagePathT{}
	for _, parentPageKey := range parentPageKeysL {
		parentPaths := ppt.resolveIntraPagePath(parentPageKey, ipp)
		if parentPaths == nil {
			continue
		}
		nestedList = append(nestedList, parentPaths.nestedList...)
	}
	return &pathListT{nestedList: nestedList}
}
