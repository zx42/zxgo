package csv

func SkipEmptyLinesAtEOF(records [][]string) [][]string {
	l := len(records)
	for l > 0 && len(records[l-1]) == 0 {
		l--
	}
	records = records[:l]
	return records
}
