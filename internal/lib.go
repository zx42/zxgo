package internal

import (
	"gitlab.com/zx42/zxgo/log"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

func ExpandHome(dir string) string {
	if dir == "" || dir[0] != '~' {
		return dir
	}

	dirS := filepath.ToSlash(dir)
	if runtime.GOOS == "windows" {
		mutateString(&dirS, `^~`, "USERPROFILE")
	}
	mutateString(&dirS, `^~`, "HOME")
	dirSlash := filepath.ToSlash(dirS)
	return dirSlash
}

func mutateString(s *string, symbol, varName string) {
	*s = expandVar(*s, symbol, varName)
}

func expandVar(s, symbol, envVarName string) string {
	lg := log.New(false)

	envVarValue := os.Getenv(envVarName)
	if envVarValue == "" {
		return s
	}
	expanded := s

	symbolRx := regexp.MustCompile(symbol)
	foundIdx := symbolRx.FindAllStringIndex(s, -1)
	for i, x := range foundIdx {
		lg.Log("expandVar(%q, %q, %q): [%d]: %#v\n", s, symbol, envVarName, i, x)
		expanded = strings.Replace(expanded, s[x[0]:x[1]], envVarValue, 1)
	}
	if s != expanded {
		//panic(fmt.Sprintf("expandVar(%q, %q, %q): %q\n", s, symbol, envVarName, expanded))
		lg.Log("expandVar(%q, %q, %q): %q\n", s, symbol, envVarName, expanded)
	}
	return expanded
}


