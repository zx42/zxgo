package front

//import (
//	"fmt"
//	"time"
//
//	"github.com/maxence-charriere/go-app/v8/pkg/app"
//)
//
//type (
//	ErrorViewT struct {
//		app.Compo
//
//		errL []errEntryT
//	}
//	errEntryT struct {
//		err  error
//		when time.Time
//	}
//	errString string
//)
//
//func (es errString) Error() string {
//	return string(es)
//}
//
//func (ev *ErrorViewT) AddErrorString(errS string) bool {
//	if errS != "" {
//		ev.AddError(errString(errS))
//	}
//	return errS != ""
//}
//
//func (ev *ErrorViewT) AddError(err error) bool {
//	if err != nil {
//		ev.errL = append(ev.errL, errEntryT{err: err, when: time.Now()})
//	}
//	return err != nil
//}
//
//func (ev *ErrorViewT) Render() app.UI {
//	if len(ev.errL) == 0 {
//		return app.Div()
//	}
//
//	errItemsL := []app.UI{}
//	for i := 0; i < len(ev.errL); i++ {
//		if i < 5 {
//			errEntry := ev.errL[len(ev.errL)-i-1]
//			errItemsL = append(errItemsL, app.Li().Body(
//				app.Text(FormatTimeStamp(errEntry.when)),
//				app.Text(": "),
//				app.Text(errEntry.err.Error()),
//			))
//		}
//	}
//
//	return app.Div().Body(
//		app.H2().Text("Errors"),
//		app.Ul().Body(errItemsL...),
//	)
//}
//
//func FormatTimeStamp(ts time.Time) string {
//	return fmt.Sprintf("%s", ts.Local().
//		// Jan 2 15:04:05 2006 MST
//		Format("2006.01.02 - 15.04 Uhr"))
//}
