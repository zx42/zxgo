package env

import (
	"os"
	"strings"
)

func GetFirst(varNames ...string) (name string, value string) {
	for _, varName := range varNames {
		val := strings.TrimSpace(os.Getenv(varName))
		if val != "" {
			return varName, val
		}
	}
	return "", ""
}
