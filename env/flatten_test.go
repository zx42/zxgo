package env

import (
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/zx42/zxgo/osfs"
	"gitlab.com/zx42/zxgo/path"
)

type (
	dataT struct {
		String  string
		Bool    bool
		Int64   int64
		Float64 float64
		Sub     *dataT
	}
)

func TestSetEnv(t *testing.T) {
	Convey("Given some settings", t, func() {
		baseDir, clean := osfs.MkdirTemp("tmp", "env-*")
		dataIn := dataT{
			String:  "hello",
			Bool:    true,
			Int64:   -42,
			Float64: 42.42,
			Sub:     &dataT{Sub: &dataT{String: "sub.sub"}},
		}
		emptyPath := path.AbsPath{}
		nilEnv := baseDir.Join("nil.env").Abs()
		lowEnv := baseDir.Join("ToLower.env").Abs()
		upEnv := baseDir.Join("ToUpper.env").Abs()
		Convey("they should export to env and load back correctly no Format", func() {
			err := ExportDataToEnv("hpa", nil, dataIn, emptyPath)
			So(err, ShouldBeNil)
			var dataOut dataT
			err = LoadDataFromEnv("hpa", nil, &dataOut, emptyPath)
			So(err, ShouldBeNil)
			So(dataOut, ShouldResemble, dataIn)
			clean.IfOk(err)
		})
		Convey("they should export to env and load back correctly no Format with file store", func() {
			err := ExportDataToEnv("hpa", nil, dataIn, nilEnv)
			So(err, ShouldBeNil)
			var dataOut dataT
			err = LoadDataFromEnv("hpa", nil, &dataOut, nilEnv)
			So(err, ShouldBeNil)
			So(dataOut, ShouldResemble, dataIn)
			clean.IfOk(err)
		})
		Convey("they should export to env and load back correctly ToLower", func() {
			err := ExportDataToEnv("hpa", strings.ToLower, dataIn, emptyPath)
			So(err, ShouldBeNil)
			var dataOut dataT
			err = LoadDataFromEnv("hpa", strings.ToLower, &dataOut, emptyPath)
			So(err, ShouldBeNil)
			So(dataOut, ShouldResemble, dataIn)
			clean.IfOk(err)
		})
		Convey("they should export to env and load back correctly ToLower with file store", func() {
			err := ExportDataToEnv("hpa", strings.ToLower, dataIn, lowEnv)
			So(err, ShouldBeNil)
			var dataOut dataT
			err = LoadDataFromEnv("hpa", strings.ToLower, &dataOut, lowEnv)
			So(err, ShouldBeNil)
			So(dataOut, ShouldResemble, dataIn)
			clean.IfOk(err)
		})
		Convey("they should export to env and load back correctly ToUpper", func() {
			err := ExportDataToEnv("hpa", strings.ToUpper, dataIn, upEnv)
			So(err, ShouldBeNil)
			var dataOut dataT
			err = LoadDataFromEnv("hpa", strings.ToUpper, &dataOut, emptyPath)
			So(err, ShouldBeNil)
			So(dataOut, ShouldResemble, dataIn)
			clean.IfOk(err)
		})
		Convey("they should export to env and load back correctly ToUpper with file store", func() {
			err := ExportDataToEnv("hpa", strings.ToUpper, dataIn, upEnv)
			So(err, ShouldBeNil)
			var dataOut dataT
			err = LoadDataFromEnv("hpa", strings.ToUpper, &dataOut, upEnv)
			So(err, ShouldBeNil)
			So(dataOut, ShouldResemble, dataIn)
			clean.IfOk(err)
		})
		// Convey("they should export to env and load back correctly ToTitle", func() {
		//	err := ExportDataToEnv("hpa", strings.ToTitle, dataIn, "tmp/ToTitle.env")
		//	So(err, ShouldBeNil)
		//	var dataOut dataT
		//	err = LoadDataFromEnv("hpa", strings.ToTitle, &dataOut)
		//	So(err, ShouldBeNil)
		//	So(dataOut, ShouldResemble, dataIn)
		// })
		clean.Up("..")
	})
}
