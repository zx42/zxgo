package env

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"

	"gitlab.com/zx42/zxgo/path"
	"gitlab.com/zx42/zxgo/path/files"
)

type (
	VarList [][2]string
)

func ExportDataToEnv(varNamePrefix string, formatKey func(string) string, data any, fpath path.AbsPath) error {
	jsonB, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return fmt.Errorf("JSONizing settings: %w", err)
	}
	var settingsM map[string]any
	err = json.Unmarshal(jsonB, &settingsM)
	if err != nil {
		return fmt.Errorf("deJSONizing settings: %w", err)
	}
	settingsL, err := SetEnv(varNamePrefix, formatKey, settingsM)
	if err != nil {
		return err
	}
	if fpath.NotEmpty() {
		//err = WriteToEnvIn(fpath, varNamePrefix, formatKey, settingsM)
		err = settingsL.Store(fpath)
	}
	return err
}

func flatten(varNamePrefix string, formatKey func(string) string, settings any) VarList {
	//varNamePrefix = strings.ToUpper(varNamePrefix)
	//varNamePrefix = formatKey(varNamePrefix)
	varNamePrefix = applyFormatKey(varNamePrefix, formatKey)
	settingsL := make(VarList, 0, 20)
	switch stg := settings.(type) {
	case map[string]any:
		for key, value := range stg {
			if value == "" || value == nil /*|| value == 0*/ {
				continue
			}
			//pfxKey := pfx(varNamePrefix) + key
			pfxKey := getPfx(varNamePrefix, key)
			settingsL = append(settingsL, flatten(pfxKey, formatKey, value)...)
		}

	case []any:
		for i, value := range stg {
			//pfxKey := fmt.Sprintf("%s%d", pfx(varNamePrefix), i)
			pfxKey := getPfx(varNamePrefix, i)
			settingsL = append(settingsL, flatten(pfxKey, formatKey, value)...)
		}

	case bool, float64, string:
		//val := fmt.Sprintf(`"%v"`, stg)
		val := fmt.Sprintf(`%v`, stg)
		settingsL = append(settingsL, [2]string{varNamePrefix, val})

	default:
		panic(fmt.Sprintf("unknown setting type %T %#v", settings, settings))
	}

	sort.Slice(settingsL, func(i, j int) bool {
		return settingsL[i][0] < settingsL[j][0]
	})
	return settingsL
}

func applyFormatKey(s string, formatKey func(string) string) string {
	if formatKey == nil {
		return s
	}
	return formatKey(s)
}

type idxType interface {
	string | int
}

func getPfx[IT idxType](varNamePrefix string, idx IT) string {
	if varNamePrefix == "" {
		return fmt.Sprintf("%v", idx)
	}

	for strings.HasSuffix(varNamePrefix, "__") {
		varNamePrefix = varNamePrefix[:len(varNamePrefix)-2]
		//prefix += "__"
	}
	newPfx := fmt.Sprintf("%s__%v", varNamePrefix, idx)
	return newPfx
	//return prefix
}

//func pfx(prefix string) string {
//	if prefix != "" && !strings.HasSuffix(prefix, "__") {
//		prefix += "__"
//	}
//	return prefix
//}

//func flatten(settingsM map[string]any, prefix string) [][2]string {
//	if prefix != "" && !strings.HasSuffix(prefix, "_") {
//		prefix += "_"
//	}
//	settingsL := make([][2]string, 0, 20)
//	for key, val := range settingsM {
//		pfxKey := prefix + key
//		switch value := val.(type) {
//		case string:
//			settingsL = append(settingsL, [2]string{pfxKey, value})
//		case map[string]any:
//			settingsL = append(settingsL, flatten(value, pfxKey)...)
//		case map[string]any:
//			settingsL = append(settingsL, flatten(value, pfxKey)...)
//		}
//	}
//}

func SetEnv(varNamePrefix string, formatKey func(string) string, settings any) (VarList, error) {
	settingsL := flatten(varNamePrefix, formatKey, settings)
	//return settingsL, setEnv(settingsL)
	return settingsL, settingsL.setEnv()
}

func (varL VarList) setEnv(
// settingsL [][2]string,
) error {
	for _, stg := range varL {
		envVarName := stg[0]
		value := stg[1]
		err := os.Setenv(envVarName, value)
		if err != nil {
			return fmt.Errorf("setting env var %q: %w", envVarName, err)
		}
	}
	return nil
}

//func WriteEnv(prefix string, settings any,
//
////export bool
//) error {
//	mainRepoDir, _ := GetRepoDirs()
//	fname := mainRepoDir.Join("build", "env")
//	return WriteToEnvIn(fname, prefix, settings) //, export)
//}

func (varL VarList) Store(fname path.AbsPath) error {
	//settingsL := flatten(varNamePrefix, formatKey, settings)
	//if export {
	//	err := setEnv(settingsL)
	//	if err != nil {
	//		return fmt.Errorf("export file %q: %w", fname, err)
	//	}
	//}
	//et := toEnvText(settingsL)
	et := varL.toEnvText()
	err := files.WriteFile(fname, []byte(et))
	if err != nil {
		return fmt.Errorf("write file %q: %w", fname, err)
	}
	return nil
}

//func WriteToEnvIn(fname path.AbsPath, varNamePrefix string, formatKey func(string) string, settings any) error {
//	settingsL := flatten(varNamePrefix, formatKey, settings)
//	//if export {
//	//	err := setEnv(settingsL)
//	//	if err != nil {
//	//		return fmt.Errorf("export file %q: %w", fname, err)
//	//	}
//	//}
//	et := toEnvText(settingsL)
//	err := file.WriteFile(fname, []byte(et))
//	if err != nil {
//		return fmt.Errorf("write file %q: %w", fname, err)
//	}
//	return nil
//}

func (varL VarList) toEnvText(
// settingsL [][2]string,
) string {
	sbld := strings.Builder{}
	for _, stg := range varL {
		line := fmt.Sprintf("%s=%s\n", stg[0], stg[1])
		sbld.WriteString(line)
	}
	return sbld.String()
}
