package env

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

func LoadDataFromEnv(varNamePrefix string, formatKey func(string) string, data any, fpath path.AbsPath) error {
	if fpath.NotEmpty() {
		f, err := os.Open(fpath.String())
		if err != nil {
			return fmt.Errorf("LoadDataFromEnv(): open: %w", err)
		}
		defer must.Func(f.Close)
		scnr := bufio.NewScanner(f)
		allVarsL := make([]string, 0, 10)
		for scnr.Scan() {
			allVarsL = append(allVarsL, strings.TrimSpace(scnr.Text()))
		}
		if scnr.Err() != nil {
			return fmt.Errorf("LoadDataFromEnv(): scan lines: %w", err)
		}
		vL := parseVars(varNamePrefix, allVarsL)
		err = vL.setEnv()
		if err != nil {
			return fmt.Errorf("LoadDataFromEnv(): set env: %w", err)
		}
	}

	varL := GetVarsWithPrefix(varNamePrefix)
	if len(varL) == 0 {
		return fmt.Errorf("could not find env vars with prefix %q", varNamePrefix)
	}
	varsM := varL.deflatten(formatKey)
	key := applyFormatKey(varNamePrefix, formatKey)
	dataM, ok := varsM[key]
	if !ok {
		return fmt.Errorf("could not find subset with prefix %q", varNamePrefix)
	}

	jsonB, err := json.MarshalIndent(dataM, "", "  ")
	if err != nil {
		return fmt.Errorf("JSONizing settings: %w", err)
	}

	err = json.Unmarshal(jsonB, &data)
	if err != nil {
		return fmt.Errorf("deJSONizing settings: %w", err)
	}
	return nil
}

func GetVarsWithPrefix(varNamePrefix string) VarList {
	varNamePrefix = getPfx(varNamePrefix, "")
	allEnvVarsL := os.Environ()
	return parseVars(varNamePrefix, allEnvVarsL)
}

func parseVars(varNamePrefix string, allEnvVarsL []string) VarList {
	varsL := make(VarList, 0, len(allEnvVarsL))
	for _, line := range allEnvVarsL {
		varDef := strings.SplitN(line, "=", 2)
		if varNamePrefix == "" || strings.HasPrefix(strings.ToLower(varDef[0]), strings.ToLower(varNamePrefix)) {
			varsL = append(varsL, [2]string{varDef[0], varDef[1]})
		}
	}
	return varsL
}

func (varL VarList) deflatten(formatKey func(string) string) map[string]any {
	varsM := make(map[string]any)
	for _, varDef := range varL {
		varsM = insMap(varsM, varDef[0], formatKey, varDef[1])
	}
	return varsM
}

func insMap(varsM map[string]any, key string, formatKey func(string) string, value string) map[string]any {
	keyL := strings.SplitN(key, "__", 2)
	if len(keyL) == 1 {
		key = applyFormatKey(key, formatKey)
		return insValue(varsM, key, value)
	}
	key0 := applyFormatKey(keyL[0], formatKey)
	v, ok := varsM[key0]
	if !ok {
		v = make(map[string]any)
	}
	m := v.(map[string]any)
	varsM[key0] = insMap(m, keyL[1], formatKey, value)
	return varsM
}

func insValue(varsM map[string]any, key, value string) map[string]any {
	i64, err := strconv.ParseInt(value, 10, 64)
	if err == nil {
		varsM[key] = i64
		return varsM
	}

	f64, err := strconv.ParseFloat(value, 64)
	if err == nil {
		varsM[key] = f64
		return varsM
	}

	flag, err := strconv.ParseBool(value)
	if err == nil {
		varsM[key] = flag
		return varsM
	}

	varsM[key] = value
	return varsM
}

func CleanDataFromEnv(varNamePrefix string, fpath path.AbsPath) error {
	if fpath.NotEmpty() {
		err := os.Remove(fpath.String())
		if err != nil && !errors.Is(err, os.ErrNotExist) {
			return fmt.Errorf("CleanDataFromEnv(): remove: %w", err)
		}
	}

	varL := GetVarsWithPrefix(varNamePrefix)
	for _, varDef := range varL {
		err := os.Unsetenv(varDef[0])
		if err != nil {
			return fmt.Errorf("CleanDataFromEnv(): unset %q: %w", varDef, err)
		}
	}
	return nil
}
