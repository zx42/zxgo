package zip

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"os"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
	"gitlab.com/zx42/zxgo/path/files"
)

type (
	// Deprecated: use Writer instead
	ZipperT struct {
		archiveFileName path.AbsPath
		archiveW        io.ReadWriteCloser
		zipW            *zip.Writer
		stripSubFolders bool
		verbose         bool
		//noRenameOld     bool
	}
)

// New creates a new Zipper object.
// If a zip archive with the same name already exists it will be renamed to a backup file with a time stamp in the name.
// If the folder for the zip file does not exist it will be created.
//
// Deprecated: use zip.CreateFile() instead
func New(archiveFile path.AbsPath, noRenameOld bool, stripSubFolders bool, verbose bool) (zpr *ZipperT, errOut error) {
	defer errs.DeferWrapf(&errOut, "new zipper(%s)", archiveFile)

	zpr = &ZipperT{
		archiveFileName: archiveFile,
		stripSubFolders: stripSubFolders,
		verbose:         verbose,
	}
	fi, err := archiveFile.Stat()
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return nil, fmt.Errorf("stat zip archive: %w", err)
		}
		// zip file does not exist (err is os.ErrNotExist)
	} else {
		// zip file does exist already
		if fi.IsDir() {
			return nil, fmt.Errorf("zip archive is a directory")
		}

		if noRenameOld {
			err = fmt.Errorf("zip archive exists already and rename to old disabled")
			return nil, err

		}
		// rename existing zip file to backup name
		err = files.RenameOld(archiveFile)
		if err != nil {
			err = fmt.Errorf("zip archive exists already. rename to old failed: %w", err)
			return nil, err
		}
	}

	archiveDir := archiveFile.AbsDir()
	err = archiveDir.MkdirAll(0777)
	if err != nil {
		return nil, fmt.Errorf("creating zip archive dir %q: %w", archiveDir, err)
	}
	//if err := path.EnsureParentFolderExists(archiveFile); err != nil {
	//	return nil, fmt.Errorf("creating zip archive dir for %q: %w", archiveFile, err)
	//}
	zipf, err := archiveFile.Create()
	if err != nil {
		return nil, fmt.Errorf("creating zip archive: %w", err)
	}
	zpr.archiveW = zipf
	zpr.zipW = zip.NewWriter(zipf)
	return zpr, nil
}

//// New creates a new Zipper object.
//// If a zip archive with the same name already exists it will be renamed to a backup file with a time stamp in the name.
//// If the folder for the zip file does not exist it will be created.
//func New(archiveFile path.AbsPath, noRenameOld bool, stripSubFolders bool, verbose bool) (zpr *Writer, errOut error) {
//	defer errs.DeferWrapf(&errOut, "new zipper(%s)", archiveFile)
//
//	zpr = &Writer{
//		archiveFileName: archiveFile,
//		stripSubFolders: stripSubFolders,
//		verbose:         verbose,
//	}
//	fi, err := archiveFile.Stat()
//	if err != nil {
//		if !errors.Is(err, os.ErrNotExist) {
//			return nil, fmt.Errorf("stat zip archive: %w", err)
//		}
//		// zip file does not exist (err is os.ErrNotExist)
//	} else {
//		// zip file does exist already
//		if fi.IsDir() {
//			return nil, fmt.Errorf("zip archive is a directory")
//		}
//
//		if noRenameOld {
//			err = fmt.Errorf("zip archive exists already and rename to old disabled")
//			return nil, err
//
//		}
//		// rename existing zip file to backup name
//		err = files.RenameOld(archiveFile)
//		if err != nil {
//			err = fmt.Errorf("zip archive exists already. rename to old failed: %w", err)
//			return nil, err
//		}
//	}
//
//	archiveDir := archiveFile.AbsDir()
//	err = archiveDir.MkdirAll(0777)
//	if err != nil {
//		return nil, fmt.Errorf("creating zip archive dir %q: %w", archiveDir, err)
//	}
//	zipf, err := archiveFile.CreateFile()
//	if err != nil {
//		return nil, fmt.Errorf("creating zip archive: %w", err)
//	}
//	zpr.archiveW = zipf
//	zpr.zipW = zip.NewWriter(zipf)
//	return zpr, nil
//}

func (zpr *ZipperT) StripSubFolders(strip bool) *ZipperT {
	zpr.stripSubFolders = strip
	return zpr
}

func (zpr *ZipperT) Verbose(verbose bool) *ZipperT {
	zpr.verbose = verbose
	return zpr
}

func (zpr *ZipperT) Close() (err error) {
	defer errs.DeferWrapf(&err, "close zipper(%s)", zpr.archiveFileName)
	if zpr.zipW != nil {
		defer errs.DeferWrapf(&err, "close zip writer")
		err = zpr.zipW.Close()
	}
	if err == nil && zpr.archiveW != nil {
		defer errs.DeferWrapf(&err, "close zip file")
		err = zpr.archiveW.Close()
	}
	return err
}

func (zpr *ZipperT) AddFile(fullPath path.AbsPath, fRelPath path.RelPath) (errOut error) {
	defer errs.DeferWrapf(&errOut, "zipper.AddFile(%q as %q) in %q", fullPath, fRelPath, zpr.archiveFileName)
	f, err := fullPath.Open()
	if err != nil {
		return fmt.Errorf("open source file: %w", err)
	}
	defer must.Func(f.Close)
	return zpr.AddReader(fRelPath, f)
}

func (zpr *ZipperT) AddBytes(fRelPath path.RelPath, b []byte) error {
	buf := bytes.NewBuffer(b)
	return zpr.AddReader(fRelPath, buf)
}

func (zpr *ZipperT) AddReader(fRelPath path.RelPath, r io.Reader) (errOut error) {
	defer errs.DeferWrapf(&errOut, "zipper.AddReader(%q) in %q", fRelPath, zpr.archiveFileName)
	fnameS := fRelPath.String()
	if zpr.stripSubFolders {
		fnameS = fRelPath.Base()
	}
	w, err := zpr.zipW.Create(fnameS)
	if err != nil {
		return fmt.Errorf("create zip writer for %q: %w", fnameS, err)
	}
	buf := make([]byte, 1024*1024)
	_, err = io.CopyBuffer(w, r, buf)
	if err != nil {
		return fmt.Errorf("copy bytes into zip: %w", err)
	}
	if zpr.verbose {
		slog.Info("zipper.Add() done", "in-zip-file", fRelPath, "zip-archive-file", zpr.archiveFileName)
	}
	return nil
}
