package zip

import (
	"fmt"
	"os"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/etc"
	"gitlab.com/zx42/zxgo/path"
)

func Folder(
	dir path.AbsDir, zipFile path.AbsPath,
	includeGlobs, excludeGlobs []path.Glob,
	logFileError func(de path.DirEntry, err error) error,
	// noRenameOldZipArchive bool,
) (errOut error) {
	defer errs.DeferWrapf(&errOut, "zip folder %q into %q", dir, zipFile)

	if logFileError == nil {
		logFileError = path.LogFileError
	}

	zpr, err := CreateFile(zipFile)
	if err != nil {
		return fmt.Errorf("create zip file %q: %w", zipFile, err)
	}
	defer etc.Close(&errOut, zpr.Close, "failed to close zip archive file")
	//defer func() {
	//	err := zpr.Close()
	//	if errOut == nil && err != nil {
	//		errOut = err
	//	}
	//}()

	dirFs := os.DirFS(dir.String())
	err = path.Walk(dirFs,
		path.ShouldInExclude(includeGlobs...)(excludeGlobs...),
		func(de path.DirEntry) error {
			fpath := dir.Append(de.RelPath()).Abs()
			err = zpr.AddFile(fpath, de.RelPath())
			if err != nil {
				return err
			}
			return nil
		},
		logFileError,
	)
	if err != nil {
		return err
	}
	return nil
}

// Deprecated: use zip.Folder instead
func ZipFolder(
	dir path.AbsDir, zipFile path.AbsPath,
	includeGlobs, excludeGlobs []path.Glob,
	logFileError func(de path.DirEntry, err error) error,
	noRenameOldZipArchive bool,
) (errOut error) {
	defer errs.DeferWrapf(&errOut, "zip folder %q into %q", dir, zipFile)

	if logFileError == nil {
		logFileError = path.LogFileError
	}

	zpr, err := New(zipFile, noRenameOldZipArchive, false, false)
	if err != nil {
		return err
	}
	defer func() {
		err := zpr.Close()
		if errOut == nil && err != nil {
			errOut = err
		}
	}()

	dirFs := os.DirFS(dir.String())
	err = path.Walk(dirFs,
		path.ShouldInExclude(includeGlobs...)(excludeGlobs...),
		func(de path.DirEntry) error {
			fpath := dir.Append(de.RelPath()).Abs()
			err = zpr.AddFile(fpath, de.RelPath())
			if err != nil {
				return err
			}
			return nil
		},
		logFileError,
	)
	if err != nil {
		return err
	}
	return nil
}
