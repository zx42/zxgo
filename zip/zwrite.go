package zip

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"time"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

type (
	Writer struct {
		File            path.AbsPath
		archiveW        io.ReadWriteCloser
		zipW            *zip.Writer
		StripSubFolders bool
		Verbose         bool
	}
)

var (
	ErrIsFolder = errors.New("is a directory")
)

// CreateFile creates a new zip.Writer object.
// If a zip archive with the same name already exists it will return an error.
// (You can use files.RenameOld() before.)
// If the folder for the zip file does not exist it will be created.
func CreateFile(archiveFile path.AbsPath) (zpr *Writer, errOut error) {
	defer errs.DeferWrapf(&errOut, "new zip.Writer(%s)", archiveFile)

	zpr = &Writer{
		File: archiveFile,
	}
	fi, err := archiveFile.Stat()
	if err != nil {
		if !errors.Is(err, fs.ErrNotExist) {
			return nil, fmt.Errorf("stat zip archive: %w", err)
		}
		// zip file does not exist (err is os.ErrNotExist)
	} else {
		// zip file does exist already
		if fi.IsDir() {
			return nil, fmt.Errorf("zip archive: %w", ErrIsFolder)
		}

		err = fmt.Errorf("create zip archive: %w", fs.ErrExist)
		return nil, err
	}

	zipf, err := archiveFile.Create()
	if err != nil {
		return nil, fmt.Errorf("creating zip archive: %w", err)
	}
	zpr.archiveW = zipf
	zpr.zipW = zip.NewWriter(zipf)
	return zpr, nil
}

// TODO: add constructor which adds files to existing zip archive by first copying the entries from
//       the old existing zip archive into the newly created one
//func x() {
//	zr, err := zip.OpenReader(zipPath)
//	defer zr.Close()
//	zwf, err := os.CreateFile(targetFilePath)
//	defer zwf.Close()
//	zw := zip.NewWriter(zwf)
//	defer zwf.Close() // or not... since it will try to wrote central directory
//
//	for _, zipItem := range zrw.File {
//		if isOneOfNamesWeWillAdd(zipItem.Name) {
//			continue // avoid duplicate files!
//		}
//		zipItemReader, err := zipItem.OpenRaw()
//		header := zipItem.FileHeader                          // clone header data
//		targetItem, err := targetZipWriter.CreateRaw(&header) // use cloned data
//		_, err = io.Copy(targetItem, zipItemReader)
//	}
//
//	addNewFiles(zw) // IMPLEMENT YOUR LOGIC
//}

func (zwr *Writer) Close() (err error) {
	defer errs.DeferWrapf(&err, "close zipper(%s)", zwr.File)
	if zwr.zipW != nil {
		defer errs.DeferWrapf(&err, "close zip writer")
		err = zwr.zipW.Close()
	}
	if err == nil && zwr.archiveW != nil {
		defer errs.DeferWrapf(&err, "close zip file")
		err = zwr.archiveW.Close()
	}
	return err
}

func (zwr *Writer) AddFile(fullPath path.AbsPath, fRelPath path.RelPath) (errOut error) {
	defer errs.DeferWrapf(&errOut, "zipper.AddFile(%q as %q) in %q", fullPath, fRelPath, zwr.File)
	f, err := fullPath.Open()
	if err != nil {
		return fmt.Errorf("open source file: %w", err)
	}
	defer must.Func(f.Close)
	fi, err := f.Stat()
	if err != nil {
		return fmt.Errorf("stat source file: %w", err)
	}
	return zwr.AddTimedReader(fRelPath, f, fi.ModTime())
}

func (zwr *Writer) AddBytes(fRelPath path.RelPath, b []byte) error {
	buf := bytes.NewBuffer(b)
	return zwr.AddReader(fRelPath, buf)
}

func (zwr *Writer) AddReader(fRelPath path.RelPath, r io.Reader) (errOut error) {
	return zwr.addOptTimedReader(fRelPath, r, nil)
}

func (zwr *Writer) AddTimedReader(fRelPath path.RelPath, r io.Reader, ftime time.Time) (errOut error) {
	return zwr.addOptTimedReader(fRelPath, r, &ftime)
}

func (zwr *Writer) addOptTimedReader(fRelPath path.RelPath, r io.Reader, ftime *time.Time) (errOut error) {
	defer errs.DeferWrapf(&errOut, "zipper.AddReader(%q) in %q", fRelPath, zwr.File)
	fnameS := fRelPath.String()
	if zwr.StripSubFolders {
		fnameS = fRelPath.Base()
	}
	fHdr := &zip.FileHeader{
		Name: fnameS,
	}
	if ftime != nil {
		fHdr.Modified = *ftime
	}
	w, err := zwr.zipW.CreateHeader(fHdr)
	if err != nil {
		return fmt.Errorf("create zip writer for %q: %w", fnameS, err)
	}
	buf := make([]byte, 1024*1024)
	_, err = io.CopyBuffer(w, r, buf)
	if err != nil {
		return fmt.Errorf("copy bytes into zip: %w", err)
	}
	if zwr.Verbose {
		slog.Info("zipper.Add() done", "in-zip-file", fRelPath, "zip-archive-file", zwr.File)
	}
	return nil
}

func (zwr *Writer) AddData(fRelPath path.RelPath, marshal func(any) ([]byte, error), data any) (err error) {
	defer errs.DeferWrapf(&err, "zip.Writer.AddData(%q) in %q", fRelPath, zwr.File)
	dataB, err := marshal(data)
	if err != nil {
		return fmt.Errorf("marshal data %q in zip file: %w", fRelPath.String(), err)
	}
	err = zwr.AddBytes(fRelPath, dataB)
	if err != nil {
		return err
	}
	return nil
}
