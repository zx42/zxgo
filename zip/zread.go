package zip

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/fs"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/path"
)

type (
	Reader struct {
		*zip.ReadCloser
		//f *io.Closer
	}
)

//var (
//	// Deprecated: use OpenFileReader instead
//	NewFileReader = OpenFileReader
//)

func OpenFileReader(zipFile path.Path) (*Reader, error) {
	zr, err := zip.OpenReader(zipFile.String())
	if err != nil {
		return nil, fmt.Errorf("new zip reader of %q: %w", zipFile, err)
	}
	return &Reader{ReadCloser: zr}, nil
}

// Deprecated: use OpenFileReader instead
func NewFileReader(fname string) (*Reader, error) {
	return OpenFileReader(path.New(fname))
}

//// Deprecated: use OpenFileReader instead
//func NewFileReader(fname string) (*Reader, error) {
//	fr, err := os.Open(fname)
//	if err != nil {
//		return nil, fmt.Errorf("open %q: %w", fname, err)
//	}
//	fi, err := fr.Stat()
//	if err != nil {
//		return nil, fmt.Errorf("stat %q: %w", fname, err)
//	}
//	zr, err := zip.NewReader(fr, fi.Size())
//	if err != nil {
//		return nil, fmt.Errorf("new zip reader of %q: %w", fname, err)
//	}
//	return &Reader{ReadCloser: zr}, nil
//}

func (r Reader) ForAllPackedFiles(cb func(fname path.RelPath, r io.Reader) error, failEarly bool, globs ...path.Glob) error {
	var errL []error
	for _, zfi := range r.File {
		zf, err := r.Open(zfi.Name)
		if err != nil {
			if failEarly {
				return fmt.Errorf("open zip file %q: %w", zfi.Name, err)
			}
			errL = append(errL, fmt.Errorf("open %q: %w", zfi.Name, err))
			continue
		}

		if !matches(zfi.Name, globs) {
			continue
		}

		fname := path.New(zfi.Name).AsRel()
		err = cb(fname, zf)
		if err != nil {
			if failEarly {
				return fmt.Errorf("handling zip file %q: %w", zfi.Name, err)
			}
			errL = append(errL, fmt.Errorf("handle %q: %w", zfi.Name, err))
		}
	}
	return errs.Join(errL...)
}

func matches(s string, globs []path.Glob) bool {
	if len(globs) == 0 {
		return true
	}
	for _, glob := range globs {
		if glob.Matches(s) {
			return true
		}
	}
	return false
}

func (r Reader) DumpFile(srcFName path.RelPath, tgtFPath path.AbsPath, perm fs.FileMode) error {
	bufB, err := r.ReadBytes(srcFName)
	if err != nil {
		return fmt.Errorf("unpack file %q in zip file: %w", srcFName.String(), err)
	}
	err = tgtFPath.WriteFile(bufB, perm)
	if err != nil {
		return fmt.Errorf("write file %q from zip file to %q: %w",
			srcFName.String(), tgtFPath.String(), err)
	}
	return nil
}

func (r Reader) UnmarshalFile(fname path.RelPath, unmarshal func([]byte, any) error, data any) error {
	bufB, err := r.ReadBytes(fname)
	if err != nil {
		return fmt.Errorf("unpack file %q in zip file: %w", fname.String(), err)
	}
	err = unmarshal(bufB, data)
	if err != nil {
		return fmt.Errorf("unmarshal buffer of file %q in zip file: %w", fname.String(), err)
	}
	return nil
}

func (r Reader) ReadBytes(fname path.RelPath) ([]byte, error) {
	var fileBuf bytes.Buffer
	err := r.CopyTo(fname, &fileBuf)
	if err != nil {
		return nil, fmt.Errorf("read stream in zip file: %w", err)
	}
	return fileBuf.Bytes(), nil
}

func (r Reader) CopyTo(fname path.RelPath, w io.Writer) error {
	fileRdr, err := r.Open(fname.String())
	if err != nil {
		return fmt.Errorf("open file %q in zip file: %w", fname.String(), err)
	}
	_, err = io.Copy(w, fileRdr)
	if err != nil {
		return fmt.Errorf("read file %q in zip file: %w", fname.String(), err)
	}
	return nil
}
