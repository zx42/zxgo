package slog

import (
	"io"
	"log/slog"
)

type (
	// Logger is a small wrapper
	//
	// Deprecated: Use log/slog from the standard library of Go 1.21+
	Logger interface {
		Info(msg string, kvs ...any)
	}
)

var (
	defaultLogger = New()
)

// Info is a small wrapper
//
// Deprecated: Use log/slog from the standard library of Go 1.21+
func Info(msg string, kvs ...any) {
	defaultLogger.Info(msg, kvs...)
}

// Error is a small wrapper
//
// Deprecated: Use log/slog from the standard library of Go 1.21+
func Error(msg string, err error, kvs ...any) {
	if len(kvs)%2 == 1 {
		panic("need even number of arguments")
	}
	kvs = append(kvs, "err", err)
	defaultLogger.Error(msg, kvs...)
}

// New is a small wrapper
//
// Deprecated: Use log/slog from the standard library of Go 1.21+
func New() *slog.Logger {
	//defLogger := slog.New(slog.NewTextHandler(os.Stdout))
	//slog.SetDefault(defLogger)
	return slog.Default()
}

// NewTextNew is a small wrapper
//
// Deprecated: Use log/slog from the standard library of Go 1.21+
func NewText(w io.Writer) *slog.Logger {
	logger := slog.New(slog.NewTextHandler(w, nil))
	//slog.SetDefault(logger)
	return logger
}

// NewJSON is a small wrapper
//
// Deprecated: Use log/slog from the standard library of Go 1.21+
func NewJSON(w io.Writer) *slog.Logger {
	logger := slog.New(slog.NewJSONHandler(w, nil))
	//slog.SetDefault(logger)
	return logger
}
