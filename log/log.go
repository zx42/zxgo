package log

import (
	"log"
)

type (
	LoggerT struct {
		verbose bool
	}
)

func New(verbose bool) *LoggerT {
	return &LoggerT{verbose: verbose}
}

func (lgr *LoggerT) Log(fmtS string, args ...interface{}) {
	if lgr.verbose {
		lgr.logMsg(fmtS, args...)
	}
}

func (lgr *LoggerT) LogErr(fmtS string, args ...interface{}) {
	lgr.logMsg("ERROR: "+fmtS, args...)
}

func (lgr *LoggerT) logMsg(fmtS string, args ...interface{}) {
	log.Printf(fmtS, args...)
}
