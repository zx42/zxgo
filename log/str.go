package log

import "fmt"

func StrMax(maxLen int, fmtS string, args ...interface{}) string {
	s := fmt.Sprintf(fmtS, args...)
	if len(s) > maxLen {
		s = s[:maxLen-3] + "..."
	}
	return s
}

