package etc

import (
	"errors"
	"log/slog"
	"runtime/debug"
	"strings"
	"time"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/slice"
)

func Retry(callF func() error, maxRetries int, sleepFor time.Duration, errorsToReturn ...error) (err error) {
	for i := 0; i < maxRetries; i++ {
		err = callF()
		if err == nil {
			return nil
		}
		for _, errToReturn := range errorsToReturn {
			if errors.Is(err, errToReturn) {
				return err
			}
		}
		time.Sleep(sleepFor)
	}
	return err
}

func TimeStamp(time time.Time, precision string) string {
	if precision == "" {
		precision = "minute"
	}
	// Mon Jan 2 15:04:05 -0700 MST 2006
	switch strings.ToLower(precision)[:1] {
	case "y":
		return time.Format("2006")
	case "d":
		return time.Format("2006.01.02")
	case "s":
		return time.Format("2006.01.02-15.04.05")
	default:
		return time.Format("2006.01.02-15.04")
	}
}

func Now(precision string) string {
	return TimeStamp(time.Now(), precision)
}

func HappyPath(err *error) func(func()) {
	return func(act func()) {
		if *err == nil {
			act()
		}
	}
}

// Close calls f(). if an error is returned, it will be stored in pErrOut if *pErrOut is nil.
// if args... starts with some errors these will be ignored.
// the rest of args will be used as input to fmt.Errorf()
func Close(pErrOut *error, f func() error, args ...any) {
	err := f()
	//if err == nil || *pErrOut != nil || errors.Is(err, fs.ErrClosed) {
	if err == nil || *pErrOut != nil {
		return
	}
	exceptIsErrs, args := slice.SplitByType[error](args)
	for _, eie := range exceptIsErrs {
		if errors.Is(err, eie) {
			return
		}
	}

	must.OrDo(err, args, func(formattedErr any) {
		*pErrOut = formattedErr.(error)
	})
}

func LogPanic(funcName string, args ...any) func(registerPanics ...func(panicObj any)) {
	return func(registerPanics ...func(panicObj any)) {
		if panicObj := recover(); panicObj != nil {
			args = append([]any{"function", funcName, "panic", panicObj}, args...)
			args = append(args, "stacktrace", string(debug.Stack()))
			slog.Error("panic", args...)
			for _, registerPanic := range registerPanics {
				registerPanic(panicObj)
			}
		}
	}
}
