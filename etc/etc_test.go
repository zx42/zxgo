package etc_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/zx42/zxgo/etc"
)

func TestLogPanic(t *testing.T) {
	Convey("Given a panic", t, func() {
		defer etc.LogPanic("TestLogPanic")(func(panicObj any) {
			So(panicObj, ShouldNotBeNil)
			So(panicObj, ShouldEqual, "oops")
		})
		panic("oops")
	})
}
