package etc

import (
	"fmt"
	"regexp"
)

type (
	StringMatcher interface {
		fmt.Stringer
		Match(string) bool
	}
	MatchEqualT struct {
		searchStr string
	}
	MatchRegexT struct {
		searchRx *regexp.Regexp
	}
)

func MatchEqual(searchStr string) StringMatcher {
	return &MatchEqualT{searchStr: searchStr}
}

func (me MatchEqualT) Match(searchStr string) bool {
	return me.searchStr == searchStr
}

func (me MatchEqualT) String() string {
	return me.searchStr
}

func MatchRegex(searchRxS string) StringMatcher {
	return &MatchRegexT{searchRx: regexp.MustCompile(searchRxS)}
}

func (mr MatchRegexT) Match(searchStr string) bool {
	return mr.searchRx.MatchString(searchStr)
}

func (mr MatchRegexT) String() string {
	return mr.searchRx.String()
}

