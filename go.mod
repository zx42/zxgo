module gitlab.com/zx42/zxgo

go 1.21

require (
	github.com/alecthomas/kong v0.6.1
	github.com/fsnotify/fsnotify v1.5.4
	github.com/go-resty/resty/v2 v2.7.0
	github.com/onsi/ginkgo v1.16.5
	github.com/smartystreets/goconvey v1.7.2
	golang.org/x/exp v0.0.0-20221114191408-850992195362
	golang.org/x/tools v0.2.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-task/slim-sprig v0.0.0-20210107165309-348f09dbbbc0 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/smartystreets/assertions v1.2.0 // indirect
	golang.org/x/mod v0.6.0 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
