package path

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"
	"path/filepath"
	"runtime"
	"slices"
	"strings"

	"gitlab.com/zx42/zxgo/internal"
	"gitlab.com/zx42/zxgo/must"
)

type (
	Path interface {
		String() string
		IsEmpty() bool
		NotEmpty() bool
		Base() string
		Ext() string
		BaseName() string
		WoExt() string
		InsBeforeExt(inFix string) Path
		ToSlash() Path
		FromSlash() Path
		Clean() Path
		ExpandHome() Path
		EvalSymlinks() (Path, error)
		Abs() AbsPath
		// Dir()  AbsDir
		Dir() Path
		AbsDir() AbsDir
		AsDir() AbsDir
		AsRel() RelPath
		Join(names ...string) Path
		Append(relPath RelPath) Path
		IsAbs() bool
		IsDir() bool

		Stat() (fs.FileInfo, error)
		Open() (*os.File, error)
		Create() (*os.File, error)
		CreateForced() (*os.File, error)
		Exists() bool
		RenameTo(newPath Path) error
		ReadFile() ([]byte, error)
		WriteFile(data []byte, perm fs.FileMode) error
		Sha256Hex() (string, error)

		io.WriterTo
	}
	path struct {
		string
	}
	RelPath struct {
		path
	}
	AbsPath struct {
		path
	}
)

var (
	FollowSymlinks    = true
	ExpandTilde       = true
	ConvertAllToSlash = true
)

func New(s string) Path {
	ps := s
	if ConvertAllToSlash {
		ps = filepath.ToSlash(s)
	}
	p := newPath(ps)
	return p
}

func newPath(s string) Path {
	//for len(s) > 1 && filepath.ToSlash(s) != "/" && strings.HasSuffix(filepath.ToSlash(s), "/") {
	//	s = s[:len(s)-1]
	//}
	s = normalizePath(s)
	var p Path = path{string: s}
	if p.IsAbs() {
		p = AbsPath{path: p.(path)}
	} else {
		p = RelPath{path: p.(path)}
	}
	return p
}

func newAbsPath(s string) AbsPath {
	var p Path = path{string: s}
	if p.IsAbs() {
		return AbsPath{path: p.(path)}
	}
	panic(fmt.Sprintf("need absolute path but got %q", s))
}

func newRelPath(s string) RelPath {
	var p Path = path{string: s}
	if p.IsAbs() {
		panic(fmt.Sprintf("need relative path but got %q", s))
	}
	return RelPath{path: p.(path)}
}

func (p path) String() string {
	return p.string
}

func (p path) IsEmpty() bool {
	return p.string == ""
}

func (p path) NotEmpty() bool {
	return p.string != ""
}

func (p path) Base() string {
	return filepath.Base(p.string)
}

func (p path) Ext() string {
	return filepath.Ext(p.string)
}

func (p path) BaseName() string {
	le := len(p.Ext())
	base := p.Base()
	lb := len(base)
	return base[:lb-le]
}

func (p path) WoExt() string {
	le := len(p.Ext())
	lp := len(p.string)
	return p.string[:lp-le]
}

func InsBeforeExt(fpath Path, inFix string) Path {
	fext := fpath.Ext()
	le := len(fext)
	lp := len(fpath.String())
	pNew := fpath.String()[:lp-le] + inFix + fext
	return path{string: pNew}
}

func (p path) InsBeforeExt(inFix string) Path {
	return InsBeforeExt(p, inFix)
	//fext := p.Ext()
	//le := len(fext)
	//lp := len(p.string)
	//pNew := p.string[:lp-le] + inFix + fext
	//return path{string: pNew}
	//// return New(pNew)
}

func ToSlash(p Path) Path {
	return path{string: filepath.ToSlash(p.String())}
}

func (p path) ToSlash() Path {
	return ToSlash(p)
	//return path{string: filepath.ToSlash(p.string)}
}

func FromSlash(p Path) Path {
	return path{string: filepath.FromSlash(p.String())}
}

func (p path) FromSlash() Path {
	return FromSlash(p)
	//return path{string: filepath.FromSlash(p.string)}
}

func Clean(p Path) Path {
	clean := filepath.Clean(p.String())
	if runtime.GOOS == "windows" && clean[1] == ':' {
		clean = strings.ToUpper(clean[:1]) + clean[1:]
	}
	return path{string: clean}
}

func (p path) Clean() Path {
	return Clean(p)
}

func ExpandHome(p Path) Path {
	pS := internal.ExpandHome(p.String())
	return path{string: pS}
}

func (p path) ExpandHome() Path {
	return ExpandHome(p)
}

func EvalSymlinks(p Path) (Path, error) {
	woSymLinks, err := filepath.EvalSymlinks(p.String())
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			fname := p.Base()
			fdir := p.AbsDir()
			pathWoSymLinks, err := EvalSymlinks(fdir)
			if err == nil {
				return pathWoSymLinks.Join(fname), nil
			}
		}
		return p, fmt.Errorf("EvalSymlinks(%q): %w", p, err)
	}
	return path{string: woSymLinks}, nil
}

func (p path) EvalSymlinks() (Path, error) {
	return EvalSymlinks(p)
}

func (p path) Abs() AbsPath {
	if p.IsAbs() {
		return AbsPath{path: p}
	}
	return Abs(p)
}

func (p path) AsRel() RelPath {
	if p.IsAbs() {
		panic(fmt.Sprintf("%q is not a relative path", p.string))
	}
	return RelPath{path: p}
}

func (p path) Dir() Path {
	return New(filepath.Dir(p.string))
	//return AbsDir{New(filepath.Dir(p.string)).Abs()}
}

func (p path) AbsDir() AbsDir {
	return AbsDir{p.Dir().Abs()}
}

func (p path) AsDir() AbsDir {
	return AbsDir{p.Abs()}
}

func Join(p Path, names ...string) Path {
	args := append([]string{p.String()}, names...)
	slices.DeleteFunc(args, func(s string) bool {
		return s == ""
	})
	fpath := filepath.Join(args...)
	return New(fpath)
}

func (p path) Join(names ...string) Path {
	return Join(p, names...)
}

func Append(p Path, relPath RelPath) Path {
	return p.Join(relPath.string)
}

func (p path) Append(relPath RelPath) Path {
	return Append(p, relPath)
}

func (p path) IsAbs() bool {
	return IsAbs(p)
}

func (p path) IsDir() bool {
	//return must.Ok1(IsDir(p))
	isDir, err := IsDir(p)
	if err != nil {
		slog.Warn("IsDir", "path", p.String(), "err", err)
		return false
	}
	return isDir
}

func IsDir(dir Path, fnames ...string) (bool, error) {
	fpath := dir.Join(fnames...).Abs()
	fi, err := os.Stat(fpath.String())
	if err != nil {
		return false, fmt.Errorf("stat: %w", err)
	}
	return fi.IsDir(), nil
}

func (p path) WriteTo(writer io.Writer) (int64, error) {
	f, err := p.Open()
	if err != nil {
		return 0, err
	}
	bufB := make([]byte, 1024*1024)
	n, err := io.CopyBuffer(writer, f, bufB)
	return n, err
}

func (p path) Sha256Hex() (string, error) {
	h := sha256.New()
	_, err := p.WriteTo(h)
	if err != nil {
		return "", fmt.Errorf("hash xlsxfile: %w", err)
	}
	hashS := hex.EncodeToString(h.Sum(nil))
	return hashS, nil
}

func Exists(dir Path, fnames ...string) bool {
	_, err := Stat(dir, fnames...)
	return err == nil
}

func Stat(dir Path, fnames ...string) (fpath AbsPath, err error) {
	fpath = dir.Join(fnames...).Abs()
	_, err = os.Stat(fpath.string)
	return fpath, err
}

func (p path) Segments() []string {
	return Segments(p)
}

func Segments(fpath Path) []string {
	return strings.Split(fpath.ToSlash().String(), "/")
}

func (ap AbsPath) Rel(baseDir AbsDir) RelPath {
	rp, err := filepath.Rel(baseDir.string, ap.string)
	if err != nil {
		return RelPath{path{ap.string}}
	}
	return New(rp).(RelPath)
}

func (ap AbsPath) IsLocalTo(dir AbsDir) bool {
	rel := ap.Rel(dir)
	return rel.NotEmpty() && !strings.HasPrefix(rel.ToSlash().String(), "..")
}

func Abs(p Path, fpathElems ...string) AbsPath {
	pathElems := append([]string{p.String()}, fpathElems...)
	absPath := absClean(filepath.Join(pathElems...))
	if FollowSymlinks {
		oldAbsPath := absPath
		absPath = absFollowSymlinks(absPath)
		if strings.HasSuffix(absPath.string, "42.dat") {
			slog.Info("absFollowSymlinks", "oldAbsPath", oldAbsPath, "absPath", absPath)
		}
	}
	// if ExpandTilde {
	//	oldAbsPath := absPath
	//	//absPath = AbsPath{absPath.ExpandHome()}
	//	absPath = newAbsPath(absPath.ExpandHome().String())
	//	if strings.HasSuffix(absPath.string, "42.dat") {
	//		slog.Info("ExpandHome", "oldAbsPath", oldAbsPath, "absPath", absPath)
	//	}
	// }
	if ConvertAllToSlash {
		oldAbsPath := absPath
		// absPath = AbsPath(filepath.ToSlash(string(absPath)))
		// absPath = AbsPath{New(absPath.string)}
		// absPath = AbsPath{absPath.ToSlash()}
		absPath = newAbsPath(absPath.ToSlash().String())
		if strings.HasSuffix(absPath.string, "42.dat") {
			slog.Info("ToSlash", "oldAbsPath", oldAbsPath, "absPath", absPath)
		}
	}
	return absPath
}

func absClean(fpath string) AbsPath {
	if ExpandTilde {
		fpath = internal.ExpandHome(fpath)
	}
	abs := must.Ok1(filepath.Abs(fpath))
	if strings.HasSuffix(fpath, "42.dat") {
		slog.Info("absClean", "fpath", fpath, "abs", abs)
	}

	clean := filepath.Clean(abs)
	if runtime.GOOS == "windows" && clean[1] == ':' {
		clean = strings.ToUpper(clean[:1]) + clean[1:]
	}
	return newAbsPath(clean)
}

func absFollowSymlinks(fpath AbsPath) AbsPath {
	if runtime.GOOS == "windows" && len(fpath.string) > 250 {
		return fpath
	}

	// woSymLinks, err := filepath.EvalSymlinks(fpath.string)
	woSymLinks, err := fpath.EvalSymlinks()
	if err != nil {
		if strings.HasSuffix(fpath.string, "42.dat") {
			slog.Error("absFollowSymlinks", "fpath", fpath, "woSymLinks", woSymLinks, "err", err)
		}
		return fpath
	}
	// woSymLinks := must.Ok1(symlinks, err, "filepath.EvalSymlinks(%q)", fpath)
	// return AbsPath(woSymLinks)
	// return AbsPath{Path: New(woSymLinks)}
	// return AbsPath{woSymLinks}
	return newAbsPath(woSymLinks.String())
}

func AbsList(paths ...string) (aps []AbsPath) {
	for _, p := range paths {
		aps = append(aps, New(p).(AbsPath))
	}
	return aps
}

func IsAbs(p Path) bool {
	return filepath.IsAbs(p.String())
}
