package path

import (
	"testing"
)

func TestList_Add(t *testing.T) {
	tests := []struct {
		name     string
		p        string
		wantBase string
	}{
		// TODO: Add test cases.
		{name: "single", p: "a/b/c", wantBase: "a/b/c"},
		{name: "simple", p: "a/b/c/d", wantBase: "a/b/c"},
		{name: "common", p: "a/b/d", wantBase: "a/b"},
		{name: "disjunct", p: "/a/b/c", wantBase: "/"},
		{name: "min", p: "x/y", wantBase: "/"},
		{name: "min", p: "..", wantBase: "/"},
	}
	l := NewList()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := New(tt.p).Abs()
			l.Add(p)
			// fmt.Printf("base = %q", l.base)
			wantBase := New(tt.wantBase).Abs().string
			base := l.base.string
			if base != wantBase {
				t.Errorf("expected base of %q but got %q", tt.wantBase, l.base)
			}
		})
	}
}
