package files

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

const (
	chunkSize = 64 * 1024
)

func AreSame(file1, file2 path.AbsPath) bool {
	f1Info, err1 := os.Stat(file1.String())
	f2Info, err2 := os.Stat(file2.String())
	if err1 != nil || err2 != nil {
		return false
	}
	return os.SameFile(f1Info, f2Info)
}

func AreEqual(file1, file2 path.AbsPath) bool {
	return areEqual(file1, file2, chunkSize)
}

func areEqual(file1, file2 path.AbsPath, chunkSize int) bool {
	// Check file size ...
	f1Info, err1 := os.Stat(file1.String())
	f2Info, err2 := os.Stat(file2.String())
	if err1 != nil || err2 != nil {
		return false
	}
	//f1Info := must.Ok1(os.Stat(file1.String()))
	//f2Info := must.Ok1(os.Stat(file2.String()))
	//if os.SameFile(f1Info, f2Info) {
	//	return true
	//}
	if f1Info.Size() != f2Info.Size() {
		return false
	}
	return deepCompareFiles(file1, file2, chunkSize)
}

func deepCompareFiles(file1, file2 path.AbsPath, chunkSize int) bool {
	f1 := must.Ok1(os.Open(file1.String()))
	defer f1.Close()

	f2 := must.Ok1(os.Open(file2.String()))
	defer f2.Close()

	return deepCompare(f1, f2, chunkSize)
}

func deepCompare(file1, file2 io.Reader, chunkSize int) bool {
	b1 := make([]byte, chunkSize)
	b2 := make([]byte, chunkSize)
	for {
		n1, err1 := file1.Read(b1)
		n2, err2 := file2.Read(b2)
		if err1 != nil || err2 != nil {
			err1Eof := errors.Is(err1, io.EOF)
			err2Eof := errors.Is(err2, io.EOF)
			if err1Eof && err2Eof {
				return true
			}
			if err1Eof || err2Eof {
				return false
			}
			err := fmt.Errorf("comparing 2 files: file1 err=%v  file2 err=%w", err1, err2)
			must.Ok(err)
		}

		if !bytes.Equal(b1[:n1], b2[:n2]) {
			return false
		}
	}
}
