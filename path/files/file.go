package files

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"

	"gitlab.com/zx42/zxgo/path"
)

const (
	Kibi = 1024
	Mebi = Kibi * Kibi
	Gibi = Mebi * Kibi
	Tebi = Gibi * Kibi
)

//func Stat(fnames ...string) (fpath string, err error) {
//	if len(fnames) == 0 || fnames[0] == "" {
//		return "", fmt.Errorf("empty filepath %q: %w", fnames, os.ErrNotExist)
//	}
//
//	fname := filepath.Join(fnames...)
//	fpath = filepath.FromSlash(filepath.Clean(fname))
//	_, err = os.Stat(fpath)
//	return fpath, err
//}
//
//func Exists(fnames ...string) bool {
//	_, err := Stat(fnames...)
//	return err == nil
//}
//
//func IsAbs(dir string) bool {
//	expanded := internal.ExpandHome(dir)
//	return filepath.IsAbs(expanded)
//}

//func Rel(basePath, targetPath string) (string, error) {
//	rel, err := filepath.Rel(basePath, targetPath)
//	if err != nil {
//		return "", err
//	}
//	return filepath.ToSlash(rel), nil
//}

func WriteFile(fname path.AbsPath, data []byte) error {
	return WriteFileWithPerm(fname, data, 0644)
}

func WriteFileWithPerm(fname path.AbsPath, data []byte, perm os.FileMode) error {
	// fdir := fname.AbsDir()
	// err := fdir.MkdirAll(0777)
	// if err != nil {
	if err := path.EnsureParentFolderExists(fname); err != nil {
		return fmt.Errorf("write file: mkdir for %q: %w", fname, err)
	}
	err := os.WriteFile(fname.String(), data, perm)
	if err != nil {
		return fmt.Errorf("write file %q: %w", fname, err)
	}
	return nil
}

// CreateTestDataFile creates a file with random bytes of size (sizeInBytes/100Ki+1)*100Ki
func CreateTestDataFile(fname string, sizeInBytes int, breakTime time.Duration) error {
	return CreateRandomDataFileWithBreaks(fname, sizeInBytes, time.Now().UnixNano(), breakTime)
}

// CreateTestDataFile creates a file with random bytes of size (sizeInBytes/100Ki+1)*100Ki
func CreateRandomDataFile(fname string, sizeInBytes int, seed int64) error {
	return CreateRandomDataFileWithBreaks(fname, sizeInBytes, seed, 0)
}

// CreateTestDataFile creates a file with random bytes of size (sizeInBytes/100Ki+1)*100Ki
func CreateRandomDataFileWithBreaks(fname string, sizeInBytes int, seed int64, breakDelay time.Duration) error {
	// fdir := path.New(fname).AbsDir()
	// err := fdir.MkdirAll(0777)
	// if err != nil {
	fpath := path.New(fname)
	if err := path.EnsureParentFolderExists(fpath); err != nil {
		return fmt.Errorf("creating dir for %q: %w", fname, err)
	}
	f, err := os.Create(fname)
	if err != nil {
		return fmt.Errorf("creating big file %q: %w", fname, err)
	}
	defer f.Close()

	rr := rand.New(rand.NewSource(seed))
	chunkSize := 100 * Kibi
	for i := 0; i < sizeInBytes/chunkSize+1; i++ {
		_, err := io.CopyN(f, rr, int64(chunkSize))
		if err != nil {
			return fmt.Errorf("copy data to big file %q: %w", fname, err)
		}
		//if n != MB {
		//	return fmt.Errorf("copied %d bytes instead of %d to big file %q: %w", n, MB, fname, err)
		//}
		time.Sleep(breakDelay)
	}
	err = f.Close()
	if err != nil {
		return fmt.Errorf("closing big file %q: %w", fname, err)
	}
	return nil
}
