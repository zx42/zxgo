package files

import (
	"testing"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

func TestRenameToBackup(t *testing.T) {
	tests := []struct {
		name              string
		fname             string
		wantNextFreeFPath string
		wantI             int
		wantErr           bool
	}{
		// TODO: Add test cases.
		{name: "x1", fname: "tmp/f1.dat"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fpath := path.New(tt.fname)
			f := must.Ok1(fpath.Create())
			must.Ok(f.Close())
			// defer must.Func(fpath.Abs().Remove)
			defer fpath.Abs().Remove(false)
			gotNextFreeFPath, _, err := RenameToBackup(fpath)
			defer must.Func(func() error { return gotNextFreeFPath.Abs().Remove(false) })
			if (err != nil) != tt.wantErr {
				t.Errorf("RenameToBackup() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// if !reflect.DeepEqual(gotNextFreeFPath, tt.wantNextFreeFPath) {
			// 	t.Errorf("RenameToBackup() gotNextFreeFPath = %v, want %v", gotNextFreeFPath, tt.wantNextFreeFPath)
			// }
			// if gotI != tt.wantI {
			// 	t.Errorf("RenameToBackup() gotI = %v, want %v", gotI, tt.wantI)
			// }
		})
	}
}
