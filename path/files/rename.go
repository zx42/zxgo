package files

import (
	"errors"
	"fmt"
	"io/fs"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/hash"
	"gitlab.com/zx42/zxgo/path"
)

type (
	RenameByKind int
)

const (
	RnByNone RenameByKind = iota
	RnByNumber
	RnByOrigTimeMilliSec
	RnByOrigTimeSec
	RnByOrigTimeMin
	RnByOrigTimeHour
	RnByOrigTimeDay
	RnByOrigTimeMonth
	RnByOrigTimeYear
	RnByHashSha512Hex12
)

func RenameBy(fpath path.Path, renByKind RenameByKind) (err error) {
	switch renByKind {
	case RnByNumber:
		_, _, err = RenameToBackup(fpath)

	case RnByOrigTimeSec:
		err = renameByTimestamp(fpath, "-2006.01.02-15.04.05")
	case RnByOrigTimeMin:
		err = renameByTimestamp(fpath, "-2006.01.02-15.04")
	case RnByOrigTimeHour:
		err = renameByTimestamp(fpath, "-2006.01.02-15")
	case RnByOrigTimeDay:
		err = renameByTimestamp(fpath, "-2006.01.02")
	case RnByOrigTimeMonth:
		err = renameByTimestamp(fpath, "-2006.01")
	case RnByOrigTimeYear:
		err = renameByTimestamp(fpath, "-2006")

	case RnByHashSha512Hex12:
		err = renameByHashSha512Hex12(fpath)

	default:
		msg := fmt.Sprintf("RenameByKind %d is not yet implemented", renByKind)
		panic(msg)
	}
	return err
}

func RenameOld(file path.Path) error {
	if err := RenameBy(file, RnByOrigTimeSec); err != nil {
		return fmt.Errorf("rename old excel file %q: %w", file.String(), err)
	}
	return nil
}

func renameByTimestamp(file path.Path, layout string) error {
	fi, err := file.Stat()
	if err == nil {
		dateSuffixS := fi.ModTime().Format(layout)
		bakFile := file.InsBeforeExt(dateSuffixS)
		err = file.RenameTo(bakFile)
		if err != nil {
			return fmt.Errorf("rename %q to %q: %w", file.String(), bakFile.String(), err)
		}
	} else if !errors.Is(err, fs.ErrNotExist) {
		return fmt.Errorf("stat %q: %w", file.String(), err)
	}
	return nil
}

func renameByHashSha512Hex12(fpath path.Path) (err error) {
	defer errs.DeferWrapf(&err, "RenameBy %q RnByHashSha512Hex12", fpath)
	fileB, err := fpath.ReadFile()
	if err != nil {
		return fmt.Errorf("read file: %w", err)
	}
	hashS := hash.HashSha512AsHex(fileB, 12)
	oldFile := fpath.InsBeforeExt(".bak-" + hashS)
	err = fpath.RenameTo(oldFile)
	if err == nil {
		return nil
	}
	if !errors.Is(err, fs.ErrExist) {
		return err
	}
	// errors.Is(err, fs.ErrExist) == true // target file exists
	// ==>> remove original file because it should be identical with same hash
	err = path.Remove(fpath, false)
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		return err
	}
	return nil
}
