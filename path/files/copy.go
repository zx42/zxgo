package files

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"

	"gitlab.com/zx42/zxgo/etc"
	"gitlab.com/zx42/zxgo/path"
)

type (
	// A FileCopier copies files.
	// The operation of FileCopier's public functions are controled by its
	// public fields. If none are set, the FileCopier behaves accoriding to
	// the zero value rules of each public field.
	FileCopier struct {
		FailIfTargetExists      bool
		PruneEmptyTargetFolders bool
		ProgressCallBack        ProgressCallBackFunc
	}
	ProgressCallBackFunc func(srcFPath, tgtFPath path.Path, percentDone int) bool
)

var ErrFileCopyAbort = errors.New("file copy aborted")

// CopyFile copies the contents of src to dst atomically.
func (fcr FileCopier) CopyFile(srcFPath, tgtFPath path.Path) (err error) {
	_, err = tgtFPath.Stat()
	if fcr.FailIfTargetExists {
		if !errors.Is(err, fs.ErrNotExist) {
			if err == nil {
				err = fs.ErrExist
			}
			err = fmt.Errorf("copy file %s to %s: %w", srcFPath, tgtFPath, err)
			return err
		}
	}

	srcFI, err := srcFPath.Stat()
	if err != nil {
		err = fmt.Errorf("copy file: stat source file: %w", err)
		return err
	}

	err = RenameOld(tgtFPath)
	if err != nil {
		err = fmt.Errorf("copy file: rename target: %w", err)
		return err
	}

	progressCallBack := func(int) bool { return true }
	if fcr.ProgressCallBack != nil {
		progressCallBack = func(percent int) bool { return fcr.ProgressCallBack(srcFPath, tgtFPath, percent) }
	}
	tmpFPath, err := copyFileToTemp(srcFPath, srcFI.Size(), tgtFPath, progressCallBack)
	if err != nil {
		err = fmt.Errorf("copy file to temp %q: %w", tmpFPath, err)
		return err
	}
	cleanup := func() error {
		err := path.Remove(tmpFPath, fcr.PruneEmptyTargetFolders)
		slog.Debug("CopyFile cleanup", "tmpFPath", tmpFPath, "remove-err", err)
		if err == nil || errors.Is(err, fs.ErrNotExist) {
			return nil
		}
		return err
	}
	defer etc.Close(&err, cleanup, "failed to remove temp file")

	err = os.Chmod(tmpFPath.String(), srcFI.Mode())
	slog.Debug("CopyFile os.Chmod", "tmpFPath", tmpFPath, "err", err)
	if err != nil {
		err = fmt.Errorf("copy file to temp %q: %w", tmpFPath, err)
		return err
	}

	err = tmpFPath.RenameTo(tgtFPath)
	slog.Debug("CopyFile tmpFPath.RenameTo", "tmpFPath", tmpFPath, "tgtFPath", tgtFPath, "err", err)
	if err != nil {
		err = fmt.Errorf("rename temp file %q to target %q: %w", tmpFPath, tgtFPath, err)
		return err
	}

	fi, err := srcFPath.Stat()
	if err != nil {
		err = fmt.Errorf("stat source file %q: %w", srcFPath, err)
		return err
	}

	err = os.Chtimes(tgtFPath.String(), fi.ModTime(), fi.ModTime())
	slog.Debug("CopyFile os.Chtimes", "tgtFPath", tgtFPath, "err", err)
	if err != nil {
		err = fmt.Errorf("set time of target file %q: %w", tgtFPath, err)
		return err
	}
	//if fcr.ProgressCallBackFunc != nil && !fcr.ProgressCallBackFunc(100) {
	//	return fmt.Errorf("copy %q -> %q: %w", srcFPath, tgtFPath, ErrFileCopyAbort)
	//}
	return nil
}

func copyFileToTemp(
	srcFPath path.Path,
	fsize int64,
	tgtFPath path.Path,
	progressCallBack func(int) bool,
	// progressCallBack ProgressCallBackFunc,
) (tmpFile path.Path, err error) {
	if progressCallBack != nil && !progressCallBack(0) {
		return tmpFile, fmt.Errorf("copy %q -> %q: %w", srcFPath, tgtFPath, ErrFileCopyAbort)
	}

	srcF, err := srcFPath.Open()
	slog.Debug("copyFileToTemp open src", "srcFPath", srcFPath, "err", err)
	if err != nil {
		err = fmt.Errorf("copy file: open source: %w", err)
		return nil, err
	}
	defer etc.Close(&err, srcF.Close, "failed to close source file")

	err = path.EnsureParentFolderExists(tgtFPath)
	slog.Debug("copyFileToTemp EnsureParentFolderExists", "tgtFPath", tgtFPath, "err", err)
	if err != nil {
		err = fmt.Errorf("copy file %q -> %q: %w", srcFPath.String(), tgtFPath.String(), err)
		return nil, err
	}

	tmpFNamePat := "." + tgtFPath.Base() + "-*"
	tmpF, err := os.CreateTemp(tgtFPath.Dir().String(), tmpFNamePat)
	slog.Debug("copyFileToTemp os.CreateTemp", "tgtFPath", tgtFPath, "tmpFNamePat", tmpFNamePat, "tmpF", tmpF.Name(), "err", err)
	if err != nil {
		err = fmt.Errorf("copy file: create temp file: %w", err)
		return nil, err
	}
	defer etc.Close(&err, tmpF.Close, "failed to close temp file")

	const blockSize int64 = 1024 * 1024
	if progressCallBack != nil && fsize > blockSize {
		restSize := fsize
		var copied int64 = 0
		for restSize > 0 {
			n, err := io.CopyN(tmpF, srcF, blockSize)
			if err != nil {
				if errors.Is(err, io.EOF) {
					restSize -= n
					break
				}
				err = fmt.Errorf("copy file %q -> %q: %w", srcFPath.String(), tmpF.Name(), err)
				return nil, err
			}
			if restSize >= blockSize && n < blockSize {
				panic(fmt.Sprintf("only copied %d bytes", n))
			}
			copied += n
			restSize -= n
			if !progressCallBack(int(copied * 100 / fsize)) {
				return tmpFile, fmt.Errorf("copy %q -> %q: %w", srcFPath, tgtFPath, ErrFileCopyAbort)
			}
		}
		if restSize > 0 {
			panic(fmt.Sprintf("failed to copy rest of %d bytes", restSize))
		}
	} else {
		n, err := io.Copy(tmpF, srcF)
		slog.Debug("copyFileToTemp io.Copy", "srcF", srcF.Name(), "tmpF", tmpF.Name(), "bytes", n, "err", err)
		if err != nil {
			err = fmt.Errorf("copy file %q -> %q: %w", srcFPath.String(), tmpF.Name(), err)
			return nil, err
		}
	}

	if progressCallBack != nil && !progressCallBack(100) {
		return tmpFile, fmt.Errorf("copy %q -> %q: %w", srcFPath, tgtFPath, ErrFileCopyAbort)
	}
	return path.New(tmpF.Name()), nil
}

// CopyFile is a convenience method that calls CopyFile on a FileCopier
// zero value.
func CopyFile(srcFPath, tgtFPath path.Path) error {
	var c FileCopier
	return c.CopyFile(srcFPath, tgtFPath)
}

func (fcr FileCopier) CopyDir(srcDir, tgtDir path.AbsDir) (errOut error) {
	report := func(err error) {
		if err != nil {
			slog.Error("copy file failed", "err", err)
			if errOut == nil {
				errOut = err
			}
		}
	}
	fsys := os.DirFS(srcDir.String())
	err := fs.WalkDir(fsys, ".", func(relPathS string, d fs.DirEntry, err error) error {
		if err != nil {
			report(err)
			return nil
		}
		if d.IsDir() {
			return nil
		}
		srcFPath := srcDir.Join(relPathS).(path.AbsPath)
		tgtFPath := tgtDir.Join(relPathS).(path.AbsPath)
		err = fcr.CopyFile(srcFPath, tgtFPath)
		if err != nil {
			report(err)
			return nil
		}
		return nil
	})
	return err
}

func CopyDir(srcDir, tgtDir path.AbsDir) (errOut error) {
	var c FileCopier
	return c.CopyDir(srcDir, tgtDir)
}

// Deprecated: use CopyDir() instead
func CopyAll(srcDir, tgtDir path.AbsDir) (errOut error) {
	return CopyDir(srcDir, tgtDir)
}

// Deprecated: use CopyFile() instead
func Copy(srcFPath, tgtFPath path.AbsPath) (errOut error) {
	return CopyFile(srcFPath, tgtFPath)
}

//func Copy(srcFPath, tgtFPath path.AbsPath) (errOut error) {
//	_, _, err := RenameToBackup(tgtFPath)
//	if err != nil {
//		err = fmt.Errorf("copy file: rename target: %w", err)
//		return err
//	}
//	srcF, err := srcFPath.Open()
//	if err != nil {
//		err = fmt.Errorf("copy file: open source: %w", err)
//		return err
//	}
//	close := func(f io.Closer) {
//		err := f.Close()
//		if errOut == nil {
//			errOut = err
//		}
//	}
//	defer close(srcF)
//	tgtF, err := tgtFPath.Create()
//	if err != nil {
//		err = fmt.Errorf("copy file: create target: %w", err)
//		return err
//	}
//	defer close(tgtF)
//
//	_, err = io.Copy(tgtF, srcF)
//	if err != nil {
//		err = fmt.Errorf("copy file %q -> %q: %w", srcFPath.String(), tgtFPath.String(), err)
//		return err
//	}
//
//	return nil
//}
