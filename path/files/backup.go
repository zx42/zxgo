package files

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/zx42/zxgo/path"
)

// Deprecated: use RenameOld() instead
func RenameToBackup(fpath path.Path) (nextFreeFPath path.Path, i int, err error) {
	nextFreeFPath, i = GetNextFreeFileName(fpath)
	if nextFreeFPath.IsEmpty() {
		return nextFreeFPath, i, nil
	}

	err = fpath.Abs().RenameTo(nextFreeFPath.Abs())
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		err = fmt.Errorf("RenameToBackup(%q): rename to %q: %w", fpath, nextFreeFPath, err)
		return path.AbsPath{}, i, err
	}
	return nextFreeFPath, i, nil
}

func GetNextFreeFileName(fpath path.Path) (nextFreeFPath path.Path, i int) {
	_, err := os.Stat(fpath.String())
	for i = 1; err == nil; i++ {
		num := fmt.Sprintf(".bak-%03d", i)
		nextFreeFPath = fpath.InsBeforeExt(num) // .Abs()
		_, err = nextFreeFPath.Stat()
	}
	return nextFreeFPath, i - 1
}
