package path

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
)

type (
	AbsDir struct {
		AbsPath
	}
)

func (ad AbsDir) IoFsFS() fs.FS {
	return os.DirFS(ad.string)
}

func (ad AbsDir) Walk(
	shouldSkip ShouldSkipFunc,
	process ProcessFunc,
	handleError HandleErrorFunc,
	// shouldSkip func(de fs.DirEntry) (bool, error),
	// process func(de DirEntry) error,
	// handleError func(de DirEntry, err error) error,
) error {
	return Walk(ad.IoFsFS(), shouldSkip, process, handleError)
}

func (ad AbsDir) IsDirEmpty() (bool, error) {
	return IsDirEmpty(ad)
}

func IsDirEmpty(dir AbsDir) (bool, error) {
	entriesL, err := os.ReadDir(dir.String())
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return true, nil
		}
		return false, fmt.Errorf("IsDirEmpty: read dir %q: %w", dir, err)
	}
	return len(entriesL) == 0, nil
}
