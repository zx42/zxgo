package path_test

import (
	"os"
	"testing"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

func TestEvalSymlinks(t *testing.T) {
	tests := []struct {
		name string
		old  string
		newN string
		eval string
		want string
	}{
		{name: "x1", old: "../d1", newN: "d2/l1", want: "d1"},
		{name: "x2", old: "../d1", newN: "d2/l1", eval: "x2", want: "d1"},
		{name: "x3", old: "../d1", newN: "d2/l1", eval: "x2/x3", want: "d1"},
		// TODO: Add test cases.
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			must.Ok(os.RemoveAll("tmp"))
			must.Ok(os.MkdirAll("tmp/d1", 0777))
			must.Ok(os.MkdirAll("tmp/d2", 0777))

			newS := "tmp/" + tc.newN
			must.Ok(os.Symlink(tc.old, newS))
			newP := path.New(newS).Join(tc.eval)
			got := must.Ok1(path.EvalSymlinks(newP))
			wantP := path.New("tmp").Join(tc.want, tc.eval)
			if got.Abs().String() != wantP.Abs().String() {
				t.Errorf("%q: EvalSymlinks(%q) got = %q, want %q", tc.name, newP, got, wantP)
			}

			must.Ok(os.RemoveAll("tmp"))
		})
	}
}

func TestIsChildOf(t *testing.T) {
	tests := []struct {
		name  string
		dir   string
		fpath string
		want  bool
	}{
		// TODO: Add test cases.
		{name: "simple yes", dir: "tmp", fpath: "tmp/x", want: true},
		{name: "simple no", dir: "tmp", fpath: "x", want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir := path.New(tt.dir).AsDir()
			fpath := path.New(tt.fpath).Abs()
			if got := fpath.IsLocalTo(dir); got != tt.want {
				t.Errorf("IsChildOf() = %v, want %v", got, tt.want)
			}
		})
	}
}
