package path

import (
	"fmt"
	"io/fs"
	"os"
)

func (p path) Stat() (fs.FileInfo, error) {
	fi, err := os.Stat(p.string)
	return fi, err
}

func (p path) Open() (*os.File, error) {
	of, err := os.Open(p.string)
	return of, err
}

func (p path) Create() (*os.File, error) {
	if p.Exists() {
		return nil, fmt.Errorf("creating file %q: %w", p, os.ErrExist)
	}
	return p.CreateForced()
}

func (p path) CreateForced() (*os.File, error) {
	err := EnsureParentFolderExists(p)
	if err != nil {
		return nil, fmt.Errorf("create %q: %w", p.string, err)
	}
	cf, err := os.Create(p.string)
	return cf, err
}

func (p path) Exists() bool {
	return Exists(p)
}

func (p path) ReadFile() ([]byte, error) {
	contB, err := os.ReadFile(p.string)
	return contB, err
}

func (p path) WriteFile(data []byte, perm fs.FileMode) error {
	err := EnsureParentFolderExists(p)
	if err != nil {
		return fmt.Errorf("write file %q: %w", p.string, err)
	}
	err = os.WriteFile(p.string, data, perm)
	return err
}

func (ad AbsDir) MkdirAll(perm os.FileMode) error {
	return os.MkdirAll(ad.string, perm)
}

func RenameForced(oldPath, newPath Path) error {
	return doRename(oldPath, newPath, true)
}

func doRename(oldPath, newPath Path, force bool) error {
	if !force {
		_, err := newPath.Stat()
		if err == nil {
			return fmt.Errorf("rename %q: target %q exists: %w", oldPath, newPath, fs.ErrExist)
		}
	}
	err := EnsureParentFolderExists(newPath)
	if err != nil {
		return fmt.Errorf("rename %q >> %q: mkdir parent: %w", oldPath, newPath, err)
	}
	err = os.Rename(oldPath.String(), newPath.String())
	return err
}

//func Rename(oldPath, newPath Path) error {
//	_, err := newPath.Stat()
//	if err == nil {
//		return fmt.Errorf("rename %q: target %q exists: %w", oldPath, newPath, fs.ErrExist)
//	}
//	err = newPath.AbsDir().MkdirAll(fs.ModePerm)
//	if err != nil {
//		return fmt.Errorf("rename %q >> %q: mkdir parent: %w", oldPath, newPath, err)
//	}
//	err = os.Rename(oldPath.String(), newPath.String())
//	return err
//}

func (p path) RenameTo(newPath Path) error {
	return doRename(p, newPath, false)
}

func EnsureParentFolderExists(p Path) error {
	return p.AbsDir().MkdirAll(fs.ModePerm)
}

// Deprecated: use EnsureParentFolderExists() instead
func EnsureDirExists(p Path) error {
	return EnsureParentFolderExists(p)
}

func (ap AbsPath) Remove(pruneDirs bool) error {
	return Remove(ap.path, pruneDirs)
}

func (ap AbsPath) SymlinkTo(tgtPath AbsPath) error {
	err := EnsureParentFolderExists(ap)
	if err != nil {
		return fmt.Errorf("symlink %q >> %q: mkdir parent: %w", ap.string, tgtPath.string, err)
	}
	err = os.Symlink(tgtPath.string, ap.string)
	if err != nil {
		return fmt.Errorf("symlink %q >> %q: %w", ap.string, tgtPath.string, err)
	}
	return nil
}

func Remove(f Path, pruneDirs bool) error {
	err := os.Remove(f.String())
	if err != nil {
		return fmt.Errorf("remove file %q: %w", f, err)
	}
	if !pruneDirs {
		return nil
	}
	err = PruneEmptyDir(f.AbsDir())
	if err != nil {
		return fmt.Errorf("remove file %q: %w", f, err)
	}
	return nil
}

func PruneEmptyDir(dir AbsDir) error {
	isEmpty, err := IsDirEmpty(dir)
	if err != nil {
		return fmt.Errorf("prune dir: %w", err)
	}
	if !isEmpty {
		return nil
	}
	err = os.Remove(dir.String())
	if err != nil {
		return fmt.Errorf("prune dir: remove dir %q: %w", dir, err)
	}
	return PruneEmptyDir(dir.AbsDir())
}
