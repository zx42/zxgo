package path

import (
	"path/filepath"
	"runtime"
	"strings"
)

type (
	List struct {
		base AbsDir
		l    []AbsPath
	}
)

func NewList() List {
	return List{}
}

func (l List) BaseDir() AbsDir {
	return l.base
}

func (l *List) Add(p AbsPath) {
	l.l = append(l.l, p)
	l.base = l.commonBase()
}

func (l List) Rel(p AbsPath) RelPath {
	return p.Rel(l.base)
}

func (l List) commonBase() AbsDir {
	var base AbsDir
	for _, p := range l.l {
		if base.IsEmpty() {
			base = p.AsDir()
			continue
		}
		if strings.HasPrefix(p.string, base.string) {
			continue
		}

		base = findCommonBase(base, p)
	}
	return base
}

func findCommonBase(cp AbsDir, pp AbsPath) AbsDir {
	cpL := strings.Split(cp.ToSlash().String(), "/")
	ppL := strings.Split(pp.ToSlash().String(), "/")
	for i := range cpL {
		if i >= len(ppL) || cpL[i] != ppL[i] {
			return joinAbsDir(ppL[:i])
		}
		// if i >= len(ppL) {
		//	return joinAbsDir(ppL[:i])
		// }
		// if cpL[i] != ppL[i] {
		//	return joinAbsDir(ppL[:i])
		// }
	}
	return cp
}

func joinAbsDir(pathL []string) AbsDir {
	psep := "/"
	if !ConvertAllToSlash {
		psep = filepath.FromSlash("/")
	}
	xpS := strings.Join(pathL, psep)
	if runtime.GOOS == "windows" {
		vn := filepath.VolumeName(xpS)
		// fmt.Println(vn)
		if xpS == vn {
			xpS += "/"
		}
	} else if xpS == "" {
		xpS = "/"
	}
	// if runtime.GOOS != "windows" && xpS == "" {
	// 	xpS = "/"
	// }
	return New(xpS).AsDir()
}
