package path

import (
	"fmt"
	"io/fs"
	"log/slog"
)

type (
	DirEntry interface {
		fs.DirEntry
		RelPath() RelPath
	}
	dirEntry struct {
		fs.DirEntry
		relPath RelPath
	}
)

func newDirEntry(fpath string, de fs.DirEntry) DirEntry {
	return &dirEntry{
		DirEntry: de,
		relPath:  New(fpath).AsRel(),
	}
}

func (de dirEntry) RelPath() RelPath {
	return de.relPath
}

func Walk(
	fsToWalk fs.FS,
	shouldSkip ShouldSkipFunc,
	process ProcessFunc,
	handleError HandleErrorFunc,
) error {
	if shouldSkip == nil {
		shouldSkip = ShouldSkip()
	}
	err := fs.WalkDir(fsToWalk, ".", func(path string, d fs.DirEntry, err error) error {
		de := newDirEntry(path, d)
		if err != nil {
			if d != nil && d.IsDir() {
				handleError(de, err)
				return err
			}
			return handleError(de, err)
		}
		skip, err := shouldSkip(de)
		if skip {
			return err
		}
		return process(de)
	})
	if err != nil {
		return fmt.Errorf("walking fs: %w", err)
	}
	return nil
}

func CollectFile(entries map[RelPath]fs.DirEntry) func(de DirEntry) error {
	return func(de DirEntry) error {
		entries[de.RelPath()] = de
		return nil
	}
}

func LogFileError(de DirEntry, err error) error {
	if err != nil {
		slog.Error("stat file", "file", de.RelPath(), "err", err)
	}
	return err
}
