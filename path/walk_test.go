package path_test

import (
	"io/fs"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	_ "github.com/smartystreets/goconvey/convey"

	"gitlab.com/zx42/zxgo/memfs"
	"gitlab.com/zx42/zxgo/osfs"
	"gitlab.com/zx42/zxgo/path"
)

func TestWalk(t *testing.T) {
	// Only pass t into top-level Convey calls
	Convey("Given a folder with files", t, func() {
		baseDir, clean := osfs.MkdirTemp("tmp", "walk-*")
		mfs, _, err := memfs.CreateFsOnDisk(baseDir, memfs.MemFS{"a/b.x": "#a/b", "c/d.y": "#c/d"})
		So(err, ShouldBeNil)
		Convey("walking it should collect all files", func() {
			filesM := map[path.RelPath]fs.DirEntry{}
			err := baseDir.Walk(path.ShouldSkip(), path.CollectFile(filesM), path.LogFileError)
			So(err, ShouldBeNil)
			for fn := range mfs {
				frel := path.New(fn).AsRel()
				_, ok := filesM[frel]
				So(ok, ShouldBeTrue)
				delete(filesM, frel)
			}
			So(len(filesM), ShouldEqual, 0)
			clean.IfOk(err)
		})

		Convey("walking it should not collect skipped files", func() {
			filesM := map[path.RelPath]fs.DirEntry{}
			err := baseDir.Walk(path.ShouldSkip(path.NewGlob("*/b*")), path.CollectFile(filesM), path.LogFileError)
			So(err, ShouldBeNil)
			delete(mfs, "a/b.x")
			for fn := range mfs {
				frel := path.New(fn).AsRel()
				_, ok := filesM[frel]
				So(ok, ShouldBeTrue)
				delete(filesM, frel)
			}
			So(len(filesM), ShouldEqual, 0)
			clean.IfOk(err)
		})
		clean.Up("..")
	})
}
