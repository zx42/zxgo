package path_test

import (
	"testing"

	"gitlab.com/zx42/zxgo/must"
	"gitlab.com/zx42/zxgo/path"
)

func Test_pruneEmptyDir(t *testing.T) {
	type args struct {
		dir   string
		empty bool
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "empty dir", args: args{
			dir:   "tmp/empty",
			empty: true,
		}},
		{name: "non empty dir", args: args{
			dir: "tmp/non-empty",
			//empty: true,
		}},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir := path.New(tt.args.dir).AsDir()
			if tt.args.empty {
				must.Ok(dir.MkdirAll(0777))
			} else {
				must.Ok(dir.Join("d0").AsDir().MkdirAll(0777))
			}
			if err := path.PruneEmptyDir(dir); (err != nil) != tt.wantErr {
				t.Errorf("pruneEmptyDir() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
