package path

import (
	"io/fs"
)

type (
	ShouldSkipFunc  func(de fs.DirEntry) (bool, error)
	ProcessFunc     func(de DirEntry) error
	HandleErrorFunc func(de DirEntry, err error) error
)

func ShouldSkip(excludeRXs ...Glob) ShouldSkipFunc {
	return ShouldInExclude()(excludeRXs...)
}

func ShouldInExclude(includeGlobs ...Glob) func(excludeGlobs ...Glob) ShouldSkipFunc {
	inGlobs := NewGlobs(includeGlobs...)
	includeAll := len(includeGlobs) == 0
	return func(excludeGlobs ...Glob) ShouldSkipFunc {
		exGlobs := NewGlobs(excludeGlobs...)
		return func(de fs.DirEntry) (bool, error) {
			fname := de.Name()
			pde, ok := de.(DirEntry)
			if ok {
				fname = pde.RelPath().String()
			}
			inMatches := inGlobs.Matches(fname)
			if !includeAll && !inMatches {
				return true, nil
			}
			exMatches := exGlobs.Matches(fname)
			if !de.Type().IsRegular() {
				if de.IsDir() && exMatches {
					return true, fs.SkipDir
				}
				return true, nil
			}
			if exMatches {
				return true, nil
			}
			return false, nil
		}
	}
}
