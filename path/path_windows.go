package path

import (
	"path/filepath"
	"strings"
)

func normalizePath(pathS string) string {
	pS := pathS
	for len(pS) > 1 && filepath.ToSlash(pS) != "/" && strings.HasSuffix(filepath.ToSlash(pS), "/") {
		pS = pS[:len(pS)-1]
	}
	if strings.HasSuffix(pS, ":") {
		pS += "/"
	}
	return pS
}
