package path

type (
	Globs struct {
		globs []Glob
	}
)

func NewGlobs(globPatterns ...Glob) Globs {
	globs := Globs{globs: globPatterns}
	return globs
}

func (globs Globs) Matches(s string) bool {
	for _, glob := range globs.globs {
		if glob.Matches(s) {
			return true
		}
	}
	return false
}

func (globs Globs) Files(baseDir AbsDir) ([]Path, error) {
	return getMatchingFiles(baseDir, globs.Matches)
}

func NewGlobsFromStrings(sL ...string) (globs []Glob) {
	for _, s := range sL {
		globs = append(globs, NewGlob(s))
	}
	return globs
}
