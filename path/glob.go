package path

import (
	"io/fs"
	"regexp"
	"strings"
)

type (
	Glob struct {
		Pattern string
		rx      *regexp.Regexp
		//Verbose  bool
	}
)

func NewGlob(globPattern string) Glob {
	pat := globPattern
	pat = strings.ReplaceAll(pat, ".", `\.`)
	pat = strings.ReplaceAll(pat, "**", `$$double-star$$`)
	pat = strings.ReplaceAll(pat, "*", `[^/\\]*`)
	pat = strings.ReplaceAll(pat, "$$double-star$$", `.*`)
	pat = strings.ReplaceAll(pat, "?", `.`)
	glob := Glob{
		Pattern: globPattern,
		rx:      regexp.MustCompile(pat),
	}
	return glob
}

//func NewGlob(globPattern string) Glob {
//	pat := globPattern
//	pat = strings.ReplaceAll(pat, ".", `\.`)
//	pat = strings.ReplaceAll(pat, "**", `.*`)
//	pat = strings.ReplaceAll(pat, "*", `[^/\\]*`)
//	pat = strings.ReplaceAll(pat, "?", `.`)
//	glob := Glob{
//		Pattern: globPattern,
//		rx:      regexp.MustCompile(pat),
//	}
//	return glob
//}

func (glob Glob) Matches(s string) bool {
	return glob.rx.MatchString(s)
}

func (glob Glob) Files(baseDir AbsDir) ([]Path, error) {
	return getMatchingFiles(baseDir, glob.Matches)
}

func getMatchingFiles(baseDir AbsDir, matches func(relPath string) bool) ([]Path, error) {
	filesM := map[RelPath]fs.DirEntry{}
	err := baseDir.Walk(ShouldSkip(), CollectFile(filesM), LogFileError)
	if err != nil {
		return nil, err
	}
	matching := make([]Path, 0, 30)
	for fname := range filesM {
		if matches(fname.String()) {
			matching = append(matching, baseDir.Append(fname))
		}
	}
	return matching, nil
}
