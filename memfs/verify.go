package memfs

import (
	"errors"
	"fmt"
	"io/fs"

	"gitlab.com/zx42/zxgo/path"
)

var (
	ErrFileContentWrong = errors.New("wrong file content")
)

func (wantFs MemFS) VerifyFiles(
	folder path.AbsDir,
	shouldSkip func(de fs.DirEntry) (bool, error),
	stopOnFirstError bool,
) (err error) {
	if len(wantFs) == 0 {
		if folder.Exists() {
			return fmt.Errorf("verify nonexistence of files: %w", fs.ErrExist)
		}
		return nil
	}

	actFs, err := LoadFromDiskInExclude(
		folder,
		shouldSkip,
	)
	if err != nil {
		return err
	}

	var (
		errs []error
	)
	for fn, fwant := range wantFs {
		err = verifyFile(actFs, fn, fwant)
		if err != nil && stopOnFirstError {
			return err
		}
		errs = append(errs, err)
	}
	return errors.Join(errs...)
}

func verifyFile(actFs MemFS, fname string, fwant string) (err error) {
	fcont, ok := actFs[fname]
	if fcont != fwant {
		err = ErrFileContentWrong
	}
	if fwant == "" {
		if ok {
			return fmt.Errorf("found unexpected file %q: %w", fname, fs.ErrExist)
		}
	} else {
		if !ok {
			return fmt.Errorf("missing file %q: %w", fname, fs.ErrNotExist)
		}
		if fcont != fwant {
			return fmt.Errorf("wrong file %q: %w", fname, ErrFileContentWrong)
		}
	}
	return err
}
