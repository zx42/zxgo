package memfs

import (
	"errors"
	"fmt"
	"io/fs"
	"regexp"

	"gitlab.com/zx42/zxgo/errs"
	"gitlab.com/zx42/zxgo/path"
)

type (
	// used to verify:
	//   - existence of files
	//   - nonexistence of files
	//   - exact content of files
	//   - similar content of files
	ExpectFS     map[string]FileVerifier
	FileVerifier func(fpath path.Path) error
)

var (
	ErrFileContentMismatch = errors.New("file content mismatch")
)

func NewExpectFS(nameConts ...string) (fakeFS ExpectFS) {
	memFs := NewMemFS(nameConts...)
	return NewExpectFromMemFS(memFs)
}

func NewExpectFromMemFS(memFs MemFS) (fakeFS ExpectFS) {
	for fname, fcont := range memFs {
		if len(fcont) == 0 {
			fakeFS[fname] = VerifyFileMissing()
		} else {
			fakeFS[fname] = VerifyFileContent(fcont)
		}
	}
	return fakeFS
}

//func (expFs ExpectFS) AddAgeFile(hashS string) {
//	expFs[fmt.Sprintf("data-crypt/%s.age", hashS)] = VerifyFileRegex("^-----BEGIN AGE ENCRYPTED FILE-----")
//}

func VerifyFileContent(fcontS string) FileVerifier {
	return verifyFileMatch(func(fcontB []byte) bool {
		return string(fcontB) == fcontS
	})
}

func VerifyFileExists() FileVerifier {
	return func(fpath path.Path) error {
		if !fpath.Exists() {
			return fs.ErrNotExist
		}
		return nil
	}
}

func VerifyFileMissing() FileVerifier {
	return func(fpath path.Path) error {
		if fpath.Exists() {
			return fs.ErrExist
		}
		return nil
	}
}

func VerifyFileRegex(rxS string) FileVerifier {
	rx := regexp.MustCompile(rxS)
	return verifyFileMatch(func(fcontB []byte) bool {
		return rx.Match(fcontB)
	})
}

func verifyFileMatch(matches func(fcontB []byte) bool) FileVerifier {
	return func(fpath path.Path) error {
		fcontB, err := fpath.ReadFile()
		if err != nil {
			return fmt.Errorf("read file: %w", err)
		}
		if !matches(fcontB) {
			return ErrFileContentMismatch
		}
		return nil
	}
}

func (expFs ExpectFS) VerifyMinFiles(
	folder path.AbsDir,
	stopOnFirstError bool,
) (err error) {
	return verifyMinFiles(expFs, folder, stopOnFirstError, nil)
}

func verifyMinFiles(
	expFs ExpectFS,
	folder path.AbsDir,
	stopOnFirstError bool,
	deleteFromFilesMap func(fname string),
) (err error) {
	defer errs.DeferWrapf(&err, "verify at least files in %q", folder)
	errL := make([]error, 0, len(expFs))
	for fname, verify := range expFs {
		fpath := folder.Join(fname)
		err := verify(fpath)
		if err != nil {
			err = fmt.Errorf("verify file %q failed: %w", fname, err)
			if stopOnFirstError {
				return err
			}
			errL = append(errL, err)
			continue
		}
		if deleteFromFilesMap != nil {
			deleteFromFilesMap(fname)
		}
	}
	return errors.Join(errL...)
}

func (expFs ExpectFS) VerifyExactFiles(
	folder path.AbsDir,
	stopOnFirstError bool,
	shouldSkip path.ShouldSkipFunc,
) (err error) {
	defer errs.DeferWrapf(&err, "verify exact files in %q", folder)

	actFilesM := map[path.RelPath]fs.DirEntry{}
	err = folder.Walk(shouldSkip, path.CollectFile(actFilesM), path.LogFileError)
	if err != nil {
		return fmt.Errorf("walk %q: %w", folder, err)
	}

	del := deleteFromFilesMap(actFilesM)
	err = verifyMinFiles(expFs, folder, stopOnFirstError, del)
	if err != nil {
		return err
	}
	if len(actFilesM) > 0 {
		return fmt.Errorf("found unexpected extra files %+v in %q", actFilesM, folder)
	}
	return nil
}

func deleteFromFilesMap(actFilesM map[path.RelPath]fs.DirEntry) func(fname string) {
	return func(fname string) {
		frel := path.New(fname).AsRel()
		_, ok := actFilesM[frel]
		if !ok {
			panic("oops")
		}
		delete(actFilesM, frel)
	}
}
