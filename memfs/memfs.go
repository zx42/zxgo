package memfs

import (
	"fmt"
	"io/fs"

	"gitlab.com/zx42/zxgo/path"
)

type (
	MemFS map[string]string
)

func NewMemFS(nameConts ...string) (memFs MemFS) {
	if len(nameConts)%2 != 0 {
		panic("need even number of strings")
	}
	for i := range nameConts[len(nameConts)/2:] {
		fname := nameConts[i*2]
		fcont := nameConts[i*2+1]
		memFs[fname] = fcont
	}
	return memFs
}

func CreateFsOnDisk(baseDir path.AbsDir, memFs MemFS) (MemFS, path.AbsDir, error) {
	return memFs, baseDir, memFs.WriteToDisk(baseDir)
}

func (memFs MemFS) WriteToDisk(baseDir path.AbsDir) error {
	for fpathS, fileS := range memFs {
		fpath := baseDir.Join(fpathS)
		err := fpath.WriteFile([]byte(fileS), 0777)
		if err != nil {
			return fmt.Errorf("create fake fs: %w", err)
		}
	}
	return nil
}

func (memFs MemFS) WipeFromDisk(baseDir path.AbsDir) error {
	for fpathS := range memFs {
		fpath := baseDir.Join(fpathS)
		err := path.Remove(fpath, true)
		if err != nil {
			return fmt.Errorf("wipe fake fs %q: %w", baseDir, err)
		}
	}
	return nil
}

func LoadFromDisk(
	baseDir path.AbsDir,
) (MemFS, error) {
	storeFs := MemFS{}
	err := storeFs.LoadFromDisk(
		baseDir,
		path.ShouldSkip(),
	)
	return storeFs, err
}

func LoadFromDiskInExclude(
	baseDir path.AbsDir,
	shouldSkip func(de fs.DirEntry) (bool, error),
) (MemFS, error) {
	storeFs := MemFS{}
	err := storeFs.LoadFromDisk(
		baseDir,
		shouldSkip,
	)
	return storeFs, err
}

func (memFs MemFS) LoadFromDisk(
	baseDir path.AbsDir,
	shouldSkip func(de fs.DirEntry) (bool, error),
) error {
	files := make(map[path.RelPath]fs.DirEntry)
	err := baseDir.Walk(
		shouldSkip,
		path.CollectFile(files),
		path.LogFileError,
	)
	for path := range files {
		fpath := baseDir.Append(path)
		fcontB, err := fpath.ReadFile()
		if err != nil {
			return err
		}
		memFs[path.String()] = string(fcontB)
	}
	if err != nil {
		return fmt.Errorf("load fake fs: %w", err)
	}
	return nil
}
