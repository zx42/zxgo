package num

import (
	"strconv"
	"strings"

	"golang.org/x/exp/constraints"
)

func IsNum(s string) bool {
	_, isNum := GetNum(s)
	return isNum
}

func GetNum(s string) (int, bool) {
	i64, err := strconv.ParseInt(strings.TrimSpace(s), 10, 32)
	if err != nil {
		return 0, false
	}
	return int(i64), true
}

func FirstNumIdx(tgtKeyL []string) int {
	for j, key := range tgtKeyL {
		//_, IsNum := GetNum(key)
		//if IsNum {
		if IsNum(key) {
			return j
		}
		//switch tgtKeyL[j].(type) {
		//case int:
		//	return j
		//}
	}
	return -1
}

func SliceUpTo[T any](slc []T, max int) []T {
	min := Min(len(slc), max)
	return slc[:min]
}

func Min[T constraints.Ordered](candidates ...T) T {
	if len(candidates) < 2 {
		panic("need at least 2 values to compare")
	}
	// s[x:y]
	candMin := candidates[0]
	for _, cand := range candidates[1:] {
		if cand < candMin {
			candMin = cand
		}
	}
	return candMin
}
